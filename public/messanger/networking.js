function handleData(data) {
    var trueData = {}

    console.log(data)
    try {
        trueData = JSON.parse(data)
        console.log(trueData)

        if (trueData.type == "message-event") {
            messages.push(trueData.data)
            renderMessageInfo(messages)
        } else if (trueData.type == "nick-event") {
            const currentDate = new Date();
            const fullISODate = currentDate.toISOString();

            messageObj = {
                text:  trueData.data.usernameChanged[0] + " has changed their name to " + trueData.data.usernameChanged[1],
                time: fullISODate,
                user: "server",
            }

            messages.push(messageObj)

            renderMessageInfo(messages)
        } else if (trueData.type == "join-event") {
            const currentDate = new Date();
            const fullISODate = currentDate.toISOString();

            messageObj = {
                text:  trueData.data.userJoin + " has just joined!" ,
                time: fullISODate,
                user: "server",
            }

            messages.push(messageObj)

            renderMessageInfo(messages)
        }
    } catch (error) {
        console.log(error.message)
    }
}

function startServer() {
    peer = new Peer();

    peer.on('open', function (id) {

        console.log('My peer ID is: ' + id);

        me.peerId = id
        $("#id").val(me.peerId)

        me.isServer = true

        conId = id
        peer.on('connection', function (conn) {
            // Receive messages
            console.log("oh, this is exiting, there's a connection!")

            conn.on('data', function (data) {
                console.log(data)
                handleData(data)
            });

            // Send messages
            conn.send('Hello!');

            // New message handler
            // Will execute myCallback every 0.5 seconds 
            var intervalID = window.setInterval(handleMessageQueue, 500);

            function handleMessageQueue() {
                for (message in me.messageQueue){
                    conn.send(me.messageQueue[message])
                }

                me.messageQueue = []
            }
        });
    });
}

function beClient (peerId) {
    var peer = new Peer();

    peer.on('open', function (id) {
        console.log('My peer ID is: ' + id);
        conn = peer.connect(peerId);

        console.log("Connecting to ", peerId)

        conn.on('open', function () {
            // Receive messages

            console.log("opened connection")
            conn.on('data', function (data) {
                console.log(data)

                handleData(data)
            });

            // Send messages
            conn.send('Hello!');

            // New message handler
            // Will execute myCallback every 0.5 seconds 
            var intervalID = window.setInterval(handleMessageQueue, 500);

            function handleMessageQueue() {
                for (message in me.messageQueue){
                    conn.send(me.messageQueue[message])
                }

                me.messageQueue = []
            }
        });
    });
}