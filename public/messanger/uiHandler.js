// Prepare UI
me.username = prompt("Enter a username") || "404 user"

//me.username = "hi"

$("#username").val(me.username)

function weJoined() {
    apiRequest = {
        type: "join-event",
        data: {
            userJoin: me.username
        }
    }

    me.messageQueue.push(JSON.stringify(apiRequest))

    console.log(apiRequest)

    renderMessageInfo(messages)
}

// Top form

$("#id").on("input", function () {
    beClient($("#id").val())
    weJoined()
})

$("#username").on("input", function () {
    me.oldUsername = me.username
    me.username = $(this).val()

    const currentDate = new Date();
    const fullISODate = currentDate.toISOString();

    messageObj = {
        text:  me.oldUsername + " has changed their name to " + me.username,
        time: fullISODate,
        user: "server",
        usernameChanged: [me.oldUsername, me.username]
    }

    apiRequest = {
        type: "nick-event",
        data: messageObj
    }

    messages.push(messageObj)

    me.messageQueue.push(JSON.stringify(apiRequest))

    console.log(apiRequest)

    renderMessageInfo(messages)
})

$("#becomeServer").click(() => {
    startServer()
    weJoined()
    $("#id").val(me.peerId)
    //copyToClipboard(me.peerId)
})

// Message send area buttons

$("#sendMessage").click(() => {
    messageText = $("#message").val()

    const currentDate = new Date();
    const fullISODate = currentDate.toISOString();
    
    messageObj = {
        text: messageText,
        time: fullISODate,
        user: me.username
    }

    apiRequest = {
        type: "message-event",
        data: messageObj
    }

    messages.push(messageObj)

    me.messageQueue.push(JSON.stringify(apiRequest))

    console.log(apiRequest)

    renderMessageInfo(messages)
})

// Functions for drawing out a series of messages
function renderMessageInfo(messages){
    $("#messageArea").empty()

    for (i = 0; i < messages.length; i++) {
        $("#messageArea").append(
            $("<div>").append(
                $("<code>").text(messages[i].user),
                $("<i>").text(messages[i].time),
                $("<p>").text(messages[i].text)
            )
        )
    }
}
