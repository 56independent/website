$("#add-component-dialog").hide()
$("#add-scene-dialog").hide()

let PROJECT = {
  editors: {
    
  },
  settings: {
    "currentObj":"",
    "preferredCode":"text",
    "advancedMode":false
  },
}

const attributesObject = {
  "content":{
    "tooltip":"The contents of an element, will often show as text",
    "type": "string"
  },
  "type":{
    "tooltip":"The type of HTML element; refer to documentation.",
    "type":"string"
  },
  "class":{
    "tooltip":"Used by bootstrap to make an element show",
    "type":"list"
  },
  "src":{
    "tooltip":"The external source of the item",
    "type":"string"
  },
  "accesskey": {
    "tooltip": "a shortcut key to activate/focus an element",
    "type": "string"
  },
  "alt": {
    "tooltip": "an alternate text for an image",
    "type": "string"
  },
  "autocomplete": {
    "tooltip": "whether an input field should have autocomplete enabled or disabled",
    "type": "string"
  },
  "autofocus": {
    "tooltip": "that an element should automatically get focus when the page loads",
    "type": "boolean"
  },
  "autoplay": {
    "tooltip": "that the audio/video should automatically start playing",
    "type": "boolean"
  },
  "charset": {
    "tooltip": "the character encoding used in the external script file",
    "type": "string"
  },
  "checked": {
    "tooltip": "that an input element should be pre-selected when the page loads",
    "type": "boolean"
  },
  "contenteditable": {
    "tooltip": "whether the content of an element is editable or not",
    "type": "boolean"
  },
  "controls": {
    "tooltip": "that audio/video controls should be displayed",
    "type": "boolean"
  },
  "disabled": {
    "tooltip": "that an input element should be disabled",
    "type": "boolean"
  },
  "download": {
    "tooltip": "that the target will be downloaded when a user clicks on the hyperlink",
    "type": "string"
  },
  "draggable": {
    "tooltip": "whether an element is draggable or not",
    "type": "boolean"
  },
  "form": {
    "tooltip": "the form element an input element belongs to",
    "type": "string"
  },
  "height": {
    "tooltip": "the height of an element",
    "type": "string"
  },
  "hidden": {
    "tooltip": "that an element should be hidden",
    "type": "boolean"
  },
  "href": {
    "tooltip": "the URL of the page the link goes to",
    "type": "string"
  },
  "id": {
    "tooltip": "a unique id for an element",
    "type": "string"
  },
  "lang": {
    "tooltip": "the language of the element's content",
    "type": "string"
  },
  "max": {
    "tooltip": "the maximum value for an input element",
    "type": "string"
  },
  "maxlength": {
    "tooltip": "the maximum number of characters allowed in an input element",
    "type": "number"
  },
  "min": {
    "tooltip": "the minimum value for an input element",
    "type": "string"
  },
  "multiple": {
    "tooltip": "that a user can enter multiple values in an input element",
    "type": "boolean"
  },
  "name": {
    "tooltip": "the name of an element",
    "type": "string"
  },
  "pattern": {
    "tooltip": "the pattern (regex) that an input element's value is checked against",
    "type": "string"
  },
  "placeholder": {
    "tooltip": "a short hint that describes the expected value of an input element",
    "type": "string"
  },
  "readonly": {
    "tooltip": "that an input element is read-only",
    "type": "boolean"
  },
  "required": {
    "tooltip": "that an input element must be filled out before submitting a form",
    "type": "boolean"
  },
  "spellcheck": {
    "tooltip": "whether the element is to have its spelling and grammar checked or not",
    "type": "boolean"
  },
  "srcset": {
    "tooltip": "the URL of the image to use in different situations",
    "type": "string"
  },
  "step": {
    "tooltip": "the legal number intervals for an input element",
    "type": "string"
  },
  "style": {
    "tooltip": "an inline CSS style for an element",
    "type": "string"
  },
  "tabindex": {
    "tooltip": "the tab order of an element",
    "type": "number"
  },
  "target": {
    "tooltip": "where to open the linked document",
    "type": "string"
  },
  "title": {
    "tooltip": "extra information about an element",
    "type": "string"
  },
  "value": {
    "tooltip": "the value of an input element",
    "type": "string"
  },
  "width": {
    "tooltip": "the width of an element",
    "type": "string"
  },
}


// ChatGPT generated these descriptions.
const possibleCallbacks = {
  "click": {
    "description": "the item is clicked"
  },
  "input": {
    "description": "the item receives input"
  },
  "dblclick": {
    "description": "the item is double-clicked"
  },
  "mouseenter": {
    "description": "the mouse enters the item"
  },
  "mouseleave": {
    "description": "the mouse leaves the item"
  },
  "mousemove": {
    "description": "the mouse moves over the item"
  },
  "keydown": {
    "description": "a key is pressed down"
  },
  "keyup": {
    "description": "a key is released"
  },
  "submit": {
    "description": "a form is submitted"
  },
  "change": {
    "description": "the item's value changes"
  },
  "focus": {
    "description": "the item receives focus"
  }
};

const elementsObject = {
  "a": {
    "tooltip": "The URL of a linked resource"
  },
  "abbr": {
    "tooltip": "an abbreviation or acronym"
  },
  "address": {
    "tooltip": "the contact information for the author/owner of a document"
  },
  "area": {
    "tooltip": "an area inside an image map"
  },
  "article": {
    "tooltip": "an independent, self-contained content"
  },
  "aside": {
    "tooltip": "content aside from the content it is placed in"
  },
  "audio": {
    "tooltip": "audio content"
  },
  "b": {
    "tooltip": "bold text"
  },
  "base": {
    "tooltip": "a base URL/target for all relative URLs in a document"
  },
  "bdi": {
    "tooltip": "Isolates a part of text that might be formatted in a different direction from other text outside it"
  },
  "blockquote": {
    "tooltip": "a section that is quoted from another source"
  },
  "body": {
    "tooltip": "the document's body"
  },
  "br": {
    "tooltip": "Inserts a single line break"
  },
  "button": {
    "tooltip": "a clickable button"
  },
  "canvas": {
    "tooltip": "Used to draw graphics, on the fly, via scripting (usually JavaScript)"
  },
  "caption": {
    "tooltip": "the caption for a table"
  },
  "cite": {
    "tooltip": "the title of a work"
  },
  "code": {
    "tooltip": "a piece of computer code"
  },
  "col": {
    "tooltip": "attributes for table columns"
  },
  "colgroup": {
    "tooltip": "a group of one or more columns in a table for formatting"
  },
  "data": {
    "tooltip": "Adds a machine-readable translation of a given content"
  },
  "datalist": {
    "tooltip": "a list of pre-defined options for input controls"
  },
  "dd": {
    "tooltip": "the description of the term in a description list"
  },
  "del": {
    "tooltip": "text that has been deleted from a document"
  },
  "details": {
    "tooltip": "additional details that the user can view or hide"
  },
  "dfn": {
    "tooltip": "a term that is going to be defined within the content"
  },
  "dialog": {
    "tooltip": "Defines a dialog box or window"
  },
  "div": {
    "tooltip": "a section in a document"
  },
  "dl": {
    "tooltip": "Defines a description list"
  },
  "dt": {
    "tooltip": "a term in a description list"
  },
  "em": {
    "tooltip": "emphasized text"
  },
  "embed": {
    "tooltip": "a container for an external (non-HTML) application"
  },
  "fieldset": {
    "tooltip": "Groups related elements in a form"
  },
  "figcaption": {
    "tooltip": "a caption for a <figure> element"
  },
  "figure": {
    "tooltip": "self-contained content"
  },
  "footer": {
    "tooltip": "a footer for a document or section"
  },
  "form": {
    "tooltip": "an HTML form for user input"
  },
  "h1": {
    "tooltip": "a level 1 heading"
  },
  "h2": {
    "tooltip": "a level 2 heading"
  },
  "h3": {
    "tooltip": "a level 3 heading"
  },
  "h4": {
    "tooltip": "a level 4 heading"
  },
  "h5": {
    "tooltip": "a level 5 heading"
  },
  "h6": {
    "tooltip": "a level 6 heading"
  },
  "head": {
    "tooltip": "information about the document"
  },
  "header": {
    "tooltip": "a header for a document or section"
  },
  "hr": {
    "tooltip": "a thematic break between paragraphs"
  },
  "html": {
    "tooltip": "the root of an HTML document"
  },
  "i": {
    "tooltip": "italicized text"
  },
  "iframe": {
    "tooltip": "an inline frame"
  },
  "img": {
    "tooltip": "an image"
  },
  "input": {
    "tooltip": "an input control"
  },
  "ins": {
    "tooltip": "a text that has been inserted into a document"
  },
  "kbd": {
    "tooltip": "keyboard input"
  },
  "label": {
    "tooltip": "a label for an input element"
  },
  "legend": {
    "tooltip": "a caption for a <fieldset> element"
  },
  "li": {
    "tooltip": "a list item"
  },
  "link": {
    "tooltip": "the relationship between a document and an external resource"
  },
  "main": {
    "tooltip": "the main content of a document"
  },
  "map": {
    "tooltip": "an image-map"
  },
  "mark": {
    "tooltip": "marked/highlighted text"
  },
  "meta": {
    "tooltip": "metadata about an HTML document"
  },
  "meter": {
    "tooltip": "a scalar measurement within a known range"
  },
  "nav": {
    "tooltip": "navigation links"
  },
  "noscript": {
    "tooltip": "Defines an alternate content for users that do not support client-side scripts"
  },
  "object": {
    "tooltip": "an embedded object"
  },
  "ol": {
    "tooltip": "an ordered list"
  },
  "optgroup": {
    "tooltip": "a group of related options in a drop-down list"
  },
  "option": {
    "tooltip": "an option in a drop-down list"
  },
  "output": {
    "tooltip": "the result of a calculation"
  },
  "p": {
    "tooltip": "a paragraph"
  },
  "param": {
    "tooltip": "a parameter for an object"
  },
  "picture": {
    "tooltip": "a container for multiple image resources"
  },
  "pre": {
    "tooltip": "preformatted text"
  },
  "progress": {
    "tooltip": "Represents the progress of a task"
  },
  "q": {
    "tooltip": "a short quotation"
  },
  "rp": {
    "tooltip": "what to show in browsers that do not support ruby annotations"
  },
  "rt": {
    "tooltip": "an explanation/pronunciation of characters (for East Asian typography)"
  },
  "ruby": {
    "tooltip": "a ruby annotation (for East Asian typography)"
  },
  "s": {
    "tooltip": "text that is no longer correct"
  },
  "samp": {
    "tooltip": "sample output from a computer program"
  },
  "script": {
    "tooltip": "a client-side script"
  },
  "section": {
    "tooltip": "a section in a document"
  },
  "select": {
    "tooltip": "a drop-down list"
  },
  "small": {
    "tooltip": "smaller text"
  },
  "source": {
    "tooltip": "multiple media resources for media elements (<video> and <audio>)"
  },
  "span": {
    "tooltip": "a section in a document"
  },
  "strong": {
    "tooltip": "strong text"
  },
  "style": {
    "tooltip": "style information for a document"
  },
  "sub": {
    "tooltip": "subscripted text"
  },
  "summary": {
    "tooltip": "a visible heading for a <details> element"
  },
  "sup": {
    "tooltip": "superscripted text"
  },
  "svg": {
    "tooltip": "a container for SVG graphics"
  },
  "table": {
    "tooltip": "a table"
  },
  "tbody": {
    "tooltip": "Groups the body content in a table"
  },
  "td": {
    "tooltip": "a cell in a table"
  },
  "template": {
    "tooltip": "a container for content that should be hidden when not in use"
  },
  "textarea": {
    "tooltip": "a multiline input control"
  },
  "tfoot": {
    "tooltip": "Groups the footer content in a table"
  },
  "th": {
    "tooltip": "a header cell in a table"
  },
  "thead": {
    "tooltip": "Groups the header content in a table"
  },
  "time": {
    "tooltip": "a date/time"
  },
  "title": {
    "tooltip": "the document's title"
  },
  "tr": {
    "tooltip": "a row in a table"
  },
  "track": {
    "tooltip": "text tracks for media elements (<video> and <audio>)"
  },
  "u": {
    "tooltip": "text with a stylistic alternate"
  },
  "ul": {
    "tooltip": "an unordered list"
  },
  "var": {
    "tooltip": "a variable"
  },
  "video": {
    "tooltip": "a video"
  },
  "wbr": {
    "tooltip": "a possible line-break"
  }
};
