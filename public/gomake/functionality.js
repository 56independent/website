// ChatGPT made this code
function downloadText(text, filename, type="text/plain") {
    const blob = new Blob([text], { type: type});

    // Create a download link element
    const link = document.createElement('a');
    link.href = URL.createObjectURL(blob);
    link.download = filename;

    // Append the link to the document and trigger the download
    document.body.appendChild(link);
    link.click();

    // Clean up by removing the link element
    document.body.removeChild(link);


}

function downloadJSON(jsonData, filename) {
    // Create a Blob object from the JSON data
    string = /*"// A gomake project. For best results, don't edit from a text editor. For worst results, encode to Baudot code\n\n " +*/ JSON.stringify(jsonData, null, 4) // TODO: Ask all software in the world to change JSON to allow comments

    downloadText(string, filename, "application/json")
}

// ChatGPT made this code
function editSubobject(obj, targetKey, newValue) {
    if (typeof obj === 'object' && obj !== null) {
      for (let key in obj) {
        if (key === targetKey) {
          obj[key] = newValue;
        } else if (typeof obj[key] === 'object' && obj[key] !== null) {
          editSubobject(obj[key], targetKey, newValue);
        }
      }
    }
}

function getComponentList(obj) {
  let keys = [];

  function traverse(obj) {
    if (typeof obj === "object") {
      for (let key in obj) {
        keys.push(key);
        if (obj[key] && obj[key].hasOwnProperty("contains")) {
          traverse(obj[key].contains);
        }
      }
    }
  }

  traverse(obj);
  return keys;
}

function createDefaultProject() {
    PROJECT.settings.currentObj = "Main"

    PROJECT.editors[PROJECT.settings.currentObj] = {
        "container":{
            "type":"div",
            "class":"container",
            "contains":{
                "row":{
                    "type":"div",
                    "class":"row",
                    "contains":{
                        "title":{
                            "type":"p",
                            "content":"New project."
                        }
                    }
                },
                "rowf":{
                    "type":"div",
                    "class":"row",
                    "contains":{
                        "button":{
                            "type":"button",
                            "content":"Assign an onclick to this thing",
                            "class":["btn", "btn-primary"],
                            "code":{
                                "click":"console.log(\"Hello, world!\")"
                            }
                        }
                    }
                }
            }
        }
    }

    openEditor(PROJECT.editors["Main"])
}

function openProject(project) {
    PROJECT = {}
    PROJECT = project

    openEditor("component", PROJECT.editors[PROJECT.settings.currentObj])
}


// I programmed it, and asked ChaGPT to make it work after implementing scripting
function renderHTML(components) {
    let html = `
      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="../favicon.ico">

        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.7/dist/umd/popper.min.js" integrity="sha384-zYPOMqeu1DAVkHiLqWBUTcbYfZ8osu1Nd6Z89ify25QV9guujx43ITvfi12/QExE" crossorigin="anonymous"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
      </head>
      <body>
    `;
  
    let script = "<script>";
  
    function renderItems(components) {
      let renderedHtml = ""; // New variable to store the rendered HTML
  
      for (let component in components) {
        if (components.hasOwnProperty(component)) {
          renderedHtml += "<" + components[component].type + " id=\"" + component + "\" ";
  
          for (let parameter in components[component]) {
            if (
              parameter !== "contains" &&
              parameter !== "classes" &&
              parameter !== "code"
            ) {
              renderedHtml += parameter + "=\"" + components[component][parameter] + "\" ";
            } else if (parameter === "classes") {
              renderedHtml += "class=\"";
  
              for (let i = 0; i < components[component].classes.length; i++) {
                renderedHtml += components[component].classes[i] + " ";
              }
  
              renderedHtml += "\" ";
            }
          }
  
          renderedHtml += ">";
  
          if (components[component].hasOwnProperty("code")) {
            for (let callback in components[component].code) {
              script += "\n$(\"#" + component + "\").on(\"" + callback + "\", () => {\n";
              script += components[component].code[callback];
              script += "\n});\n";
            }
          }
  
          if (components[component].hasOwnProperty("content")) {
            renderedHtml += components[component].content;
          }
  
          if (components[component].hasOwnProperty("contains")) {
            renderedHtml += renderItems(components[component].contains);
          }
  
          renderedHtml += "</" + components[component].type + ">";
        }
      }
  
      return renderedHtml; // Return the rendered HTML
    }
  
    html += renderItems(components);
  
    script += "</script>";
    html += script;
    html += "</body>";
  
    return html;
  }
  
  