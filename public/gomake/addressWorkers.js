
// ChatGPT made this code
function findObjectAddressByName(obj, name, address = '') {
    if (!obj || typeof obj !== 'object') {
      return null;
    }
    
    if (obj[name]) {
      return address;
    }
    
    for (var key in obj) {
      if (obj.hasOwnProperty(key) && typeof obj[key] === 'object') {
        var result = findObjectAddressByName(obj[key], name, address + key + '.');
        if (result) {
          return result;
        }
      } else if (key === "contains" && typeof obj[key] === 'object') {
        var containsResult = findObjectAddressByName(obj[key], name, address + key + '.');
        if (containsResult) {
          return containsResult;
        }
      }
    }
    
    return null;
}

function getDataByAddress(obj, address) {
    if (!obj || typeof obj !== 'object' || !address) {
      return null;
    }
    
    var keys = address.split('.'); // Split the address by dots to get the keys
    
    var currentObj = obj;
    for (var i = 0; i < keys.length; i++) {
      var key = keys[i];
      if (currentObj.hasOwnProperty(key)) {
        currentObj = currentObj[key];
      } else {
        return null; // Key not found, return null
      }
    }
    
    return currentObj;
}
function setDataByAddress(obj, address, newData) {
  if (!obj || typeof obj !== 'object' || !address) {
    return;
  }

  var keys = address.split('.'); // Split the address by dots to get the keys

  var currentObj = obj;
  for (var i = 0; i < keys.length - 1; i++) {
    var key = keys[i];
    if (currentObj.hasOwnProperty(key)) {
      currentObj = currentObj[key];
    } else {
      return; // Key not found, do nothing
    }
  }

  var lastKey = keys[keys.length - 1];
  currentObj[lastKey] = newData;

  return obj; // Return the modified object
}
