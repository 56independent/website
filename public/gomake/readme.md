# Introduction
Gomake has one simple goal:

> Make software design approachable to all, regardless of programming skill.

Gomake achieves that goal by building abstraction above a basic text editor.

# Special Thanks
I thank the developers of the following products for their efforts, ensuring i don't have to reinvent a wheel:

* Jquery, for providing the functions needed to ensure this project got to the stage it has
* Excalidraw, for providing a sketch editor
* Blockly, for providing the block-based JS editor
* Ace, for providing the text-based JS editor
