$("#project-create").click(() => {
    createDefaultProject()
})

$("#add-component").click(() => {
    type = $("#add-component-dialog-type")
    parent = $("#add-component-dialog-parent")

    possibleParents = getComponentList(PROJECT.editors[PROJECT.settings.currentObj])

    for (element in elementsObject){
        type.append(
            $("<option>")
                .attr("value", element)
                .text((PROJECT.settings.advancedMode) ? element : elementsObject[element].tooltip)
        )
    }

    for (i = 0; i < possibleParents.length; i++) {
        parent.append(
            $("<option>")
                .attr("value", possibleParents[i])
                .text(possibleParents[i])
        )
    }

    $("#add-component-dialog").show()
})

$("#add-scene").click(() => {
    $("#add-scene-dialog").show()
})
$("#add-component-we-done").click(() => {
    id =$("#add-component-dialog-name").val()
    type = $("#add-component-dialog-type").val()
    parent = $("#add-component-dialog-parent").val()
    
    address = findObjectAddressByName(PROJECT.editors[PROJECT.settings.currentObj], parent) || ""

    newComponent = {
        "type": type,
    }

    if (address.length == 0) {
        $("#add-component-dialog").append($("<p>").text("Parent not found!"))
    } else {
        console.log(address + parent + ".contains." + id)

        PROJECT.editors[PROJECT.settings.currentObj] = setDataByAddress(PROJECT.editors[PROJECT.settings.currentObj], address + parent + ".contains." + id, newComponent)
        openEditor(PROJECT.editors[PROJECT.settings.currentObj])
    }
})

$("#add-scene-we-done").click(() => {
    sceneName = $("#add-scene-dialog-name").val()

    PROJECT.editors[sceneName] = {
        "title":{
            "type":"h1",
            "content":"This is a new scene, named " + sceneName + "."
        }
    }

    PROJECT.settings.currentObj = sceneName

    openEditor(PROJECT.editors[sceneName])

    $("#add-scene-dialog").hide()
})

$("#remove-component-button").click(() => {
    console.log("clicked")

    toRemove = $("#remove-component-input").val()

    address = findObjectAddressByName(PROJECT.editors[PROJECT.settings.currentObj], toRemove) || ""
    addressForContainer = address.replace(/\.$/, "")

    console.log("removing " + toRemove + " from " + address + toRemove)

    console.log(addressForContainer)

    contains = getDataByAddress(PROJECT.editors[PROJECT.settings.currentObj], addressForContainer)

    containsWithoutDeleted = {}

    for (thing in contains){
        if (thing != toRemove) {
            containsWithoutDeleted.push(contains[thing])
        }
    }

    console.log(containsWithoutDeleted)

    PROJECT.editors[PROJECT.settings.currentObj] = setDataByAddress(PROJECT.editors[PROJECT.settings.currentObj], addressForContainer, containsWithoutDeleted)

    openEditor(PROJECT.editors[PROJECT.settings.currentObj])
})

$("#editor-open").click(() => {
    console.log("clicked")
    var input = document.createElement('input');
    input.type = 'file';
    
    input.onchange = e => { 
    
       // getting a hold of the file reference
       var file = e.target.files[0];
    
       // setting up the reader
       var reader = new FileReader();
       reader.readAsText(file,'UTF-8');
    
       // here we tell the reader what to do when it's done reading...
       reader.onload = readerEvent => {
          var content = readerEvent.target.result; // this is the content!
          content = JSON.parse(content)
          console.log(content)

          openProject(content)
       }
    
    }
    
    input.click();
})

$("#editor-save").click(() => {
    downloadJSON(PROJECT, "gomake-project.json")
})

$("#editor-componentView").click(() => {
    $("#view").empty()
    drawComponents(PROJECT.editors[PROJECT.settings.currentObj])
})

$("#editor-sketchDesigner").click(() => {
    $("#view")
        .empty()
        .append($("<iframe>")
            .attr("src", "https://excalidraw.com")
                .css({
                    "width": "100%",
                    "height": "75vh"
                })
        )
})

$("#editor-export").click(() => {
    for (editor in PROJECT.editors) {
        downloadText(renderHTML(PROJECT.editors[editor]), editor + ".html", "text/html")
    }
})

$("#editor-switch").click(function() {
    PROJECT.settings.advancedMode = ! PROJECT.settings.advancedMode

    if (PROJECT.settings.advancedMode) {
        $(this).text("Switch to begginer mode")
    } else {
        $(this).text("Switch to advanced mode")
    }

    openEditor(PROJECT.editors[PROJECT.settings.currentObj])
})