writeTable = (array) => {
    let table = $("<table>").attr("class", "table");
    
    let container = $("<thead>"); // Changed from an empty string to a thead element
    let thing = "<th>"; // Added declaration for the variable 'thing'
    
    for (let i = 0; i < array.length; i++){
        if (i == 1){
            table.append(container); // Added the container to the table before overwriting it
            
            container = $("<tbody>");
            thing = "<td>";
        }
        
        let row = $("<tr>"); // Added declaration for the variable 'row'

        for (let o = 0; o < array[i].length; o++){ // Added declaration for the variable 'o'
            row.append(
                $(thing).html(array[i][o])
            );
        }

        container.append(row);

        row = $("<tr>"); // Assigned a new jQuery object to 'row'
    }

    table.append(container);
    table.attr("id", "writtenTable");

    return table; // Added the return statement to return the created table
}

function sendError(error) {
    $("#error").text("Error: " + error)
    console.error(error)
}


// ChatGPT generated the bodies of the following functions
function copyToClipboard(text) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
}

function putInURL(arg, data) {
    var currentUrl = new URL(window.location.href);
    var params = new URLSearchParams(currentUrl.search);
    params.delete(arg);

    params.append(arg, data);

    var newUrl = currentUrl.origin + currentUrl.pathname + '?' + params.toString();
    window.history.replaceState(null, null, newUrl);
}

function getFromURL(arg) {
    const urlParams = new URLSearchParams(window.location.search);
    var data = urlParams.get(arg) || "";

    return data
}

function deleteArgFromURL(arg) {
    var currentUrl = new URL(window.location.href);
    var params = new URLSearchParams(currentUrl.search);
    params.delete(arg);
}