
// This code is avauilanle under the MIT license and all text under CC-SA

$(document).ready(function() {
    let projects = [
        {
            "title":"JSONQuizzer",
            "description":"A small application which lets you be quizzed and edit those quizzes, using a JSON storage backend.",
            "longDescription":`<p>
                This program was created from my need to revise and the superiority of being able to quiz yourself on subjects. This program replicates services such as Memrise and Quizlet but works purely from the terminal, broadening the possibly reach with external software.
            </p><p>
                JSONEditor opens and gives you a simple interface which lets you choose between editing or writing mode. From here, you can enter either mode and after a few prompts to automatically find and show the subjects and packs you have, you can open one.
            </p><p>
                From the editing prompt, you can enter a series of commands which are automatically parsed and lead into a clean, easy interface. You can save your work and have your questions all written down for you
            </p><p>
                The program also offers a simple interface for being asked questions, where you must type the answer (with automatic detection even if a fragment is given) from a small selection. Your times are recorded into the question database which lets external programs track your progress.
            </p><p>
                The entire program already comes with some packs to get started quickly.
            </p>`,
            "skillsLearnt":["Reading JSON in python", "File operations in Python", "How to create nice TUIs"],
            "link":"https://gitlab.com/56independent/flashquizzr"
        },
        {
            "title":"Plot",
            "description":"A Lisp-like language focused on being as minimalist as possible; everything is a function (Standard library must be implemented by compiler-creator; this has not been done with mine",
            "longDescription":`<p>
                This language was created out of the desire to produce a minimalist programming language designed to be simple to learn, pushing effort onto the fnctions.
            </p><p>
                The language features two main constructs: the function, delimited by curly braces, and the list, delimited by bracket. Every function takes a list as input. A list can contain 0 to infinity items, inclusive.
            </p><p>
                The language features no hard-coded standard library. Instead, the standard library is merely a small file in the interpreter directory
            </p><p>
                It also has a custom-built IDE, available <a href="https://plot-ide-56independent-ddf58a757c55d2717a37c165f9abddbceef41f09.gitlab.io/public/">online</a>.
            </p>`,
        "skillsLearnt":["Interpreters", "Designing languages"],
            "link":"https://gitlab.com/56independent/plot-ide/"
        },
        {
            "title":"FOSS MM",
            "description":"A DAW with an ambition to superseede most other DAWs",
            "longDescription":`<p>
                The FOSS MM DAW was born out of the realisation
                </p>`,
        "skillsLearnt":["Creating OOP-based apps", "The iterative design approach"],
            "link":"https://gitlab.com/56independent/fossmm"
        },
        {
            "title":"56i-trains",
            "description":"A fork of Advtrins which currently adds high speeds, with more features planned",
            "longDescription":`<p>
                56i-trains is a fork of a popular Minetest Mod. The mod has quite a large codebase and taught me how to contribute to large git repos
            </p><p>
                56i-trains allows higher speeds to be achieved as well as introduces more begginer-friendly functionality. It also plans to add some in-game documentation.
            </p>`,
        "skillsLearnt":["Contributing to external codebases", "Interfacing with the minetest API"],
            "link":"about:none"
        },
        {
            "title":"NewSearch",
            "description":"A tag-based way to search the web. Add tags and get cool websites",
            "longDescription":`TO BE FILLED`,
            "skillsLearnt":["Backends with NodeJS", "Javascript", "Jquery", "Finding subarrays in arrays", "Effective ChatGPT prompting"],
            "link":"https://gitlab.com/56independent/newSearch"
        },
        {
            "title":"Portfolio",
            "description":"This place, programmed using Bootstrap and Jquery",
            "longDescription":`
            <p>This webpage started out as a static bootstrap page with little on it, but then i kept expanding it.</p>
            <p>On this page, there are <span id="pageCount"></span> projects, with all their informationn being written dynamically from a simple array of objects, making maintenence easier</p>
            <p>I firmly believe that portfolio pages should be pages worth of a portfolio entry.</p>
            https://plot-ide-56independent-ddf58a757c55d2717a37c165f9abddbceef41f09.gitlab.io/public/
            <script>
                // Your script logic to calculate the page count
                var pageCount = $(".project").length;

                // Update the content of the span element with the page count
                $("#pageCount").text(pageCount);
            </script>`,
    "skillsLearnt":["Bootstrap", "HTML", "CSS", "Gitlab Pages"],
    "link":"index.html"
        },
        {
            "title":"Brainfuck interpreter",
            "description":"A basic brainfuck interpreter, with plans to expand to my own language and ACTUALLY WORK MY BRAIN IS FUCKED",
            "longDescription":`
            <p>It is the middle of the night i do not want to write this description</p>`,
    "skillsLearnt":["Interpretation", "Making dynamic content on a static base (instead of my previous idea of making the content using only jquery)"],
    "link":"https://gitlab.com/56independent/website/-/tree/master/public/brainfuck"
        }
    ]

    function animate(selector, delay, animationLength) {
        // Set initial offset and delay values
    
        delay = delay || 200;
        animationLength = animationLength || 250;

        // Select the elements to slide in
        var elements = $(selector);
    
        // Set initial CSS properties for the elements
        elements.css({
        "opacity": 0
        });
    
        // Loop through each element and apply the slide-in effect
        elements.each(function(index) {
        var element = $(this);
    
        // Calculate the delay for each element
        var currentDelay = index * delay;
    
        // Apply animation with delay
        setTimeout(function() {
            element.animate({
            "opacity": 1
            }, animationLength); // Adjust the duration as needed
        }, currentDelay);
        });
    };

    function writeList(list, unordered){
        unordered = typeof unordered === 'undefined' ? true : unordered;;

        console.log(list)

        let listBody = $("<ul>")

        if (!unordered){
            listBody = $("<ol>")
        }

        for (i = 0; i < list.length; i++){
            listBody.append(
                $("<li>")
                    .text(list[i])
            )
        }

        return listBody.prop("outerHTML")
    }

    function makeCard(cardInfo, parent) {
        let buttonId = cardInfo.title.replace(" ", "_") + "-button";
        let less = "Less information"
        let more = "More information"

        let card = $("<div>")
            .addClass("card project")
            .append(
                $("<div>")
                .addClass("card-body")
                .append(
                    $("<h5>")
                        .addClass("card-title")
                        .text(cardInfo.title),
                    $("<p>")
                        .addClass("card-text")
                        .text(cardInfo.description),
                    $("<a>")
                    .attr("href", cardInfo.link)
                    .text(cardInfo.link),
                    $("<br>"),
                    $("<button>")
                        .text(more)
                        .attr("id", buttonId)
                        .addClass("btn btn-primary mt-auto")
                )
                .attr("id", buttonId + "-body")
                .children()
                .addClass("m-2")
            );

        parent.on("click", "#" + buttonId, function() {
            console.log("clicked! ", buttonId);
            button = $("#" + buttonId)

            if (button.text() == more) {
                button.text(less);
                $("#" + buttonId + "-long").slideDown(500); // Show the hidden long description container
            } else if (button.text() == less) {
                button.text(more);
                $("#" + buttonId + "-long").slideUp(300); // Show the hidden long description container
            }


        });

        let description = cardInfo.longDescription
        description = description //+ writeList(cardInfo.skillsLearnt) //TODO: Fix this!

        //console.log(writeList(cardInfo.skillsLearnt))

        console.log(cardInfo.skillsLearnt)

        let longDescriptionContainer = $("<div>") // Create a hidden container for the long description
            .attr("id", buttonId + "-long")
            .hide()
            .html(description);

        card.append(longDescriptionContainer); // Append the hidden container to the card

        parent.append(card);
    }


    let placeForCards = $("#portfolio")

    for (i = 0; i < projects.length; i++) {
        if (i == 0 || i%3 == 0) {
            row = $("<div>").addClass("row")
        }

        col = $("<div>").addClass("col")

        row.append(col.append(makeCard(projects[i], col)))
        placeForCards.append(row)
    }

    animate("body", 200, 1000)
    animate(".card", 100, 200)
})
