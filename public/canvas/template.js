// use this to start a new project!

function runX() {
    // width="500" height="300"

    addHelp("<p>Documentation here</p>")

    ctx = prepareCanvas()

    // Add globals here!

    requestAnimationFrame(platformerAnimate);

    addButtonsToX()
    handleKeypressesForX()

    function platformerAnimate() {
        const canvas = $("#place")[0]
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        // Render the game here!

        requestAnimationFrame(platformerAnimate);
    }

    function handleKeypressesForX() {
        $(document).on("keypress", (e) => {
            if (e.key === " " || e.key === "Spacebar") {

            }
            // Add keys here!
        });


        document.activeElement.blur() // Prevent space from hurting gameplay
    }

    function addButtonsToX() {
        if ($("#xButton").text().length == 0) {
            $("#controlsArea")
                .append(
                    $("<button>")
                        .text("jump")
                        .attr("class", "btn btn-primary")
                        .attr("id", "xButton")
                    )
        }

        $("#xButton").click(() => {
        })

        // Add more button logic as neccesary
    }
}
