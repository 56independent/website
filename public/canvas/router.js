function runRouter(){ // Todo: find a way to abstract into a function
    // width="500" height="300"

    /*
    Client requirements:

        I want you to have a go at a routing algorithm. I'm going to recommend Scratch or Processing as easy ways to do the graphics. 

        The eventual goal is to randomly generate points on the screen and have a little car (or other sprite) efficiently route between the nodes you have generated. This is just a 'give it a go' assignment.
    */

    // Thanks for https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array#2450976

    new p5()

    AMOUNT_OF_POINTS = 10
    ROUTE_FIND_ITERATIONS = 100000 // Enough iterations will give the perfect route; TODO: Dynamically calculate

    DRIVE = false
    DRIVE_ROUTE = []
    let DRIVE_ROUTE_INDEX = 0
    DRIVE_LOCATION = []

    DEFAULT_HELP_TEXT = "<p>Press r to find a new route, d to drive it, and p to shuffle nodes. Press e to edit values</p>"

    SECOND_DELAY_BETWEEN_DRIVING = 0.3

    function shuffleList(array) {
      let currentIndex = array.length,  randomIndex;

      // While there remain elements to shuffle.
      while (currentIndex > 0) {

        // Pick a remaining element.
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        // And swap it with the current element.
        [array[currentIndex], array[randomIndex]] = [
          array[randomIndex], array[currentIndex]];
      }

      return array;
    }

    function calculateDistance(x1, y1, x2, y2) {
        const deltaX = x2 - x1;
        const deltaY = y2 - y1;
        
        // Using the Pythagorean theorem to find the straight-line distance
        const distance = Math.sqrt(deltaX ** 2 + deltaY ** 2);
        
        return distance;
    }


    function throwRandomNodeLocations(amount, bottomCoords, topCoords) {
        let nodeLocations = []

        for (let i = 0; i < amount; i++) {
            // Find co-ordinates within the specified range
            randomX = Math.random()*(topCoords[0]-bottomCoords[0])
            randomY = Math.random()*(topCoords[1]-bottomCoords[1])
            
            // Move them to fit into our area
            okX = randomX+bottomCoords[0]
            okY = randomY+bottomCoords[1]
            
            nodeLocations.push([okX, okY])
        }
        
        return nodeLocations
    }

    function drawLocations(locations){
        let NODE_SIZE = 20

        for (node of locations) {
            circle(node[0], node[1], NODE_SIZE)
        }
    }

    let nodeLocations = []
    let canvasDivName = "canvasDiv"
    let myCanvas
    let route = []

    function setup() {
        $("#" + canvasDivName).empty() // TODO: Isn't mixing Jquery and p5.js a bit... ew?
        
        myCanvas = createCanvas(500, 300);
        myCanvas.parent(canvasDivName);
        
        nodeLocations = throwRandomNodeLocations(AMOUNT_OF_POINTS, [10, 10], [490, 290])
        drawLocations(nodeLocations)
        console.log(nodeLocations)
        
        frameRate(50)
        
        addHelp(DEFAULT_HELP_TEXT)
        
        DRIVE = false
        DRIVE_ROUTE = []
        let DRIVE_ROUTE_INDEX = 0
        DRIVE_LOCATION = []
        drawRoute(newRoute(nodeLocations, ROUTE_FIND_ITERATIONS))
    }

    function draw() { // TODO: Ever since starting the functon, draw won't work.
        if (DRIVE) {
            clear()
            drawLocations(nodeLocations)
            drawRoute(DRIVE_ROUTE)
            
            if (frameCount%(50*SECOND_DELAY_BETWEEN_DRIVING) == 0) {
                console.log(DRIVE_ROUTE_INDEX, DRIVE_LOCATION, DRIVE_ROUTE[DRIVE_ROUTE_INDEX])
                DRIVE_ROUTE_INDEX += 1

                if (DRIVE_ROUTE.length == DRIVE_ROUTE_INDEX) {
                    DRIVE_ROUTE_INDEX = 0
                }

                DRIVE_LOCATION = DRIVE_ROUTE[DRIVE_ROUTE_INDEX]
                
            }
            
            RECTANGLE_WIDTH = 50
            rect(DRIVE_LOCATION[0]-(RECTANGLE_WIDTH*0.5), DRIVE_LOCATION[1]-(RECTANGLE_WIDTH*0.5), RECTANGLE_WIDTH)
        }
    }


    function newRoute(locations, iterations=500) {    
        smallestTotalDistance = Infinity
        route = []
        

        for (i = 0; i < iterations; i++) {
            let ourDistance = 0
            let ourRoute = []
            locations = shuffleList(locations)
            
            for (node of locations) {
                if (ourRoute[ourRoute.length-1]) {
                    distanceFrom = calculateDistance(ourRoute[ourRoute.length-1][0], ourRoute[ourRoute.length-1][1], node[0], node[1])
                    ourDistance += distanceFrom
                }

                ourRoute.push(node)
            }
            
            if (ourDistance < smallestTotalDistance) {
                route = ourRoute
                smallestTotalDistance = ourDistance
            }
            
            //addHelp("Evaluating route of distance " + ourDistance + ", formed of nodes " + ourRoute + "</p>")
        }
        
        //addHelp(DEFAULT_HELP_TEXT)
        
        return route
    }

    function drawRoute(route) {
        DRIVE_ROUTE = route
        noFill()
        beginShape()
        for (node of route) {
            vertex(node[0], node[1])
        }
        endShape(CLOSE)
    }

    function drive(route) {
        DRIVE = true
        DRIVE_LOCATION = route[0]
    }

    function handleKeyPress(event) {
        console.log("Key!")
        switch (event.key) {
            case "r":
                route = newRoute(nodeLocations, ROUTE_FIND_ITERATIONS)
                
                clear()
                drawLocations(nodeLocations)
                drawRoute(route)
                break
            case "d":
                drive(route || newRoute(nodeLocations))
                break
            case "p":
                setup()
                break
            case "e":
                AMOUNT_OF_POINTS = prompt("How many points to add?")
                ROUTE_FIND_ITERATIONS = prompt("How many iterations before finding objective correct route (one million needed for 10 nodes; set to 100,000 for performance)")
                setup()
                break

        }
    }
    
    setup()
    $(document).on("keypress", handleKeyPress);
}

