// An array containing platform heights

function runJumper() {
    // width="500" height="300"

    
    addHelp("<p>Player one uses WASD and player two uses Arrows.</p>")
    
    ctx = prepareCanvas()

    singlePlayer = false
    boardHeight = 1000
    boardWidth = 300

    platforms = [[0,0], [0,100], [0,200]]
    
    for (i=0; i<100; i++) {
        var y = Math.floor(Math.random()*boardHeight)
        var x = Math.floor(Math.random()*boardWidth)

        platforms.push([x, y]) // Height is relative to the top of the board; makes things simple.
    }

    players = [
        {
            "height":1000,
            x: 150,
            "jumping":false,
            "ySpeed":0,
            "xSpeed":0,
            "yTarget":0,
            "xTarget":0,
            "playStart":100
        }
    ]

    if (! singlePlayer){
        // Multiplayer activate!
        players[1] = { ...players[0] };
    } else {
        // You're playing with AI!
        players[1] = { ...players[0] };
        players[2] = { ...players[0] };
    }
    
    function drawImage(imageName, coords) {
        
        var image = new Image();
        
        image.src = "jump/" + imageName;
        
        image.onload = function() {
            ctx.drawImage(image, coords[0], coords[1], 10, 10);
        };
    }

    function renderPlayField(platforms, yHeightOfPlayer, xOffset) {
        yHeightRelativeToTop = boardHeight-yHeightOfPlayer

        for (i=0; i < platforms.length; i++) {
            if (platforms[i][1] <= yHeightRelativeToTop-100 || platforms[i][1] >= yHeightRelativeToTop+100) {
                visualHeight = 150+(platforms[i][1]-yHeightOfPlayer) //The player shows in the middle of the screen.

                drawImage("grass.png", [platforms[i][0]+xOffset-10, visualHeight])
                drawImage("grass.png", [platforms[i][0]+xOffset, visualHeight])
                drawImage("grass.png", [platforms[i][0]+xOffset+10, visualHeight])
            }
        }
    }
    requestAnimationFrame(platformerAnimate);
    handleKeypressesForJumper()

    function platformerAnimate() {
        const canvas = $("#place")[0]
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        renderPlayField(platforms, players[0].height, 100)

        for (i=0; i < players.length; i++) {
            // Render each player as a rectangle of differing colours; the player who "owns" the render is the controller.

            playStart = (boardHeight*3)+100

            ctx.fillRect(platStart+players[i].x)
        }

        requestAnimationFrame(platformerAnimate);
    }

    function handleKeypressesForJumper() {
        $(document).on("keypress", (e) => {
            if (e.key === "a") {
                players[1].xTarget = -30
            } else if (e.key === "d") {
                players[1].xTarget = 30
            } else if (! singlePlayer) {
                // Arrow key rendering
            }
            // Add keys here!
        });


        document.activeElement.blur() // Prevent space from hurting gameplay
    }
}
