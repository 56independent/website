/*
Each x is 10 pixels wide

    0  10  20

0  |  |  |  |
   | x| x| x|
10 |  |  |  |
   | x| x| x|
20 |  |  |  |
   | x| x| x|

Tetriminos are drawn according to the super rotation system, see:

https://cdn.wikimg.net/en/strategywiki/images/7/7f/Tetris_rotation_super.png

*/

tetrinimos = {
    "t":{
        "shape1":[0, 10, 30, 10],
        "shape2":[10, 0, 10, 10],
        "rotationCore":[15, 15],
        "colour":"purple"
    },
    "l":{
        "shape1":[],
        "shape2":[],
        "rotationCore":[],
        "colour":"orange"
    },
    "j":{
        "shape1":[],
        "shape2":[],
        "rotationCore":[],
        "colour":"blue"
    },
    "z":{
        "shape1":[],
        "shape2":[],
        "rotationCore":[],
        "colour":"red"
    },
    "s":{
        "shape1":[],
        "shape2":[],
        "rotationCore":[],
        "colour":"green"
    },
    "line":{
        "shape1":[],
        "shape2":[],
        "rotationCore":[],
        "colour":""
    },
    "box":{
        "shape1":[],
        "shape2":[],
        "rotationCore":[],
        "colour":""
    }
}

function runTetris() {
    ctx = prepareCanvas()

    drawTetrinimio(tetrinimos.t, ctx, [100, 100], Math.PI/2)
}

function drawTetrinimio(tetrinimo, canvas, [offsetX, offsetY], rotationRadians) {
    canvas.fillStyle = tetrinimo.colour

    realx = tetrinimo.shape1[0]+offsetX
    realy = tetrinimo.shape1[1]+offsetY

    canvas.fillRect(realx, realy, tetrinimo.shape1[2], tetrinimo.shape1[3])

    canvas.translate(offsetX+tetrinimo.rotationCore[0], offsetY+tetrinimo.rotationCore[1])
    canvas.rotate(rotationRadians)
    canvas.translate(-(offsetX+tetrinimo.rotationCore[0]), -(offsetY+tetrinimo.rotationCore[1]))

    realx = tetrinimo.shape2[0]+offsetX
    realy = tetrinimo.shape2[1]+offsetY

    canvas.fillRect(realx, realy, tetrinimo.shape2[2], tetrinimo.shape2[3])

    canvas.translate(offsetX+tetrinimo.rotationCore[0], offsetY+tetrinimo.rotationCore[1])
    canvas.rotate(rotationRadians)
    canvas.translate(-(offsetX+tetrinimo.rotationCore[0]), -(offsetY+tetrinimo.rotationCore[1]))

}