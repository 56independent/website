// Key-value pairs of what buttons we can press

function addHelp(html) {
    $("#helpArea").empty()
    $("#helpArea").html(html)
}

// As abridged from: https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Basic_usage
function prepareCanvas() {
    const canvas = $("#place")[0]

    if (canvas.getContext) {
        return canvas.getContext("2d");
    }
}

function renderButton(text, command, importance) {
    button = $("<button>")
        .text(text)
        .attr("onclick", command + "()")
        .addClass("btn")
        .addClass("btn-" + importance)
    return button
}

$(document).ready(function () {
    thingsToDo = [
        {
            "name":"Platformer",
            "description":"A rudimentary platformer game designed to teach me the basics of canvas",
            "command":"runPlatformer",
            "button":"primary"
        },
        /*{
            "name":"Tetris",
            "description":"A (failed) tetris clone designed to teach me more about canvas",
            "command":"runTetris",
            "button":"danger"
        },
        {
            "name":"Jumper",
            "description":"A doodle jump clone designed to make fun multiplayer",
            "command":"runJumper",
            "button":"primary"
        },*/
        {
            "name":"Router",
            "description":"A routing appplication for CS made",
            "command":"runRouter",
            "button":"success"
            
        }
        
    ]


    row = $("<tr>")

    for (i=0; i < thingsToDo.length; i++) {
        if (i%15 == 0) {
            $("#buttonBank").append(row)
            row = $("<tr>")
        }

        row.append(
                $("<td>")
                    .append(
                        renderButton(thingsToDo[i].name, thingsToDo[i].command, thingsToDo[i].button)
                    )
                )
        
    }

    $("#buttonBank").append(row)
})
