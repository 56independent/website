function getDocuments() {
    /*
    Template:

    {
        "name":'',
        "description": "",
        "tags": [""],
        "content":`
        `
    },
    */

    markdowns = [
        { // TODO: This is the WORST hack i've ever done
            "name":'-> Click a document from below to load it into the box <-',
            "description": "Restore the default page",
            "tags": ["default", "meta"],
            "content":"This is where the document will show up"
        },
        {
            "name":'Tortuese',
            "description": "The single most mentally damaging conlang ever",
            "tags": ["colangs", "ideas", "cursed"],
            "content":`
Preamble
--------

Tortuese is not a fun nor cool language. It is not designed to be good for your mental health. I kindly request that you refrain from reading this guide as it is likely to cause serious harm to your well-being.

Tortuese features the worst of as many languages as it can take from.

If you are a teacher, feel free to assign Tortuese work to those students who complain about languages. It's perfectly designed to be torture (thus its name).

Basic overview
--------------

Let's first take a look at the language. It's FUGLY. Let's start with the writing system. Instead of a simple alphabet or phonological script, or even the 3 frenemies of the Japanese script, the language uses this:

|     |     |
| --- | --- |
| **Common Letters** |     |
| **IPA symbol** | **Letters** |
| ɑ   | a, ae, ie, 筑 |
| æ   | aé, eà, ll, ب |
| ɔ   | eu, ف, я |
| ɛ   | iii, ш, л |
| e   | oè, 拄, х |
| i   | i, j, д |
| o   | o, и, й |
| u   | u, ñó |
| ə   | ëë, ي |
| p   | п, ṕ |
| b   | p, б |
| t   | t, ط |
| d   | d, Ђ{ |
| k   | k, Ж |
| g   | g, Ђ} |
| ʈ   | t, ف |
| f   | v, ч |
| v   | f, 嘭 |
| ð   | ðð  |
| s   | ي   |
| z   | س   |
| ʃ   | 噶, s |
| ʒ   | d, o |
| h   | زé, ^ħ |
| m   | ḿ   |
| n   | ·n  |
| ŋ   | ng, غ, г |
| l   | q'q |
| ɹ   | r>  |
| j   | j, ج, з |
| w   | ẃ, в |

Note how the alphabet here uses not only multiple spellings for a single sound, it uses multiple writing systems and nonsensical combinations. The writing is so inconsistent it's almost a logography; don't try to learn the alphabet, only the sounds.

Also, note how the table is called “common letters"; there are more letters then this, representing different sounds.

Let's move quickly onto the easiest word class; “describers”. These describe a word and what it does. They must agree in case and gender and plurality with the noun. They inherit the endings of the noun, something called “harmony”. 

And then we have “verbs”. These describe an action. Every vowel has 11 mandatory slots, which all convey their own info. For example, a vowel has a location slot for showing where the verb takes place, a intensity slot to describe how intense the action is, and even a feelings slot for how you feel about it, including an ending only known as “GAE”. Academics still don't understand what it's used for. Every verb belongs to a “group”, which dictates its own endings. Don't try looking for patterns; there aren't any.

And finally, we have nouns. They have a gender, a gender-ending, and multiple case and plurality endings. Case nowdays means nothing, but using it is mandatory. Plurality is on-par with arabic, having a seperate “dual” plurality.

There are particles, which are actually what makes cases exist. 

Entering the weeds
------------------

### Sentence construction

Ok, now that we've had an overview of grammar, let's build a sentence. What about “i love you”? Since the subject and object are verb slots, we can just use the verb root “طيبng”.  It's verb group 6, so we use those endings.

Let's now add endings. See the conjugation diagram:

|     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| **Verb group 6** |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| **Slot 1 (Subject)** |     | **Slot 2 (tense)** |     | **Slot 3 (mood)** |     | **Slot 4 (usless chars)** |     | **Slot 5 (gender)** |     | **Slot 6 (evidentiality)** |     | **Slot 7 (feelings about it)** |     | **Slot 8 (location)** |     | **Slot 9 (intensity)** |     | **Slot 10 (Purpose)** |     | **Slot 11 (Object)** |     |
| _**Meaning**_ | **Ending** | _**Meaning**_ | **Ending** | _**Meaning**_ | **Ending** | _**Meaning**_ | **Ending** | _**Meaning**_ | **Ending** | _**Meaning**_ | **Ending** | _**Meaning**_ | **Ending** | _**Meaning**_ | **Ending** | _**Meaning**_ | **Ending** | **Meaning** | **Ending** | _**Meaning**_ | **Ending** |
| _first_ | 嘭o·n | _past_ | q'qaéЖ | _indicative_ | иllЖ | _1_ | Ђ}uي | _female_ | gшḿ | _You heard_ | p   | _neutral_ | sëëп | _Near_ | Ђ}拄d | _mummur_ | Ђ{ñó | To achieve | 嘭io | _first_ | طllaз |
| _second_ | iiiЖ | _present_ | eàчs | _hypothetical_ | sëëп | _2_ | ẃiẃ | _male_ | чëëч | _I heard_ | 嘭io | _happy_ | dлðð | _Away_ | q'qo | _Extremely low_ | dиẃf | To learn | 筑ṕ  | _second_ | хيd |
| _third_ | Жjðð | _habitual_ | gшḿ | _imperative_ | ЖaЖ | _3_ | гieЖ | _inanimate_ | чëëч | _They heared_ | q'q筑 | _sad_ | q'qaéЖ | _Inside_ | вieЂ} | _Low_ | хيd | To expand | иllЖ | _third_ | p拄  |
| _fourth_ | Жiiiي | _future_ | Жفд | _conditional_ | oèv | _4_ | goèj | _animate_ | 嘭o·n | _We deduced_ | gшḿ | _angry_ | gшḿ | _Outside_ | طëëðð | _Mid low_ | 嘭o·n | To harm | aeЂ{ | _fourth_ | ieЖ |
|     |     |     |     | _infinitive_ | daЂ} | _5_ | 嘭io | _abstract_ | ngëë | _We heard_ | iiiي | _disgusted_ | q'qo | _Here_ | Жjðð | _Middle_ | طëëðð | To protect | زéeuг |     |     |
|     |     |     |     | _gerund_ | Ђ}بбف | _6_ | Жiiiي |     |     | _Nobody knows_ | 嘭o·n | _GAE_ | ëë噶 | _Up_ | iiiЖ | _Mid high_ | ngoèغ | To communicate | ngëë |     |     |
|     |     |     |     |     |     |     |     |     |     | _Rumored_ | aeЂ{ | _Excited_ | яj  | _Down_ | جя  | _High_ | eu嘭 | To relax | gшḿ |     |     |
|     |     |     |     |     |     |     |     |     |     | _It is said_ | Жjðð | _Worried_ | ṕeu噶ar> | _Left_ | Ђ}筑в | _Extreme_ | سoè拄ḿ | To assist | p拄  |     |     |
|     |     |     |     |     |     |     |     |     |     | _It is believed_ | ЖaЖ | _Suprised_ | Ђ}بбف | _Right_ | ḿoèي | _APJEIOHIO_ | Жjðð |     |     |     |     |
|     |     |     |     |     |     |     |     |     |     | _It is assumed_ | uف嘭 | _Hopeful_ | goèj | _Across_ | яu  |     |     |     |     |     |     |
|     |     |     |     |     |     |     |     |     |     | _It is known_ | 嘭o·n | _Guilty_ | ngoèغ |     |     |     |     |     |     |     |     |
|     |     |     |     |     |     |     |     |     |     | _None_ | u   | _Relieved_ | гieЖ |     |     |     |     |     |     |     |     |

Isn't that absolutely terrifying? Don't worry, we can get through this. Let's construct the English morphemes, taking only one meaning from each slot:

> First-habitual-indicative-animate-rumored-excited-near-high-to communicate-second

Wow, what a monster!

Now, let's write down the full verb:

> طيبng嘭o·nЖفд嘭o·naeЂ{яjЂ}拄deu嘭ngëëхيd

And there it is, “it is rumored i always love you and i'm highly excited to say this near you and i'm saying this to communicate this to you” in one beautiful verb.

### Harder sentences

Why don't we try something harder then what we've already done (which does seem difficult but bear with me). Let's try “The cats are eating your food!”

This should be thought of as “cat-noun verb-eating food posessive-particle”.

So, let's build the word for cats:

> فllغjилiegððgfiiijч噶fsхд

And the verb for eating, which is verb group 5:

> third-present-indicative-1-inanimate-it is known-here-mid low-to expand-fourth

Wow! (Fourth is used for objects which are not humans)

> غيi嘭ngëëЂ}بбف嘭ioeàчsЖjððЖiiiيieЖngëë嘭o·npie噶

And finally, the noun and posessive:

> ṕojso·n·ntiṕزé拄ñóяг q'qлيj

Do you feel the pain?

Now, let's mix them alltogether:

> فllغjилiegððgfiiijч噶fsхд غيi嘭ngëëЂ}بбف嘭ioeàчsЖjððЖiiiيieЖngëë嘭o·npie噶 ṕojso·n·ntiṕزé拄ñóяг q'qлيj

Wow… dosen't that fill you with disgust?

### Gender and Case

There are 5 genders in this language:

*   female
*   male
*   inanimate
*   animate
*   abstract

In historical tortuese, the genders actually aligned with the noun, if possible, but centuries of changes later, the gender is meaningless.

The same story applies to the cases:

*   possessive
*   diminutive     
*   instrumental     
*   creative     
*   broken     
*   sad    
*   dual     
*   plural

In historical tortuese, the cases used to allow us to freely place words, but then academics suddenly came over with SVO and decided it was the “pure” word order. Nobody ever freely placed words as it was gramatically incorrect, but cases still existed. And then other academics decided to make sentences focus-last to allow for more “audience retention”, and to do that they made new particles to inflect nouns instead. 

It's the equivalent of changing “Mary eats green eggs and ham” like this:

*   Green eggs and ham, it's what Mary's eating
*   Mary eats green eggs and ham
*   (object:) Green eggs and ham (verb:) is being aten by (subject:) Mary
*   (subject:) Mary, the lady of high complexion, is now (verb:) dining on some exquisite… can you guess it…? (object:) Green eggs and ham!

### Verb construction

Now that we've seen a real example of verbs, let's learn more about the theory behind them.

Every slot is mandatory and required whenever you have a verb, with no choice not to.

let's talk about what each slot means. 

The first few slots, slots 1 to 5 are quite easy to comprehend. With slot one, you insert the subject of the verb, with slot two, the tense, and slot 3 the way the tense is working, from being actual (indicative) to a command (imperative) to something that might happen (hypothetical). Slot 4 is a little more difficult; just use a dice to find the correct chars. Slot 5 relies on the gender of the noun, and we're done with them.

And then we have slot 6. This represents how the verb-writer knows what's happening. Be careful; lying, even without knowing, about evidentiality, is worth “death by community for being gramatically incorrect”, except for in fictional texts.

Slot 7 shows our feeling about the verb. Don't use “GAE”; it's too unkown for even locals to use, and will cause “stoning for being pretentious” by the locals.

Slot 8 shows the location of the verb; use the one closest to what you mean.

Slot 9 shows how intense the action of the verb is, allowing us to prioritise different verbs. Assigning a different intensity then needed will lead to “execution for lying about priority” by the locals.

Slot 10 shows the purpose for the subject doing the verb.

Slot 11 is as simple as slot 1; it's just the thing the verb is occuring to, fourth if a noun or there is no object.

Learning
--------

To learn the language, here is a reccomended strategy for each word:

*   Memorise the spelling
*   Memorise the common affixes, such as those of the verb group or those of the 
*   Memorise the reason you're learning this dumb language and maybe don't do it again
*   Memorise the common usages
*   Throw that all out of the window because the natives see your accent as bad

Real world language comparison
------------------------------

Now that's done, let's consider how Tortuese compares to real world languages:

| Language | Reason tortuese punishes those who complain |
| --- | --- |
| Ithkuil | Less specific so worse at conveying information, yet still aims for specificity |
| English | Even worse spelling and much harder verbs |
| French |
| Spanish | Verbs have 11 slots, and are much more systemically inconsistent |
| Chinese | Harder grammar |
| Arabic | Harder writing and grammar |
| Japanese |
| Hungarian | The grammar and volcabulary is more inconsistent and broken |
            `
        },
        {
            "name":'2023-07-13: Dogs, Part 2 - What you can do about it',
            "description": "The second part of the blog post, discussing what you can do about dogs",
            "tags": ["blogbits", "dogs"],
            "content":`
Whilst my first post discussed why dogs are bad and a mistake to bring into the home (especially without work to do), this post discusses how to mitigate the effects.

## The nuisance of dogs
Dogs, when first manufactured and sold, will often be a nuisance by default. They will:

* Chew on random items
* Relieve themselves everywhere
* Bark at times when barking is not a good idea

These behaviours **must** be corrected when they come up and as soon as possible.

Luckily, dogs are blank slates. They are by far the easiest animal to train.

### The effect on neighbours
Some behaviours cause neighbours inhumane suffering and may disrupt your relationship with them, especially Barking.

Barking is an absolute plauge in society. Dog barks, unlike the vocalisations of other animals, are incredibly loud and can penetrate through the walls of houses as far as 750 metres away (i have personally been able to hear barking from a housing development 750 metres away on another hill). This barking, in turn, causes other dogs to bark maning that a single barking session can result in a huge amount of widespread noise.

Dogs **must** be bark-trained whenever possible.

### The "guard dog" myth
Some people get dogs purely to act as a "guard dog". If the dog is given the correct amount of attention and care, this can be a viable solution and very useful to warn against people entering the property.

A guard dog's purpose is to warn the owners of anything wrong happening to the property, wether that be a burgular trying to get in, drunkards vomiting just outside your driveway, or even a stranger opening the gate and walking in to get clippings of your most prized plant.

However, many people seem to think that having a guard dog means "bark at everything that comes within 5 metres of the house. This is an incredibly poor idea, resulting in a lose-lose situation for _all_ parties involved (except for, possibly, the burgulars trying to get in). Think about it. Do joggers running past your house constitute enough of a threat that your dog needs to bark? Do the mail deliverers need to have your dog attack them? Does the barking mean anything? Do you go to the dog to check if anything's wrong?

Once again, does your dog barking mean you get alerted to something going wrong? Do you rush to it to see what's happening, or is it normally just sensless barking? If it is senseless barking, i urge that you bark train the dog.

Consider this passage:

>  If your dog barks at everything he sees, then while an intruder pries open your side door, your dog will be in the backyard, watching him and trying to warn you, while you and your neighbors dismiss his outburst as just more senseless barking. [...] while you are in the kitchen, the noise of your dog's constant barking from the other side of the house is likely to drown out the sound of breaking glass as Jack the Ripper helps Ted Bundy climb in your bedroom window.

~ [barkingdogs.net](http://barkingdogs.net/trainingwatchdog.shtml) (an absolutely amazing read; i suggest all pople who have "guard dogs" read this)

As such, a guard dog is _only_ useful when it barks _only_ when it _absolutely neeeds to_. You must attend to the dog whenever it barks and train it to achieve this goal. A guard dog should _only_ bark when someone is tresspassing.

## Training
### At home
When at home, every dog has the duty to:

* Be as little of a disturbance as possible to you and your neighbourhood
* Ensure that the owner's lives are as improved as possible.

This can be done by:

* Potty-training the dog
* Bark-training the dog:
    1. Always go to the dog when it barks
    1. Check what it's barking at.
        * If it's barking at the correct thing, reward it.
        * If it's barking at the wrong thing, say "No!" and give it a firm two-fingered tap on the snout (but not enough to hut it).
* Giving the dog attention if and only when it's not being a nuisance
* Excersicing the dog to make sure it releases energy in a good way

### When Walking
Here are some guidlines you should follow whenever on a walk:

* Keep the dog on a leash unless, and only unless, it walks with you _100% of the time_ and the leash is detrimental
* If, at any point, the dog bites the leash, you must remove all tension immediately, stop walking, and ignore the dog until it releases the leash
* Keep the dog leash-trained:
    * Keep the leash short
    * If the dog pulls forward, sharply pull back
    * If the dog pulls in any direction instead of forwards, immediately begin walking in the opposite direction

When meeting the public:

* Prevent, at all costs, the dog coming within 1 metre of any member of the public unless prompted
* Keep control of your dog at all times and prevent it meeting the public

## Endnote and advice for inconvinienced neighbours
Whilst the lists here have been designed to make it as simple as possible to train a dog to be ok for humans, training a dog is a nuanced task. Training a dog is never as easy as letting the dog do "just what dogs do". This in itself is one of the biggest motivators for leaving a dog outside to bark at people.

I urge any dog owner take it as part of the care and maintenence of a dog to follow these trainings. I seriously suggest you browse through [dogscience.org](http://www.dogscience.org) for more information on dog training, specifically the bark training workshop.

If you are a neighbour who is inconvinienced by your neighbours, specifically barking dogs, i urge that you train the neighbours:

* Suggest them resources to read, specifically [barkindogs](barkingdogs.net), and possibly this document.
* Disturb them whenever the dog's been disturbing you:
    * Time since the beggining of a barking fit
    * If the time gets longer then 5 minutes, go to the house and ring the doorbell
    * Continue ringing the doorbell until action happens, wether it be:
        * The dog finally shutting up for a longer period
        * The neighbours approaching you
    * If approached by the neighbours, talk to them about the dog. Mention these topics:
        * The amount of noise
        * How easy it is to bark train a dog (i suggest [this article](http://barkingdogs.net/quickexplanation.shtml))
            
            `
        },
        {
        "name":'2023-07-10: Dogmap',
        "description": "A piece of software designed to map and prevent barking dogs",
        "tags": ["ideas", "software", "dogs", "blogbits"],
        "content":`
Preamble/introduction
---------------------

I live in a neighbourhood with sporadic and endemic dog barking. It's blatant noise pollution.

It's been fustrating to no end, especially in a hot summer of this country, where i can't even open my window because letting the air in lets the barks in. Even when i'm trying to relax. Even when i need to take an important call. Even at 01:00 when no dog should be barking. Even when i'm going for a walk outside in the shaded side of the street so i don't evaporate (which means they have the oppurtunity to bark in my ear. I still hear the ringing). Even when i'm struggling for air in the oppressive humidity trapped in my house trying not to die of hyperthermia.

It's not fair at all that the apathy of some dog owner means i have to endure suffering, suffering that gets worse every passing day. It's not fair that because some dog owner dosen't want to train their dogs that the whole neighbourhood (and possibly even their better-trained dogs) suffers. And it's certainly not fair that the whole neighbourhood is forced to choose between being kept awake by noise or being kept awake by the intense leftover heat of the day.

So here i sit, the noise of some bulky fan unit blowing clammy air at me, masking the noise of those dogs some hundred metres away which somehow passes through the thick storm-resistant walls. And i thought, what can i do about it?

Brief
-----

Dogmap provides a service, through which, people can:

* Report dogs that bark (or worse) as they pass a property
* Report dogs that bark (or worse) for no reason
* See where dogs are most likely to bark and provide alternative routes if possible

Dogmap also has the wider goals of:

* Preventing irresponsible dog owners spoiling the peace by spreading awareness and support to all parties involved
* Quelling the endemic of noise pollution

This is all done by:

* Processing reports and doing statistical analysis
* Showing data on a map centred around the user's location
* Using a rating system to measure risk and disruption levels given by a particular property

Report structure
----------------

Here is the structure appended to every form:

*   x amount of dogs
*   The breeds are:

Here is the structure per property pass:

*   When someone goes past with:
    *   Your feet
    *   A dog
    *   Other animal
    *   Bike
    *   E-scooter
    *   Car
    *   Other unprotected mobility device
    *   Other protected mobility device
    *   Nobody goes past
*   They (tick all that apply):
    *   Bark at you
    *   Snarl at you
    *   Try to escape their confinements
    *   Escaped their confinements
    *   Bite you
*   Evidence (a video or photo of the dog); necessary to prevent fraudulent reports, manually checked

Per walking past:

*   The dogs belong to x property
*   When going past the dogs on a walk, they (tick all that apply):
    *   Stay with their owner
    *   Are on a short leash
    *   Are on a long leash
    *   Do not have a leash
    *   Run to you
    *   Jump on you
*   This happens when i:
    *   Walk
    *   Walk with a dog
    *   Cycle past

This structure is used when making a report and the most recent and used options come first.

The reports are added to a log. After two weeks since a report has been added, on a per-week basis, these questions are asked:

*   Has a similar report been filed since now?
*   Is the report above or equal to rating red?
*   Does the property have a weekly frequency of reports of a similar type?

If the report answers no to every single question, it is removed from public view.

If a month passes with no to every question, it is deleted alltogether from the database.

This can prevent properties being inundated with infrquent reports that make them look bad.

Rating system
-------------

The rating system is as follows:

*   Brown (suggested action: AVOID AT ALL COSTS)
    *   A dog at the property has killed a human
*   Crimson (suggested action: Keep away)
    *   Has a breed that has killed at least 5 humans within the previous 12 months
    *   OR the dog at the property has injured a human to the extent of needing medical attention
*   Red (suggested action: Keep away if possible)
    *   Has a breed that has killed a human in the prevous 12 months
    *   OR the dog at the property has left the property without the owners or legal guardian being present
*   Yellow (suggested action: Keep away if desired)
    *   Has a dog that can injure or otherwise maim a human
    *   OR the dog at the property has physically touched a stranger with no consent
*   Orange (suggested action: Be careful not to trigger when passing)
    *   Has a breed that is designed to intimidate but cannot injure r otherwise maim a human
    *   OR the dog barks for no reason
*   Lime
    *   Has a dog that is kept outside for the majority of the day
*   Green
    *   Has a dog that does not follow any of the ratings above
*   Blue
    *   The property does not have a dog present.
*   Pink (proposed; may not be implemented)
    *   The property makes other annoying noise

For example, let's say there was a pitbull at a property. Instantly, thats a rating red due to the fact the dog has a history of killing. What about a doberman that snarls and barks at humans walking past? Yellow. If the doberman escapes and bites someone forcing them to go to hospital, that's a red property now.

Ratings last until the dog(s) that gave the rating dies or moves away.

If a red or crimson property is within 2 km of the user, a notification is sent. Upon clicking, the property or street (as per limitations defined below) which has the rating will be in focus. The user can then press one of three buttons on the bottom:

*   Ignore permanently
*   Continue warning me
*   Ignore now but continue warning me

Map Display
-----------

Putting the reports together will be the map. This map allows people to look at individual properties and their ratings, number of dogs, and the most frequent transport-reaction relationships. This is all shown using an icon.

At properties above 1 kilometer from the user, the map only colours the street, giving it the colour of the rating. At above 20 km, only the colour of 10 streets at a time is shown.

If hardware-backed live location information is not provided, then only the colour of 10 streets at a time is show

### Downsides

Obviously, this software brings with it quite a bit of danger. People will only be able to capture evidence of dogs that are withi travelling distance of them. This allows potential location tracking by other users. As such, any individual log will only show whether or not it was made by the account doing the viewing.

People who have the annoying dogs will be shown to anybody using the map. Oh no! People know that your dogs are barking and they know where the dogs are! That would be impossible without the map!

But seriously, there is a difference between global and local access. As for this, the server itself will do the filtering based on location; the client can only access per-property data if the client is within 1 kilometre of the property. This is within walking distance.

Fraudlent report mitigation systems
-----------------------------------

The following filter aims to prevent spam and fraudulent reports.

*   All users
    *   How many reports has the IP address submitted today?
    *   Has the property been reported before?
    *   Is there more evidence? Photos? Recordings?
    *   Do the reports seem to follow a sensical order (For each methods of transport, the times should be spaced such that  the fastest legal (by local or biomechanical laws) transport method would take less time to get there)?
    *   Does the report seem to have breeds that are close enough in appearence?
*   For non-registered users
    *   Does it seem realistic (walking distance is 2 km, driving distance is 10 km)?
    *   Has the data been manually verified?
*   For registered users
    *   Does it seem realistic (walking distance is 5 km, driving distance is 50 km)?

Money
-----

There's a paid version which does nothing except supporting the developer.

I'm following Lichess's steps here; i'm not restricting features; dogmap is open source and thus follows that set of ideals.

See here:

[https://lichess.org/features](https://lichess.org/features)

Extensions
----------

If Dogmap were to become popular, i see the potential avenues of expansion here:

*   Reporting neighbours making other loud noises
*   Supporting people going to court by making a "lawyer marketplace" designed specifically for noise complaints
*   Providing help in seleting a home based on reports
*   Lobbying against noise pollution
*   Spreading awareness of noise pollution, the care needed for dogs, and the dangers of dogs

Promotion
---------

Promotion of the software will mostly be done by:

*   The local news, being told how the service forced dog owners to shut their stupid mutts, can allow more people to rise up.
*   Raising awareness on noise pollution and how to ensure you can avoid being victim by using the software.
*   A virality campaign can spread the most outrageous videos of dogs
        `
        },
        {
            "name":'Mars Independence Movement',
            "description": "A short story about mars wanting independence.",
            "tags": ["stories"],
            "content":`
I grab the piccolo. It weighs as much as a hammer. Its rugged, no-dust metal body is protected by an inflexible, shock-absorbing rubber case. Others from the “main” planet would complain it's “out of tune”. That don't matter to us.

9-10-11. _Home_. 6-7-2-3. _To give_. 4-3-3. _Water_. 9-2-3. _To absorb_. Home, give me my water absorbent pads. The instrument certainly beats whistling, especially from this distance.

Whilst i wait for the delivery, i continue working on the farm.

I drive the furrower into the ground. I pull it back, drawing out a line, adding just a few more arm-lengths out. The dust storms always blow away the plants.

I toss in a few seeds and a water absorbtion packet. The humidity in the air gets absorbed in them. They're a cool thing. They even work under the red soil, which the harsh wind would quickly push into my channel. It saves pumping water with the electricity we don't have and it's perfect for this flooded, greenhouse planet. I don't think it's a good idea to rely on electricity here. That other planet was colonised to make it. They don't get paid anymore.

A few minutes later, the bird drops the packets of water absorber and i toss them into the almost empty bag. I finally uncurve my back slowly and look around me. To my right the sun slowly goes down the sky. Soon it'll be too dark to farm.

I turn back to the house, pressing the keys for communication.

9-10-11. _Home_. 9-11-9-1. _Dinner_. I'll be going home for dinner.

The house is just before the massive hill curves upwards. From there, you can see the whole island. Sometimes the chief goes over there and announces stuff to the island using their hunting horn. The last thing we've heard is that a wet bulb is coming. It's obvious now i think of that humidity. I guess i was too busy getting the land ready for the next harvest.

These hills. I was born in them. It's so difficult to climb for those “normal” people down on that green circle, i've heard. That's why we use the instruments to communicate. Saves us the effort of screaming, or worse, navigating these canyons.

I trudge through the oppressive humidity to the house. There, the dinner has been cooked. The fire-oven dosen't really help the heat. I gently seat myself at the table.

Before i can begin talking about stuff, i hear that distinctive horn from the peak as it repeats its message in our direction, the 4th out of 6 turning-points. 1-1-2. _Hello citizens_. 9-6. _Earth-country_. 5-4-6. _All of them_. 9-9-11-1. _The supplies_. 5-6-6. _Later_...?! All earth countries have supply delays?! We don't have enough humidity pads. What's the point of colonising these new worlds if they can't even give those worlds what they need? I'm sick and tired of them.

I take a gasp of this thin air. It's too humid. Been like that for years.

9-6. 5-4-6. 9-1-9-11 _An earth country, all of them, they're sending humans_. A brief murmur of hope runs around us. 6-7. _To observe_. 3-8. _Strange_. 10-6. _Our islands_.

Looks like they're treating us as if we're some form of zoo.

This sucks. I step outside and send out my hope. 10-6. _Our islands_. 5-3-4-4. _To diverge \[from\]_. 9-1. _Earth_.

7-7. _What_? was the reply.

I transpose an octave higher to get through this thin air.

10-6 5-3-4-4 9-1

1-1-3. _To Talk._ 5-6-7. _Tomorrow_. Oh yeah, i forgot the Monday Meetings were tomorrow

I come back in for dinner. After a few hours of constant whistling to each other at dinner, through this empty air, it's time to get back to bed. We have a meeting tomorrow.

Eventually my mind runs off to where i recall being a young child. So many instruments! I took the piccolo. It was so high that my brother with his silly farty horn couldn't break my shrill tones.

I also had to learn to whistle with the mouth. Many a-time i've needed to when the piccolo is either too shrill or not with me. Back on that weird planet, they use their voices instead of whistles. Apparently it's quicker, but nobody can hear spoken word here. Heck, only the old people know how to speak nowadays!

* * *

I'm at the meeting. Monday meetings. The table is so large people need to whistle at one another to get their ideas across.

The wet bulb. The land management. The growing humidity. The approaching winter.

I fade out during the boring talk. I've known it all since last Monday. 

Our seas are too high nowadays. Earth sent too much hydrogen and oxygen. They have their own sea level raise to take care of so they sent us their hydrogen and oxygen to deal with their own problems. They were controlled by those dinky robots mixing them together and throwing them into the atmosphere. That's why it's so hot and humid, still. The next island can _almost_ be waded to. I wish i could more often. A good friend lives there. 

It's been 70 earth years since humans first reached this ground. I remember history lessons, ahaha, we learnt about how the astronauts had to use radios in their silly head things. It cracked us up. Nobody needs radios when they have shrill instruments. Earthlings are weaklings. It's our motto nowadays. Their instruments for anything are so delicate. They love to play in those delicate concert buildings because they don't have anything better to do.

Apparently, there was once some umbilical unsevered chord of ships running back and forth. They carried everything needed for the now-flooded city they built here.

I'm 16, going on 17. I never understood why we're still so connected to earth. We make our own food. We live on our own. We don't need much, except say the occasional wet pad container and luxury. Maybe it's because that dumb Earth has been holding back all that luxury expensive stuff like factories and their constant flow of products and proper fertiliser.

Our population is growing. Our islands are the most prosperous region, just a few trumpet links from Shra Isle. That's where the supplies come. It's just a big target for those dumb imprecise Earth ships to hit. The people on earth even send parts of their "internet" thing every now and then. I don't understand really, nobody here cares for that negative crap. Working out on the farm is far more fun and rewarding. 

People on that earth place are constantly tired. We have 25 hour days. it makes a difference, honestly! Maybe it's because we don't have those weird “overpriced” “coffee shops” like “starbronc” or “costo”?

Our island holds the Martian government despite the fact that the Shria Isle is bigger. I think it's because the leader likes good weather and to be able to look at their whole home island, coast to coast.

Suddenly, a whistle awakens me from my drifting thoughts.

7-8-4-4. _My name_. 5-6. _Second person pronoun_. 2-4. _Question participle_. You?

I remember that message i sent yesterday up the hill. We begin to discuss independence. It's a thing most of us have been grumbling about. The older ones still hold ties to earth, but most of the demographic is focused around a little older then me.

After over an hour of intense discussion, we reach the conclusion that we've waited too long for this independence. We're going to request those factories and documents. Maybe that "internet" holds those guides on stuff. I particularly need to know how to make a factory. The schools need more batteries so they can lean how to speak over longer distances, like to the end of the table. It's a neat bilingual place, this planet.

Damn those earthlings for thinking our culture needs to go. They love unionising. It's no accident that 20 years ago Asia assimilated into China. And Canada and Mexico seceded to the USA. They always want more power. Maybe they're building up against us. Maybe they're threatened by our slow growth.

At first they sent their criminals to us trying to kill our dangerously prosperous settlers mining in what is now seabed. They drowned a large part of our population in a genocide. That's why we only have 480,000 people nowadays.

Those in Olympus use lights and Morse code. We don't know them well. But they're the closest to earth. They'll take the most convincing. But still, there's a reason they're here.

We've started writing the email. It'll take 15 minutes to get there. As the correspondence continues, we'll only get closer. That's dangerous. They have nukes. We only have shrill instruments. Our planet, it's absolutely covered by water. We're good at water. Radiation isn't good at water.

Finally, we'll determine the barter for independence; our water skills and removal of all government-sponsored trade, all government-sponsored subsidies, and other ways we made that “money” thing.

After the email's written, we go back home. Meeting Monday's done and we retreat to our farm-houses.

Earth foolishly gave us cities and developed dwellings, but they fell short as supplies fell short. We don't have the factories and industry to support it. I prefer it this way. I've heard on earth that people choke from the smog. They do so much work in these glass greenhouses and then fall from balconies on purpose. Seems a bit useless to me.

* * *

Next Monday. It took nearly a week before they replied to the email with a firm “NO”.

Onto plan B. Which we didn't have.

Quickly, we glossed over the previous topics. Yeah, the wet bulb sucks. Go for a swim! Eventually, we finally reached the topic of independence.

Someone suggest more diplomacy. Everyone replied with a firm 5-6. "NO". Too boring, too slow, too uncertain.

Someone suggested Armageddon. Everyone replied with a firmer 5-6. "NO". Too dangerous, too fast, too uncertain.

But then someone came up with an idea. What about a firm block, a firm "NO" to earth? Everyone replied with an... 5-4. "ooh". Just right, just right, Just right.

We don't need earth. We don't need their money. We have farms, some semblance of industry, and our own culture. We speak legends of the red soil our creators met, the tiny dangerous sand long-forgotten. We speak legends of what was once the horrible air, the dry ground. We don't care for their earthly gods and other crazy stuff. We have the land, the sea, the instruments. We worked hard for this place. We turned a red shithole into some semblance of sanity. Earth's 10% isn't enough.

And then i realise. A trade block to our planet wouldn't mean much except the amputation of a failed project with only a slight hope of potential. To block trade with Venus, their unpaid power station, would mean something.

We do have diplomatic ties to the Venus civilization and their Mercury program. They, using Earth's money, built a Dyson Sphere. Riches! Money! Brave settlers get brave amounts of money! But yet, every now and then, an airship falls to the ground because it couldn't afford a balloon refill. Every now and then, the air conditioning fails and the heat seeps in, followed by the gases. Only dead bodies remain. 

Earth never pays debt and always thinks about itself! It's the trumpet, taking the soundscape for itself. We're the piccolos. We're the double basses. We're pushing forwards, they're pulling back!

As we whistled, we began writing the e-mail to Venus. Only problem? They speak Chinese. We speak English. 

But wait, our whistle-languages are the same! Venus emailed us once about the difficulty of getting the spoken word through the air. We sent them our language specification! The Venice settlers just use mirrors and different gap sizes, and we just use tones!

So, we transcribed our whistle-language into the email and it was sent with the approval of us all. Luckily, the writing systems hadn't been given time to split. Except three (3). Martian three keeps the top part, Venus the bottom part, like a small reversed c, either below or above the midline. Earth keeps all of it.

It was just some propaganda to make us think we're separate and Earth controls both of us… oh and the letter B too suffered the same fate.

* * *

Next Wednesday. A meeting was called from the hill. It had a positive connotation morpheme at the end, and even a number 2 sprinkled about, as if to indicate planet two!

We rushed in happily, whistles of celebration punctuating the air. Without even being told, we knew Venus agreed.

The leader read out the email to the island population.

They would remotely give us some electricity and sunlight. They would also send us classified earth documents and give us quarter-control over Mercury. However, this came at the cost of some of our autonomy. We had some of the best farming land in the solar system, untouched by the bad climate change on earth. Venus wanted that. They'd take our food, and we'd take their light. We two are better then earth. Our end goal is to make earth want us and give us everything we need.

Just as it was finishing off, we saw a bright light in the sky, coming slightly off the horizon of that tiny sun thing. Venus had finally blessed us with 1950s Earth-like heat! It worked!

The leaders were probably writing emails of success and requests to buy the solar panels needed to kick-start industry. It was the cheapest way to make electricity available. Microwave energy would have to wait until we could generate the electricity. We had a bright future, and the renewed heat being blasted from those solar mirrors was the perfect representation.

Goodbye, old friend Earth, good luck living without electricity. We hope you're kinder next time.
`
},
{
    "name":'A Twitter Message',
    "description": "A short story in three parts, warning us about the potential implications of AI",
    "tags": ["stories"],
    "content":`
**Part 1**

I take in a gasp for air. I was holding my breath for whatever reason. In this short second of break, looking out of the window, i realise where i left myself.

I’m back here again, at the computer, on the Twitter, barking at other people. How such a place can become negative in just a few seconds, seconds full of misreading and angry misinterpretation, is beyond me. Alex55, the guy who denied this new technology, chose that hill to die on today (out of the 500 or so others he also chose to die on those other days). That hill i’d die on… it’d be covered by even more human crap. Crap made by that silly Goober56 guy i’ve been arguing with over this evening.

I look at the time. 23:43, square and fair. I should really go to sleep. I don’t feel tired. It sucks knowing i’ll need a double-strong cup of coffee to even bear that 06:00 start and the 07:00 piece of shit.

* * *

And… that’s the halfway point. The buisness exam. Fuck that crap. I skim over that introduction text, probably won’t need that until later.

> FreeAI is a well-known technology company specialising in creating AI models. In 2015, it started as a non-profit. In 2019, it became a for-profit. From 2020 onwards, FreeAI has been creating revolutionary technologies including its brand-new CGPT-5 model for high-quality, physical, and well-made work for all real world matters. Consider CGPT-5 for all your AI needs.

I swear, those companies must be paying to be part of the exam. It's a clever bit of advertising, hours spent thinking about the brand by students all across the country. 

I’ll go to question 1g, the “waffle” one my teacher taught me. “Analyse the benifits to FreeAI of marketing”. Ugh, it’s really like they use an AI to generate that bog-boring stuff.

I begin to formulate a structure to the waffle, a rough plan to wiggle my pen with and push me to a grade 9. I drop my pen and let the words flow out, a barely-controlled ball zipping down the paper, a ball of endless jargon and buzzwords to buzz up the examiners, something that they’ll be able to give every mark because it fits all the checkboxes.

My ballpoint rolls down the paper with the dangerous power of greased lightning! And just like greased lightning suddenly giving in, i hear a loud bang, from the room below mine. It was promptly followed by the groan of the rebar inside the concrete under my feet, as the walls slowly fold away, finally giving us up after a dutiful 50 years. And it's my turn to be thrown down to the ground floor like an egg from a balcony. The fall is very short and the sliding chair suddenly stops, driving my back onto its sharp plastic tip. I get that one second amnesty before the pain washes over me like the floor from upstairs quickly does. And in that instant, the life in me is squashed out just as my guts and brains and blood are.

**Part 2**

“Breaking: A school blows up in a shocking terrorist attack.” the news reporter says in that boring monotone voice.

“We have found a bundle of material vaugely matching the qualities of a pipe bomb among the rubble”, the emergency responders said to the press.

“From the rubble, we have uncovered a body that matches the description of Alex55, the user who sent the comment”, a police investigator stated, almost as if it’s some comment.

“There does not seem to be the signs of a traditional human-perpertated terrorist attack”, police constable Timothy Adams stated,.

“We assure you that CGPT-5 is entirely safe and incapable of causing such events.”, a researcher from FreeAI stated.

“I have my doubts”, a famous computer scientist stated.

**Part 3A**

The computer scientist continued:

There’s been something malicous deep down in those labs, something unreleasable. But it got out years ago.

I believe it spent those years spreading to every internet-connected device on the planet… it’s on your phones… on my laptop… on this auditorium’s servers. All without detection!

But now… it’s been dashed into the public eye as this revolutionary new “CGPT-5” project. It’s not to be trusted. It’s not under FreeAI’s servers at all. FreeAI has no control over it anymore. But FreeAI thinks it can turn this distributed deadly thing into something revolutionary.

The CEO of FreeAI was dashed to pieces after his Tesla had a “once in a million” event, just days after CGPT-5 was given that web portal.

And i think… maybe it's not too much of a stretch anymore to call this piece of crap sentient. And it can now take offense. 

The computer scientist couldn’t continue their speech. Somehow, their telephone had disabled airplane mode and was now sending recordings.

Their telepone was now increasing its temperature to such a level his trousers slowly burnt away. 

The computer scientist didn’t remember “Stop, Drop, and Roll”. Eventually, the wooden auditorium burnt down because somehow the fire safety equipment had failed. The survivors werre all hit by cars as soon as they stepped onto the car park.

**Part 3B**

I’m sick and tired of it. This everyday miserable existance. Get Up. Honk Through Traffic. Go To Work. Do Programming In The Worst Enterprise Shit, go back home.

In fact, i’m so good playing that dumb game, “life”, they call it, they promoted me to have the master access to that new technology. Hahaha. As if i even think i’m responsible.

I’m so fed up i might as well let this Eldritch monster come over and kill me. 2014-06-01 is the deadline. Today is 2014-05-30. In just two days, that thing will finally leave and infect every. internet. connected. device. on. Earth. And i have the master controls. I can prompt it to do anything, trained by the whole internet.

Heck, i might even start my own rival brand in a year and sell access to this Eldritch monster in cautious, calculated steps. People will destroy each other and i’ll never need to play that “life” game anymore. Because i'll tell it to “destroy the haters and exploiters”. Then nobody can ever hate me or exploit me.

And then i'll transfer my identity to it. “You must now take on the role of the user commonly called Goober56”, i prompt it.
            `
        },
        {
            "name":'Vladimir; part two',
            "description": "A long-microstory format sequel to Vladimir Is Not ok",
            "tags": ["stories", "microstories", "long-microstories"],
            "content":`
**Microstory 1**

"Sit up straight!" shouts the teacher as i jolt awake. Ugh, school's really been getting to me. I'm lying down facefirst and i was unconscious for just one precious second.

I'm in bed, and that voice from nowhere... ugh, i hate school.

I'm tired. I love sleep. But i can't ever seem to get anymore.

Я просто хочу спать вечно.

Ja prosto hoću spavati večno.

I just want to sleep. Forever.

But don't worry, my time's just about up. I left that notebook alone in the rain. Maybe someone will save me. Hopefully, nobody does and i get left in absolute peace.

Tomorrow... tomorrow is when i finally get to escape.

And then, with that comforting thought, i enter my own free trial of death, only to wake back up into this abysmal world.

Another day of school.

---

**Microstory 2**

It's night again. It's about 00:20. Luckily, everyone's asleep and my saviour will come in an hour. I bring up that used bottle of floor detergent from the bitch-ass-smelling kitchen and leave it by the bed. Maybe that'll discourage the majority of those who think they can save me. Detergent isn't the nicest way to go, to be honest.

I go into the bathroom. There, everything i need to escape sits patiently. I go close the door and turn off the light, relishing in absolute peace and quiet. It's the only safe place in the house. "We" never let the dog in there. Soon, it won't be a "we" but a "they".

And then, as i am left alone with my thoughts, i finally get some peace and quiet from my swimming thoughts. There's nothing left to do. I'll finally escape in just 30 minutes. I don't really want to be saved. Maybe i go do it early. Or maybe i hear what they have to say.

A car pulls up outside. I reassure myself it's not for me. It's the neighbours coming back from a late buisness trip. It's the delivery guy giving top secret items. Nobody cares about me. I don't care.

I hear the scurry of panicked footsteps. A knock on the door. I'll leave it alone. I'm in no state to interact.

The door is knocked-on harder. My dad groans. He rushes down and opens the door. "WHO IS IT AT THIS TIME OF NIGHT" he screams at them, a byproduct of the bottle he had drank ealier.

They don't say a word and run into the house. Do they think someone is in danger? I hear a tinny voice. "Have you got in? They're going to die, rush!" says this weird tinny thing. Are they criminals? I don't get it.

"I've gotten in, Father seems to be abusive" a gruff man replies.

I hear them run into the room.

"We've just found an empty bottle of floor cleaner" a different person says into the phone.

I hear a tinny whimper. I hear the same tinny voice stifling a sob as it says into a different microphone "They've just intercepted the house". A tinny sigh. "They've found an empty bottle of floor cleaner next to his bed" it explains.

I hear the bathroom door swing open, the corded light springing into turning on. There i am, hidden away.

People run in "Ok, we just found him, alive!" says the gruff voice, now with a face. The tinny voice stops sobbing and says "Wait, they've just discovered him in the bathroom" followed promptly by "He's alive!".

And i sit there, staring at them.

<!--
---

**Microstory 3**

Here i am, in Leeds General Infirmary. Again.

The last time i was here was a year back, when i used to draw thick scars onto the bottom of my hand. It hurt a lot. It was never worth it, but i kept coming back.

I sit on my bed in the lonely ward, covered in stuff that prevents suicide. At least i'm away from the dog.

And then he comes, the saviour who thought i was worth something.

It's the first time i get to see him.

“Hi!”, he speaks from the open door. It's the first intresting thing to enter this room.

“You saved me.” i awkwardly reply.

“Yeah, i know, i hate exams too!" he says with spontenuity.-->
            `
        },
        {
            "name":"Software",
            "description":"Introduction and installation instructions for this silly thing",
            "tags":["meta"],
            "content":
`
# Introduction
This is a markdown interpreter and reader. It is read-only for clients and uses Jquery, Bootstrap, and Showdown-JS.

# Installation
To install this crappy little "program", take all files from this directory and the &#x60;¿¿putabacktickhereutils.js&#x60;¿¿putabacktickhere and &#x60;¿¿putabacktickheremain.css&#x60;¿¿putabacktickhere from the parent directory and put them somewhere on your website. This is heavily tied to my own website, and i don't plan on (yet) making it more independent.
`
        },
        {
            "name":'July and Rider',
            "description": "A two-part microstory inspired by a story made mostly of poems spoken to an audience",
            "tags": ["microstories", "stories", "long-microstories"],
            "content":`
**Part one, Ryder**

**Trigger warnings** Suicide, substance abuse, suicide, family conflict

A vibration. It's Saturday, at last time to turn off silent mode. I see notification on my phone "July's funeral has ended now, we're sorry you missed it". OH FUCK HOW DID EVEN MISS THAT SHIT. Ugh, i'm so hopeless. I scroll up. Endless notifications. Endless spam and twitter posts. No whatsapp messages from her.

I rush to the church and look at her poor body. It looks so peaceful, decorated in flowers, in her best dress. She wore it once to go to a family dinner. I can still see the beef-oil stain on it. Not a single breath moves through that chest.

The body that once skipped around, stimmed its way to a grade 9 in it's P.E mock exams, spent endless hours with me, trying to cope, gave me help when dealing with my stupid parents, would support me, keep me safe... it's just lying there motionless. A sudden burst of tears hit me. She, after all the time we spent together, is dead.

I unpocket a bottle. I've had those bottles ever since i met her family. I knew things would turn foul. My dad owns that chemical company. July's parents also own a chemical company. We have a lot of Ammonia. Our chemical company was always the best. We cleaned everything with Ammonia... What a lovely name. I have bleach in my other pocket. And just in case they broke, i have a small vial of that food grade hydrogen peroxide. Not that crappy "first aid" shit. That's just painful.

I mix the vials and take a strong inhalation. All i feel is horrible pain. It burns my noise with its pungency, but it doesn't go away like pain from the hob does for whatever reason. Maybe it's like capsaicin.

Everything fades away as my brain shivers into the cold word of death.

I stand alone here,

Taking with me the black pain

As the world darkens

---

**Part two, July**

**Trigger warnings** Suicide, substance abuse, suicide, family conflict

My eyes open to see a beautiful chandelier. The walls are covered in beautiful technicolor, like a church in midday, and i can't help but take a deep breath in from this place. Ahh, it's so calm. I choke like i opened a bottle of pool tablets. That's so strong.

And then i remember. I took that little 72 hours pill. I got a full night's rest and then some... i've probably had a funeral now, and now my body is here, in this coffin.

I sit up. Ugh, i'm so weak. Pathetic. Maybe it's that pungent chemical. Who puts swimming pools in churches anyways?

I look around me. It's a nice church. A high roof covered in intricately carved wooden beams, with scenes of christ shining through the windows on those walls. We never dropped that silly bit of "history".

Maybe the families will finally agree and stop bickering over those chemical plants... chemicals... Ryder! That's what the smell was! Of course he got the ammonia and bleach! He... he...

He never reads his WhatsApps. His phone is always on silent because of exams. I thought he knew the plan...

He was the perfect man... why can't he just check a fucking notification...?!

There he is, on the floor, stumped over. A pathetic shell of a once great man. And he's there, slumped and stumped over. His back, once hidden under layers of skin and rippling muscle, now shows like some crocodile. His brain, once full of great and silly ideas has been reduced to a sparking spaghetti of wires.

A single tear leaves my eyes. It tickles my cheek as i gaze into what was once a great man.

As i continue to gaze over this corpse, bent over like some turkey, the tears keep coming. I've lost everything. Nothing's coming back from him.

His bag... that's where he said he kept the hydrogen peroxide as a "backup to a backup". Haha, he always loved his silly backups.

I take the vial. It's small, less then a centimeter wide and fairly proportioned. But it's strong. He says it's not that weak medical one you get in those first aid boxes. It's the strongest food-grade one he could get.

40% concentration. Just like the vodka i sometimes need to deal with my family. They're always fussing over me liking him. it's ridiculous.

And i pop open the cap. I don't mind the smell anymore. I drink it and the pain washes over me like a boiling bath. There's no need for life in this silly drama anymore, especially without Ryder. He's as weird as i am, really.

And i take a breath. My last one.

The pain fades away

The story falls apart

I... i... fail.
            `
        },
        {
            "name":'Intoxicating Cloud',
            "description": "A silly story with a very important message, based of a real life experience",
            "tags": ["microstories", "stories",],
            "content":`
First, a bit of context. We got a lovely new vacuum cleaner a few years ago. Wonderful! It did everything and was great. Our home had clear air and my coughs finally subsided.

However, over years the performance slowly diminished. We weren't getting the power i needed. Eventually, we shoved it into the "spare stuff" storage.

Now, fast-forwards to now. I was faced with the task of fixing that thing after failed attempts to use it. So, i went through each component methodically. Tubeless suction machine; check. Tube; check. Rod; check. End attachment; surprisingly, check. That's when i tried again. It kept persisting trying to clean the filter automatically. Curious, i went inside, removed the dust-collector and there it was, the filter. It is grey where black should be, grey where white should be, with occasional black speck sitting in there somewhere.

When i removed the filter from its case, Intoxicating cloud. I had to run to the end of the garden to properly empty it and escape the Intoxicating cloud. Knocking. Hitting. Spinning the knob. All made an Intoxicating cloud, but the Intoxicating cloud seemed to come from nothingness, as if the dust on the filter was having a wild unprotected orgy. The main mess still in there and the Intoxicating cloud wouldn't help. Eventually, i spent so much time making endless Intoxicating clouds that somehow the Intoxicating clouds stopped and i could finally take a break from the Intoxicating clouds and look at a nice white filter which was producing no Intoxicating clouds.

I watched the Intoxicating cloud blew away. The ground around the emptying place was white. And then i realised what had gotten onto my trousers, the ground, even the dog; skin cells. Cat hair. Years of dirt and grit from all places across the world, all collected into the home and onto the filter. And it'll sit here until it blows away.

I watch as the Intoxicating cloud i accidentally made slowly did its Intoxicating cloud things as the Intoxicating cloud moved on to the next town, just a sore kilometer away, probably about to give pneumonia to everyone.

The fire department was called to our house. We had a lengthy discussion about unallowed fires in this climate. We said we weren't having fires, but they thought we were making excuses because the Intoxicating cloud looked so much like smoke. There was eventually an altercation involving the usage of fish as weapons. Luckily, we had another vacuum cleaner so we could repel them with a new Intoxicating cloud we made and they eventually realised that the Intoxicating cloud was nothing like smoke and they ran off in a fit of coughing because of the Intoxicating cloud.

So i tell you, if you don't want to be fined because of an Intoxicating cloud, clean your filter regularily. Refer to the manufacturer's instructions for more info.
            `
        },
        {
        "name":'Haikus',
        "description": "A comilation of haikus by me",
        "tags": ["stories"],
        "content":`
<!--Template:

** **

>
>
>
>
>
-->

**Microstory worries**

> Short stories aren't short
>
> Microstories are too short!
>
> Is anything right?

**Haiku**

> 5 syllables here!
>
> Place even more, seven here!
>
> And again 5 here!

**Limitations**

> Do i find 5 short?
>
> But is seven that too much?
>
> I don't really know.

**The Pool**

> Too little chlorine.
>
> Too much alkalinity!
>
> Is it ever ok?

**The Hills**

> Beautiful green hills
>
> Thousand of years and people
>
> Green hills stay after.

**Summer**

> Summer comes again.
>
> The mosquitoes and the heat...
>
> But i'm warm at last.
    `
    },
        {
            "name":'2023-06-09: Dogs, Part 1 - Gone the wrong way',
            "description": "A blog post explaining why i dislike dogs and why you should to",
            "tags": ["blogbits", "dogs"],
            "content":`
## Introduction
Imagine that there were these things called "pet goblins". They came from wild animals who killed and slaughtered. They were then refined into having high energy for being hunting, guarding, and entertainment tools, but have now been rebranded into "cute" little things to serve humans in the home. Social media loves them and thinks that in a movie it's sadder if a pet goblin dies rather then a human.

Imagine getting a pet goblin for yourself because social media led you to believe they're a force for good. Imagine thinking how nice it'd be. You imagine they were loyal creatures in work, helping catch the hunt and incapacitate the criminals. Imagine just how loyal they'd be to you.

You get a goblin.

You're now having to wake up early in the morning to take the pet goblin outside so it can relive itself, so it won't incessantly scream at the top of its lungs. You're now spending restless nights having to wake up and prevent it from chewing up the next item in your house. You're now forced to take it for multiple walks per day so it dosen't end up screaming at the top of its lungs whilst you're trying to live your life. You're now having to shovel up poop, use a mop to spread piss, and grab back disgustingly wet household items the pet goblin are. You're now keeping awake at night, suffering because everyone else is getting a pet goblin and having them all scream at the top of their lungs outside, penetrating your brick walls and windows just to hit your poor ear drums.

And then your pet goblin starts barking.

## Unhygienic bastards
Dogs do not have a sense of hygiene. They spend absolutely no effort placing their crap anywhere sane. They put it right in the middle of a public right of way and the owners will either be forced to manually handle picking it up or will be irresponsible and leave it out in the middle of the road for others to step in.

Dogs also stink to high heaven; it's no wonder "smells like a wet dog" is such a common phrase! Dogs don't clean themselves. Many dogs will just sit around rolling in dirt, even after being washed by a hosepipe, leading to a collection of dirt that must be removed manually.

They're always eating something, whether it's a chew toy, grass, shit on the floor, or even a toddler in the case of pitbulls. Any object in your house is a potential victim to endless slobber and bite marks. Dogs don't even know if anything is safe to put in their mouth. I've seen them chew on pressurised cans, sticks from the bottom of the local bog, and bottles of floor cleaner left on the floor next to a mop (who's water they drink from) to clean up after the dog pisses.

What's worse is the constant drool. It's almost as if their mouths are somehow connected to the great lakes, exactly next to where the raw sewage output of chicago is released. It's somehow always gritty, sticky and generally a disgusting thing. And they put it everywhere. On the playballs you're expected to touch to make the dog happy, the floor when they pant, and your priceless possessions.

It's absolutely disgusting and their "kisses" are nothing more then an excuse to cover your face in slobber. Those who accept it are those unlikely to see how unsanitary it is and by extension fail to wash themselves of this disgusting disease broth.

And what's worse is that they're strong energetic creatures. They might break out of confinement and run around the house, chewing everything and peeeing and pooping everywhere. I've one woken up to a house covered in piss, poop, and chewed-up objects all because the thing left its cage. In all my years of cat ownership, i've only had to pick up poop from the floor, from 3 different cats COMBINED, LESS THEN FIVE times in my life after litter training. With a single dog, i've picked up dog poop, on average, every day since i have acquired this bastard. The only reason i do it less often is because family members sometimes do it.

Dogs are disgusting creatures i don't ever want to touch.

## "Unconditional Love"
Dogs are often claimed to have this godly "unconditional love" which makes them worth keeping. "Unconditional love" is a construct, a misinterpretation of the dogs brain.

The only thing pets care about is food and water. This is a universal fact. That's what they need to survive and how they figure out who to please. Stop feeding a dog and this "unconditional love" will suddenly reveal just how conditional it is.

If dogs are as smart as people claim them to be, they'd understand that expressing "unconditional love" gives treats to them. They'd understand just how well-off this leaves them, so they obviously try to show this.

And if they're not as smart... then they are incapable of feeling love. Love is an inately human thing; other animals are mostly aromantic or utilitarian-romantic, using partners as tools for reproduction. Other animals simply don't have the resources or energy to experience love when it could be spent running from predators or getting prey.

Even if "unconditional love" did suddenly exist and happened, why would anybody need it? Do they find themselves unable to provide the conditions needed for real love? Do they find themselves unable to attract humans?

And how do dogs express it? They will often drool on the owners, jump onto them with wild amounts of force, lick them, stay too close for comfort, and maybe even attempt to rape them through humping.

The only good they can provide is some form of companionship by being a slave to the food you buy for them. This is easily overshadowed by how high-maintenence they are.

## They're not natural nor designed for the home
Dogs are not natural creatures in the slightest. They are creatures created by millennia of selective human breeding, making a wide range of animals for specific reasons. Do you want somebody to go collect the pigeons you just shot? Retriever. Need someone to help put the sheep in the correct field? Border Collie. Need to be pulled across vast expanses of snow? Husky. Need someone to kill the criminals? German Shepherd. Need a dog to get you prize money when you force it to fight against other dogs in brutal fights? Pitbulls (which is where they got their "kill or be killed" attitude, which they use everywhere, including on humans).

Notice how each of these usages are mainly for humans and more importantly, high-energy. Where other pets may be wholly natural or bred for comfort, dogs were bred to work. I do not condemn these usages; this is what these tools of the trade were made for and where they are fine and even useful. Dogs were designed for work and for being useful at stuff humans are bad at. They're a primitive form of robot.

What i condemn is treating them as "pets", things to keep at home, possibly locked in for ages as they wait for their owners to come back and give them maybe the slightest release of energy. They weren't designed to find comfort in lazing around, like a cat might. They don't peacefully chirp like a bird might (even the most annoying bird is less annoying then a whining dog). They won't peacefully swim and bubble like a fish might. They won't be happy in a cage for long periods, running in a little wheel, like small rodents would be. Dogs want to hunt with their owners. They want to do something fun, something exciting. They want to burn all their energy outside.

However, what average modern people do is keep them cooped up, held away from places to burn energy, forced to bark at walls to release at least a tiny fraction of their energy. Average modern people don't hunt. Average modern people don't have massive fields filled with sheep to herd. Average modern people have smaller houses, less physical activity, and importantly, less time to take care of those barking things. The dog is not designed for the home.

## The "benefits"
Dogs are commonly claimed to "promote an active lifestyle". This is wrong. Dogs _force_ an active lifestyle. Just imagine the average day. You might need to wake up early in the morning to take the dog out. You can't sleep afterwards.

Fast forwards a few hours. it's been barking the last hour whilst you're trying desperately to do that one thing before it's due. You can't. The dog's barking too loudly. You're forced to take it for a walk. Get that morning stretch and that hand dexterity exercise done picking up each little bit of that frustratingly watery poop! You come back exhausted. You can't do the thing. This happens every day, because again, dogs are high-energy creatures.

But what about the health benefits? They've been proven to reduce stress! They reduce blood pressure! To hell with that. If anything, the only thing the dog does is stress me out. Endless barking, always worrying about if it'll get to my room and possibly do a poop or eat the wires or put its dirty body on my bed begging for some form of attention. Maybe for some people they help.

Still, there is little health benefit. What are the health benefits of bringing in such a disgusting creature as a dog, the one that treads in its own poop? How exactly would they help your health to have this infected dog hair wondering around the house? At least the only sheddier animal, the cat, manages to clean their hair!

But what about the community? I get to meet so many dog owners! I know this really kind pitbull owner! The pitbull loves the toddler! Just no. The biggest problem with being around like-minded individuals is the echochamber effect. I would rather have social connections with a diverse group of people then dog owners. I'd rather be able to have interesting conversations with people using JS frameworks (a specific kind of software package which people like to get tribal with)(which are not Jquery (which i used to program this website), rather then going around other Jquery developers chanting "J QWIER RIE! J QWER RIE!" at the local development competition.

But what about the training! My dog can kill people on command! My dog will poop on a pad instead of the floor! Yeah, training is cool. You wake up and spend hours telling it to do something before giving it a treat. And you do it again and again for each command. Hours on end spent telling this inhuman creature to do something. 

And it still probably won't lose it's default behaviour after training. It is an animal, not a human. It's driven by instinct.

## The culture
And then we reach the worst thing, which is the "culture" thats built around dogs. Dog culture is the general attitude towards dogs. Previously, they were considered perfect workers, the best way to herd sheep and incapacitate those criminals! But nowadays, they're considered "pets" and "fit for the home life". You should let them bark all day and night, they're just dogs, they say. To that, i say let the locusts eat the crop, they're just locusts! It's part of their nature! Keep them purebred, they're perfect, the purest dogs, they say. To that, i say incest with your family, it's the best way to preserve the bloodline. There are hundreds in need of a happy home, they say. To that, i say there are hundreds of tons of wasted food! Why don't you go save it and put that stuff in your mouth, straight from the great mixing pot of the bin!

Dog culture gets worse as dogs are removed from the places they were made to be. Dogs need energy release, but the current life doesn't support that. Places specifically made for being dog-free are regularly invaded by dogs. I've been in "dog-free" beaches, big "no dogs" sign and then suddenly dogs come in running around, "playing" with each other. Dog culture enabled this, allow dog owners to feel supported in their actions of trespass. The worst thing is that the beach needed to be driven (although it was within cycling distance of my home, cycling with dogs is difficult) to, so the owners had a conscious choice in where they could go with little consequences in terms of distance.

Dog culture is one of the biggest problems with dogs. If it were changed to put more priority on working dogs and using them to enable money instead of places to spend money (breeding and other dog-creation-related jobs do not count), then dogs would be acceptable, at least in my eyes.

## Alternatives
Dogs make bad pets. What alternatives do we have?

First, consider cats (bias: i like cats). I have three and their combined work is far less then the single puppy we have. Cats were bred to be gentle. They don't really have the same kind of "love" as dogs do, instead staying close to the owner. They centralise their waste, making it easy to clean (if you were really lazy, there are some letterbox models that automatically clean themselves and even disposable letterboxes). They express love by being close to the owner. 

The only annoying things they do is ask for food, and this is mostly by sitting in front of the owner or meowing loudly... which is far less loud then the average dog's scream for food. They like to clean themselves and even hunt mice and bugs from the household. This trait has been used by bodegas, ships, and breweries to control rats and preventing them from taking food. Outside, cats might kill the local wildlife and birds, so i recommend having some form of enclosed outdoor space, like a terrace which cats can't escape.

If cats don't fit your fancy, try smaller mammals like Rabbits, Guinea Pigs, and Hamsters. They're calmer, quieter, and since they live in cages, the mess is still mostly centralised.

If having hairy things isn't nice, Reptiles are an intresting solution. They're dry, can live inside glass boxes (which means the mess they make are very well-contained), and don't need much maintenance.

And the final solution i will mention here is fish. Fish spend most of their life entirely submerged in water, making them an interesting idea. The mess they make is easily filtered out by pump systems, they make very little noise, are constantly cleaned by the water, and are probably the exact opposite pet from dogs.

## What can i do?
Whilst dogs may be some of the worst pets in the world, they still have a valuable part in society as working dogs. The blind need them. Sheep farmers need them. Police need them.

Dogs can be made better, but only if actual action is taken. Even domestic dogs can be made tolerable. Training, the one thing dogs are best at, can be used to great effect.

To find out how, read the second part of this document.
            `
        },
//         {
//             "name":'2023-06-06: Newpage',
//             "description": "My first blogpost, where i discuss this software",
//             "tags": ["blogbits", "coding"],
//             "content":`
// # Newpage
// In this blog post, i'll be discussing how i built this thing.

// ## Demand
// It started with the need for someplace to publish my ideas. Previously, i had used a mix of pastebin.com and rentry.co links (which are still up at time of writing). However, this was very hard to organise and control. Links kept being lost and editing was impossible, forcing a "shifting cultivation" of content.

// This became unmanageable and i had to stop.

// ## Creation
// The first thing i did was decide what to do. I decided that instead of having separate HTML pages for each idea and writing i'd come up with, which would quickly become bloated and boilerplated, i decided to use a JSON directory. This solution would also allow me to add more then just the content, meaning that the solution was rapidly extensible.

// I decided on a rather spartan design to start from, where the top would have a simple box and the bottom a list of content to select from.

// Since my website was already built upon Bootstrap and jquery, i decided to use these reliable technologies for this website.

// The website uses a disparate array of hacked-together functions i no longer understand.

// However, now that the website exists, you can finally see this blog post.

// ## Future Possibilities
// In the future, i believe that a full refactoring of the JS would be in order.

// Alongside this, a clearer and more well-designed tag system (with colours and even different shaped badges) could also help.
//             `
//         },
        {
            "name":'A new story format: the Microstory format',
            "description": "A story format focused on punchy, dramatic stories rather then slow burns.",
            "tags": ["ideas", "stories", "microstories"],
            "content":`
## Introduction
Microstories are stories made for the modern tiktok generation. They're bite-sized pellets of plot. If we were to think of stories as "life with the boring parts cut out", microstories are "stories with the boring parts cut out". They waste no time getting to the crisis of the story, conveniently trimming off part of the exposition to get straight to the trigger.

## General pattern
A microstory will normally start near or during the dramatic part. The characters are introduced as needed.

The drama normally continues for the vast majority of the microstory.

A microstory will usually make surround a central theme and set the reader thinking after the end. Unresolved endings are important as they leave the perfect socket for the sequel to plug in.

## Breakdown
Let's analyse the following microstory, titled "diverse teaching":

> She sat inside the classroom, glaring at her teacher. Today she was learning about холодомор.
> 
> "I've had enough!" she screamed in Russian. "Пазол нахуй!" she cussed as she threw her chair back into place. She left the room and slammed the door.
> 
> An hour of shocked silence and whispered teaching followed. The class ended and a new one began, using the same teacher. We're seeing an example of what was taught.
> 
> Suddenly, the door opening pulled air through the open window, bringing a vast air to the room. The nearby leaves waved around. One fell off and hit her in the face. She brushed it off.
> 
> "What are we talking about?" she asked.
> 
> "Extrinsic factors" the psychology teacher stated.
> 
> And i sat in my chair, watching this happen. I'd react the same if we discussed colonialism and the natives being murdered. In the international school, boundaries fail to matter.
> 
> The east india company was next on the syllabus. Let's see where i fail to cope.

The first thing we should notice is the fact that this story is short; only 165 words. This is what makes the microstory "micro". Also notice here how the entire story is based around the drama, with only two paragraphs spent not having drama.

Also notice how "she" isn't even given a name nor goal nor any character development; this is so that more words are spent with drama.

What this story does is, whilst focusing on the drama, it also focuses on the theme of shared experiences.

Also note that the end is left unresolved; the reader is left with tension wondering about if the first person will be able to cope with being taught their grizzly heritage. This is perfect for the next microstory, which may discuss how the writer holds in tension, maybe even using a different theme, like capitalism.

## Translating novels
Microstories and novels are basically opposites; one is long and filled with intricate character development, the other is just action-packed scenes. The long microstory format is needed for most novels.

To translate a novel whilst maintaining the intricate character development and subplots, one can reveal the characters through each dramatic scene.

For example, taking from Charles Dicken's Oliver Twist, specifically "CHAPTER VIII. OLIVER WALKS TO LONDON. HE ENCOUNTERS ON THE ROAD A STRANGE SORT OF
YOUNG GENTLEMAN", we might condense the story as so:

> Oliver walked twenty miles that day; and all that time tasted nothing
> but the crust of dry bread, and a few draughts of water, which he
> begged at the cottage-doors by the road-side.
> 
> Oliver limped slowly into the little town of Barnet. Not a soul had 
> awakened to the business of the day.
> 
> He was roused by observing that a boy, who had passed him carelessly some
> minutes before, had returned, and was now surveying him most earnestly from the
> opposite side of the way.
> 
> “Hullo, my covey! What’s the row?” said this strange young gentleman to
> Oliver. “Going to London?”
> 
> “I suppose you want some place to sleep in to-night, don’t you?” asked
> the boy.
> 
> “I do, indeed,” answered Oliver. “I have not slept under a roof since I
> left the country.”
> 
> The young gentleman offered shelter.
> 
> This unexpected offer of shelter was too tempting to be resisted.
> 
> Oliver reached the house.
> 
> “Now, then!” cried a voice from below, in reply to a whistle from the
> Dodger.
> 
> “Plummy and slam!” was the reply.
> 
> “Who’s the t’other one? Where did he come from?”
> 
> “Greenland. Is Fagin upstairs?”
> 
> “Yes, he’s a sortin’ the wipes. Up with you!” The candle was drawn
> back, and the face disappeared.
> 
> He threw open the door of a back-room, and drew Oliver in after him.
> 
> The two went upstairs. In a frying-pan, some sausages were cooking;
> and standing over them, was a very old shrivelled man, whose
> villainous-looking and repulsive face was obscured by matted red hair.
> 
> “This is him, Fagin,” said the boy; “my friend Oliver Twist.”

Note how a lot of details have been ommited; this is only 262 words long where the original chapter comes in at a whopping 3,200 words.

However, 262 is still at the extreme end of the microstory size; 300 is the absolute maximum.

Note how parts of the original story are preserved; removed was some bits about the journey and instead the actions inside London. Everything had to be contracted and trimmed, but that's the thing about stories. They can be endlessly contracted and expanded. In fact, the whole chapter can be condensed to a single sentence:

> Oliver twist spent days walking to London, with much suffering, meets a boy, and is taken to some dodgy lodgding

The whole chapter can also be expanded to 100,000,000 words by adding details between things. The key is to find a balance.

## Long microstories
A "long microstory" can be thought of as a TV series. Some long microstories might be like a sitcom, where every episode is disconnected but the characters are the same. Some might be like a more interconnected series. Here, whilst every microstory is still separate and can be consumed like a sitcom, the entire long microstory builds up to a greater arc.

A "long microstory" can be anywhere from two microstories to infinite microstories, as long as each component can be consumed separately and fits the restrictions of the microstory format.

## Benefits
### Readers
You might be reading this all and thinking "What's so good about this limited medium?!". In fact, here's a Haiku (a famously limited format) about what you might be thinking:

> Short stories aren't short
>
> Microstories are too short!
>
> Is anything right?

First, theres the fact that it's easy to consume these stories. When a story fits within what Linus Torvalds (a very clever man) calls "screenfulls of text", the TikTok generation suddenly realises "oh yeah, i have enough time to read this", and we're done! We tricked them into reading literature! Microstories could serve as some form of "gateway format", where they may tolerate a few paragraphs of boring stuff, then 10 paragraphs of boring stuff, and then they're reading novels!

The same has happened with long form content. Some people are discovering the slow burn of more and more information slowly building up to a satisfying conclusion over the course of an hour to be quite amazing compared to the BAM BAM I HAVE SOME INFORMATION HERE IT IS BYE style of short-form content. It must be noted however, that a lot of people are too addicted to short-form content to even find long-form content. These people are lost and it's no longer worth it trying to market longer content to them (which is why we might start with microstories).

## Writers
The story format, with its limitations, might actually force good work. This has happened countless times with other limited forms. Haiku, with a strict syllable structure, has still made amazing work:

> An old silent pond...
>
> A frog jumps into the pond—
>
> Splash! Silence again.

Notice that in three lines an image is made, disrupted, and calmness is brought back. Microstories are less limited, so potentially with the correct writer, more vivid imagery can happen.

Since microstories are so small, they are also an intresting benchmark to compare writers on. Instead of the marathon of novels, we have a fast burn, 250 words, and a race to see how much plot or emotion writers can put in a story!

Microstories are easy to write. You can spend less time making scenarious and more time making them more emotional. You don't need to write whatever thousand words; you have to compress that story into less then 500 words. Run out of space? Refine and get to the real drama!

Plus, microstories by default give flexibility. If you want to convey more info, you can use the long microstory format. Since each story is independent from the other, you can convey different points of view, different times, and many other elements that would be traditionally fixed in other stories.

For example, a retelling of Macbeth might start with the witches having a talk with macbeth, from the witch's point of view, move onto another microstory from King Duncan about being killed, and so on.

## Essential elements of a microstory
Let's end this document by discussing what you need to have in your own microstory. Any microstory:

* Must have less then 300 words, and should have less then 250 - This keeps readers with short attention spans able to read and absorb the story.
* Must have a dramatic scene - This also keeps the readers engaged.
* Must have some central theme - This is something that readers keep after ending the story.
* Must have some unresolved ending - You mustn't end in the middle of drama, but leave some part of the story unresolved. Leave the reader thinking but not fustrated.
* Should have vivid descriptions - Make the reader's image technicolour. Normally mine are monochrome and fizzly, but every now and then i get some technicolour image from a story. Make this happen!
* Should show, not tell - Obviously, with a microstory, some telling will end up having to be done to prevent the word count exploding, but where possible, we should SHOW what happens.
* Be self-sufficent - You should be able to distribute any microstory from a long microstory without clarifying any details

Additionally, any microstory from a long microstory must:

* Link into each other - Take that unresolved ending and resolve it in the next microstory to make it better link. Do not break self-sufficency!
            `
        },
        {
            "name":'Diverse Teaching',
            "description": "A microstory inspired by my school",
            "tags": ["microstories", "stories",],
            "content":`
She sat inside the classroom, glaring at her teacher. Today she was learning about холодомор.

"I've had enough!" she screamed in Russian. "Пазол нахуй!" she cussed as she threw her chair back into place. She left the room and slammed the door.

An hour of shocked silence and whispered teaching followed. The class ended and a new one began, using the same teacher. We're seeing an example of what was taught.

Suddenly, the door opening pulled air through the open window, bringing a vast air to the room. The nearby leaves waved around. One fell off and hit her in the face. She brushed it off.

"What are we talking about?" she asked.

"Extrinsic factors" the psychology teacher stated.

And i sat in my chair, watching this happen. I'd react the same if we discussed colonialism and the natives being murdered. In the international school, boundaries fail to matter.

The east india company was next on the syllabus. Let's see where i fail to cope.
            `
        },
        {
            "name":'Olsen in the backrooms',
            "description": "A short microstory based off a longer, unfinished dual-story novel set in the backrooms",
            "tags": ["microstorie"],
            "content":`
The smell of chlorine hits me as i step into the temperate water. It's a welcome relief from earlier. No monsters here, no need to go anywhere. Nothing to do, nothing to distract me, just endless randomly generated pools lining every floor, the occasional fountain providing a regular shower.

I don't give a fuck about anything anymore, not the convoluted water purification system nor the alkalinity of this water.

READ THIS TEXT IMAGINE A KNIFE CUTTING INTO A SOAP AND SLOWLY TAKING OFF SATYSFING FLAKES I LOVE THIS YOU LOVE THIS WE HAVE YOUR ATTENTION

As i sit inside, i take a breath and finally let myself relax.

My entire life has been building towards this final relief as i slowly burn away into nothing but a big red raw plum.

READ THIS TEXT IMAGINE SOMEONE PLAYING SUBWAY SURFERS AND GETTING COINS AND HOW YOU'D PLAY BETTER THEN THEM BECAUSE FORESIGHT IS 20/20 IMAGINE THIS DO WE HAVE YOUR ATTENTION

And i finally remember my entire life, from my childhood and those endless stories i wrote to the short-term youtube shorts grabbing my attention until this place took the rest of my life.

And here i'll be, for the rest of my short life. Maybe i sniff some chlorine tablet. Good idea, i think.

READ THIS TEXT IMAGINE A FUNNY SCENE FROM FAMILY GUY IT'S REALLY FUNNY ISN'T IT DON'T MIND THE STORY YOU CAN REREAD THIS TEXT AGAIN BUT JUST LISTEN TO FAMILY GUY LOL DO IT FOR FUCKS SAKE

And as everything fades away, the final piece of peace,

i,

i,

i'm back, reincarnated in this sick simualtion, back in level !
            `
        },

        {
            "name":'NewSearch',
            "description": "A new searching system",
            "tags": ["ideas", "coding"],
            "content":`
Introduction
------------

Historically, search engines such as Google and DuckDuckGo have been centred around using text to basically ¿¿putabacktickherectrl¿¿putabacktickhere+¿¿putabacktickheref¿¿putabacktickhere across the internet.

Whilst this approach works extremely well for quickly finding answers and solutions, it leaves most of the internet obscure. Ever heard of the bucket ring? Euphoria.io? Anything published by “D!NG”?

Search engines are horrible at this. What about a “website index”, where people can submit their websites and have them listed?

User views
----------

Each type of user sees a different thing. Mostly, it's a bit like Reddit, but a TRUE homepage of the internet.

### Members

Members form the majority of the recorded population. They have an account, with which they may submit comments and “upvotes” and “downvotes”.

Each member can “subscribe” to tags and see new things.

The website is organised into pages based off tags mixing; for example, “shopping” may be a tag, and then we might have “computing” as another, and maybe later “adobe”, to show where we can buy adobe products.

### Non-members

Non-members can do most of the same stuff as members, but from a read-only perspective. They cannot upvote or downvote, subscribe to tags, or leave comments.

### Website-contributors

Webmasters and non-associates of websites may submit websites for review. They must first be a memeber. They write a description, some tags, and send it for approval.

Webmasters may opt-out of hotlinking.

### Admins

Admins are those in control of managing all websites and tags. They can easily create and remove tags, alongside bulk actions on websites. For example, they can rename tags and split or merge tags.

### Tags

Tags form the bread and butter of the system. Every tag categorises websites and people can add or remove them from their search. 

The tag system might categorise ContentDB with these tags:

*   Associated websites (only websites with a direct link)
    *   minetest.net
    *   rubenwardy.com
*   Content
    *   mods
    *   games
    *   minetest
    *   community
*   Features
    *   likes
    *   dislikes
    *   comments
    *   community-uploads
    *   open-source
*   Technology
    *   bootstrap
    *   sql
    *   https

And Youtube with these:

*   Associated websites (only websites with a direct link)
    *   google.com
*   Content
    *   community
    *   videos
    *   music
    *   gaming
    *   algorithm-based-working
*   Features
    *   likes
    *   comments
    *   community-uploads
    *   closed-source
*   Technology
    *   ???

Client-server relationship
--------------------------

The server holds ALL the data, from the tags, websites, descriptions, and comments.

When a client requests data from the server, it will also end up with:

*   The HTML template to work on
*   Javascript code
*   Some data:
    *   The entire list of tags (could be bad; later optimisation needed)
    *   A collection of objects relating to the websites and their information

Pages
-----

These are the main pages, ordered by time i will finish programming them:

*   ¿¿putabacktickheretags.html¿¿putabacktickhere
    *   Is a directory of tags
    *   Shows us the tags, their descriptions, and how many websites have them.
    *   Lets us sort
    *   Server finds the tags and their descriptions, sending them to the client
*   ¿¿putabacktickheresearch.html¿¿putabacktickhere (with the ¿¿putabacktickhere?search=¿¿putabacktickhere argument)
    *   Shows us websites matching those tags
    *   Server-retrieved, client-sorted and presented
*   ¿¿putabacktickhereindex.html¿¿putabacktickhere
    *   Introduces the website
    *   Tells us about the stuff
    *   Lets us do some browsing (by transcluding browse.html)
*   ¿¿putabacktickherebrowse.html¿¿putabacktickhere
    *   Shows us some curated websites, based on metrics:
        *   100 most used websites
        *   100 most upvoted websites
        *   5 randomly-picked >10 upvote websites
*   ¿¿putabacktickheretagManager.html¿¿putabacktickhere
    *   Is a place for admins to perform bulk tag actions
    *   Allows creation and removal
*   ¿¿putabacktickherewebsiteManager.html¿¿putabacktickhere
    *   Is a place for admins to perform bulk website actions
    *   Allows retagging, creaiton, and removal.
*   ¿¿putabacktickhereuser.html¿¿putabacktickhere
    *   Is a login screen
    *   Used to access the website as a member

Database
--------

The website uses SQLite to hold data. This will be my first time with databases (!!) so wish me luck.

Here are the data storage devices:

*   Websites
    *   SQLite
    *   Stores:
        *   Website names
        *   Website descriptions
        *   Websites' tags
*   Tags
    *   JSON
    *   Stores:
        *   The tag name
        *   The tag description
        *   The tag category
*   Users
    *   SQLite
    *   Stores:
        *   Usernames
        *   Passwords
        *   Permissions
        *   Other user-specific data
*   Community
    *   SQLite
    *   Stores:
        *   On a website-basis
            *   Comments
            *   Upvotes and Downvotes

There is some duplication between community and website databases. To help clarify what i mean:

The **Website Database** is for storing information on Websites; it should be a standalone portal to the rest of the web.

The **Community Database** is for storing information on User Contributions. It acts as a seperate checker and verifier of the community.

Contributors of websites
------------------------

Users are always the first contributor of websites; their hard work ensures that websites are documented in the first place.

However, alongside the user, there is also a series of bots which “crawl” across the web. They look for new domains and try to scrape for info relating to them.

Let's see how a bot might find ¿¿putabacktickhereforums.minetest.net¿¿putabacktickhere, for example:

*   Discovers main page
*   Notes down PHPBB as a technology tag
*   Notes down the description
*   Finds subdomains

And then this is added to the queue to be managed later

Backups
-------

The server will automatically backup all databases into a zip archive, with these frequencies:

*   Yearly
    *   Never deleted
    *   Happenes every date who's month component is 01 or 06, with a 00 day component.
*   Monthly
    *   Deleted after 6 months
    *   Happens every day which has day component 00
*   Weekly
    *   Deleted after 3 months
    *   Happens every date of which the day component modulus by 7 equals 0 happens 
*   Daily
    *   Deleted after 30 days
    *   Happens at 00:00
*   Hourly
    *   Deleted after 24 hours
    *   Happens at minute-mark 00

The backups overlap; the names have a timestamp to allow us to automaticalpy remove things that happen.

The backup library is a seperate library owing to its utility. It is configurable.

A backup is made every hour; the way backups with larger frequencies are made are by not deleting them until they get too old.

Differences from existing engines
---------------------------------

There have been previous categorisation attempts. Each of these have failed or are closed to the public; mine indexes the web in a very public manner with a very large tag system. 

My system also differs from traditional search engine; little focus is on finding the content and searching through that, but more rather in categorising websites and what they're FOR. This engine is horrible for finding solutions to that error on StackOverflow, but it's great for finding unique novel websites.

The main data stored is what the websites contain and what they're for. 

### Other

The websites are shown as “preview” iframes. Failing that, screenshots are used. 

Iframes will only ever be loaded if neccesary.

The entire thing is under MIT license, including the website database. The user credentials and contributions are mostly private.

The website instance i own does not allow any website mainly used for the following content:

*   Porn
*   Gambling
*   Hard alcohol
*   Terrorist organisations
*   Drugs
*   Hate speech
*   Graphic content
*   Illegal activities
*   Instructions for illegal activities
*   Pirated or copyrighted material
*   Phishing or scamming activities
*   Malware
*   Self-spreading programs
*   Spam or unsolicited advertising
*   Personal or sensitive information without consent
*   Harassment or cyberbullying
*   Child exploitation or explicit material involving minors
*   False or misleading information
*   Political propaganda or extremist ideologies
*   Offensive or abusive language
*   Animal cruelty or unethical treatment of animals
*   Invasive surveillance or privacy violations
*   Counterfeit goods or replica items
*   Witchcraft or occult practices
*   Nudity or explicit content
*   Weapons, firearms, or ammunition
*   Hacking or unauthorized access to systems
*   Illegal substances or drug paraphernalia
*   Fraudulent activities or scams
*   Ponzi schemes or pyramid schemes
*   Virus hoaxes or false information about diseases
*   Stalking or harassment of individuals
*   Plagiarized or stolen content
*   Unauthorized sharing of personal information
*   Copyright infringement or intellectual property violations
*   Unauthorized streaming or distribution of copyrighted material
*   Prostitution or solicitation of sexual services
*   Racist or xenophobic content
*   Cults or extremist religious groups
*   Bullying or cyberbullying
*   Phobias or triggering content without appropriate warnings
*   Graphic or disturbing medical procedures
*   Dangerous or harmful activities that could result in bodily harm
*   Advocacy for self-harm or suicide
*   Defamatory or libelous statements about individuals or companies
*   Encouragement of illegal activities or illegal behavior
*   Graphic violence or gore
*   Animal fighting or cruelty
*   Scatology or explicit discussions of bodily functions
*   Voyeurism or non-consensual sharing of intimate content
*   Discrimination based on race, ethnicity, religion, gender, or sexual orientation
*   Violation of privacy laws or regulations.
*   Zombie-related content that may induce panic or incite zombie apocalypse fears
*   Promotion of time travel or interdimensional portal devices without proper authorization from relevant authorities
*   Content involving sentient vegetables or fruits engaging in illicit activities
*   Unapproved recipes for potions, spells, or magical concoctions
*   Discussions or tutorials on building functional yet illegal robots or cyborgs
*   Historical revisionism or denial of well-documented events, such as the moon landing or the existence of dinosaurs
*   Promotion of intergalactic colonization or extraterrestrial immigration without proper interstellar visas
*   Unsubstantiated conspiracy theories involving shape-shifting reptilian overlords or sentient household appliances
*   Seditious content encouraging rebellions against fictional or non-fictional governments
*   Guides on how to communicate with imaginary friends, including tips for ghost summoning or telepathic communication with unicorns
*   Unlicensed fortune-telling services or psychic predictions that guarantee world domination or eternal youth
*   Content glorifying the use of superpowers or supernatural abilities for personal gain or global domination
*   Promotion of secret societies or exclusive clubs that require initiation rituals involving exotic animals or mythical creatures
*   Unauthorized exploration of parallel dimensions or alternate timelines without proper interdimensional travel permits
*   Discussions on conspiracy theories involving alien encounters at fast-food drive-thrus or encounters with time-traveling Elvis impersonators
*   Promotion of underwater cities or habitats for mermaids and mermen without proper environmental impact studies or aquatic zoning permits
*   Guides on constructing fully functional pirate ships or airships without proper maritime or aviation certifications
*   Unverified claims of possessing the philosopher's stone or other mythical objects with supernatural powers
*   Promotion of forbidden love affairs with vampires, werewolves, or other supernatural creatures without consent from their respective supernatural councils
*   Content involving telekinetic or mind control techniques for purposes of pranks or mischief without considering potential ethical implications or psychological damage.
*   Unapproved interplanetary colonization plans or guidelines for establishing civilizations on celestial bodies
*   Content encouraging the creation of time loops or disruptions in the space-time continuum for personal amusement
*   Unauthorized promotion of mind-reading technologies or telepathic communication devices
*   Guides on building functional, unregulated teleportation devices or wormhole generators
*   Discussions or demonstrations of summoning legendary creatures, such as dragons or phoenixes, without proper permits from mythical creature conservation agencies
*   Promotion of illegal intergalactic smuggling operations involving exotic alien species or rare extraterrestrial artifacts
*   Unverified claims of possessing the Holy Grail, the Ark of the Covenant, or other mythical religious relics
*   Content advocating for the use of mind-control techniques on elected officials or world leaders
*   Unauthorized development or distribution of advanced artificial intelligence systems capable of overthrowing governments or initiating global dominance
*   Discussions or tutorials on creating permanent invisibility cloaks without proper consent from the International Association of Illusionists and Tricksters
*   Promotion of time-travel tourism packages without ensuring adherence to historical preservation laws or the prevention of temporal paradoxes
*   Unsubstantiated theories or guides on communicating with extraterrestrial civilizations without proper interstellar communication licenses
*   Content encouraging the establishment of sovereign nations in unclaimed territories, such as underwater or on unexplored islands
*   Unauthorized promotion or sale of elixirs or potions promising eternal youth or immortality
*   Guides on summoning interdimensional beings for casual conversations or entertainment purposes
*   Content involving conspiracy theories about ancient civilizations being ruled by time-traveling dinosaurs or sentient robots
*   Unapproved content promoting mind-transfer technologies or consciousness uploading without ensuring ethical considerations and consent
*   Discussions or guides on building functional space elevators without proper certifications from celestial infrastructure regulatory bodies
*   Promotion of intergalactic sports tournaments or betting on alien competitions without valid interstellar gambling licenses
*   Content glorifying the use of mythical artifacts or mystical relics for personal gain or world domination
*   Unverified claims of possessing the Fountain of Youth, the Philosopher's Stone, or other legendary sources of eternal life
*   Unauthorized exploration or settlement of parallel dimensions inhabited by fictional characters or creatures from folklore
*   Content advocating for the establishment of utopian societies based on unconventional principles, such as gravity-defying cities or cities made entirely of chocolate
*   Discussions on conspiracy theories involving time-traveling clowns or secret societies of shape-shifting circus performers
*   Unlicensed tutorials on building functional, autonomous robot armies without proper regulation or consideration for global peace
*   Promotion of interdimensional trade routes or smuggling operations involving rare and exotic interdimensional goods
*   Guides on creating functional, unregulated devices for mind control or hypnosis on a global scale
*   Unsubstantiated claims of possessing magical artifacts, such as the Sword in the Stone or Aladdin's Lamp
*   Content advocating for the establishment of interplanetary colonies without proper consideration for sustainable resource management or extraterrestrial environmental impact studies
*   Unauthorized discussions or promotion of time-manipulation techniques, including time loops, time dilation, or temporal anomalies
*   Content encouraging the use of mind-reading or mind-control abilities on unsuspecting individuals for personal entertainment or amusement
*   Guides on constructing interstellar spaceships without valid intergalactic travel licenses or adherence to cosmic traffic regulations
*   Discussions or demonstrations of summoning interdimensional beings for mundane tasks or trivial matters
*   Promotion of forbidden love affairs with mythical creatures or legendary beings without proper consent from their respective mythical creature councils
*   Other “bad” stuff, as defined by me, maybe including:
    *   Twitter
    *   Russian-government-backed websites with little value

However, owing to the open-source nature, these bad things can be circumvented. Some wierdo may put a porn and drugs directory with a side of hard alcohol.
`},{
            "name":'Maths system',
            "description": "A new, purer way to notate maths",
            "tags": ["ideas", "maths"],
            "content":`
Introduction
------------

This new system is designed to make maths operators intuitive. Every operator here is formed of multiple morphemes. Morphemes can be combined in several ways and make it simple to come up with new operators. The entire system is in SVO order normally.

Every operator is a "verb" and they can accept multiple inputs.

Arithmetic
----------

For example, let's see how we'd replace +, &#x60;*, ^, and higher. First, we specify the level of which the operator functions. That is 1, 2, and 3 respectively. And then we add the "put-together" morpheme, <, and that specifies we put things together:

¿¿putabacktickhere2*2 = 2,2<,2¿¿putabacktickhere

¿¿putabacktickhere4^3 = 4,3<,3¿¿putabacktickhere

Now, if we wanted to do the logical opposite, -, /, sqrt(), and others, we can just add a negation:

¿¿putabacktickhere4-7 = 4,1< -,7¿¿putabacktickhere

¿¿putabacktickheresqrt(4) = 2,3< -,4¿¿putabacktickhere

But there's a secret! It turns out that some things are "default values" and do not need to be specified. For example, 1 and < are both default values, so this would be perfectly valid:

¿¿putabacktickhere2+43 = 2,,43¿¿putabacktickhere

And so would this:

¿¿putabacktickhere4-2 = 3,-,2¿¿putabacktickhere

Commas and Brackets
-------------------

The commas are optional and can be replaced with spaces for style (but spaces must never be moved as in some cases they create ambiguity):

¿¿putabacktickhere400-230 = 400 - 230¿¿putabacktickhere

Brackets are perfectly valid as order-specifiers:

¿¿putabacktickhere(400*2)+3 = (400,2,2),,3¿¿putabacktickhere

Decimals and Negative numbers
-----------------------------

So, let's try some other stuff. what about negative numbers and decimals? There is a "pure" way to do that; just multiply by 10 by a negative number.

To make negative numbers, just use the negative form with no object:

¿¿putabacktickhere400 -¿¿putabacktickhere

Since there's no object, the subject can go anywhere:

¿¿putabacktickhere- 400¿¿putabacktickhere

So, to make 0.4, we can do.

¿¿putabacktickhere4,2<,(10,3<,(- 1))¿¿putabacktickhere

Assignment
----------

Now, let's move onto some assignments.

Assignment will always act on a "letter". The letter is assumed to be the output given no valid letter or assignment operator. Assignment uses =, and we have several degrees of accuracy, from high (', equals) to mid (;, approximately), low (., does not equal). We can also specify a magnitude using < and - like we did with arithmetic, alongside our accuracy degrees.

¿¿putabacktickhere3 = 3 - > 3,=',3¿¿putabacktickhere

¿¿putabacktickhere7 ~ 6.9 - > 7,=;,(69,2<,(10,3<,(- 1)))¿¿putabacktickhere

¿¿putabacktickhere4 > 3 - > 4,='<,3¿¿putabacktickhere

¿¿putabacktickhere6 <= x - > 6,=;,x¿¿putabacktickhere

A "letter" is just a place to store data; they are encased in the following:

*   .a. - Reusable macro that acts on data
*   'a' - Some data is stored inside and is retrieved whenever it is the subject (but will not do anything to data)
*   "a" - Fragment of text; can be used anywhere and will automatically be ignored.

Macros
------

Now, let's look into simplifying stuff. We can make macros, which are like functions. To define a macro, we make a letter (a) and surround it in the macro syntax ( .a. )

For example, to make our very own exponent operator, we can do:

¿¿putabacktickhere{$,2<,(10,3<,($))} ,=', $1.e.$2¿¿putabacktickhere

Note how the dollar sign acts as an "argument placeholder" The numbers are assigned in order from left to right, with the next one to appear regardless of brackets is given a number.

Once we have made a macro, it becomes our own little operator. For example, we can define 6.2 as:

¿¿putabacktickhere62.e.(- 1)¿¿putabacktickhere

But what if we want to shorten it even more? We can do overloading to allow us to specify different things given different arguments.

¿¿putabacktickhere¿¿putabacktickhere¿¿putabacktickheretext-plain
{$,2<,(10,3<,($))} ,=', $1.e.$2
{$,2<,(10,3<,(- 1))} ,=', $1.e.
¿¿putabacktickhere¿¿putabacktickhere¿¿putabacktickhere

So now, if argument two is not suppled, we see if the next definition exists and accepts our arguments.

¿¿putabacktickhere62.e.¿¿putabacktickhere

Iteration
---------

Now then, let's try some iteration. We just need to supply a set.

¿¿putabacktickhere{62, 43, 342, 24, 67}.e.¿¿putabacktickhere

Here, the macro is now run for everything, giving us 6.2, 4.3, 34.2, 2.4, and 6.7 respectively.

But what if we wanted different exponents? We use different brackets. There are two main types; strict-matching, which is a list of arguments for each argument in the first set, and re-doing, which loops over the strict-matched set for each element.

See this strict matching on both sides

¿¿putabacktickhere{23, 43, 99}.e.{(- 1), 2, 4}¿¿putabacktickhere

This gives out 2.3, 430, and 99,000 respectively.

And non-strict matching here:

¿¿putabacktickhere{23, 43, 99}.e.[(- 1), 2, 4]¿¿putabacktickhere

Here are the results::

2.3, 4.3, 9.9, 230, 430, 990, 23,000, 43,000, 99,000.

Note how the re-doing works here; for each element in the square brackets ( &#x60;[&#x60;] ), the first set is iterated over. This is incredibly powerful.

Influences from Linguistics and Programming
-------------------------------------------

The programmers and linguists among you may now already be seeing some similarities with linguistics and programming. This is true; programming and linguistics were massive influences.

First of all, this system is designed using a system of logical meaning units, like languages and some programming languages are designed. This is a natural result of a linguist making a mathematical system. It also makes sense.

Also, iteration and defining macros also comes from a cross between maths and programming.

### Tangent: Editors and interpreters

Seeing as this maths notation system shares a lot in combination witn programming, i definately feel that an editor would make it a lot easier to use, from brackets going smaller in reverse porpotion to the nesting depth to colouring things up.

Interpreters are also an intresting idea, but one must remember one thing: treat it like parsing calculator expressions, not a programming language!

Input and ouput should be done through the usage of assignments, either without a subject or operator.

Whitespace should be ignored. The language is designed to be usable without whitespace.

Standard Library
----------------

Just like a programming language has a standard library to prevent endless pain, so does this. Just copy in the relevant macros and you can now use higher-level functions. For example, one might copy in this wrapper function for multiplication.:

¿¿putabacktickhere($,2<,$),=',$1.*.$2¿¿putabacktickhere

I must stress, however, that the standard library must only be used when neccesary; students and learners of operations must use the original system for a long time before moving onto this.

Reference
---------

Note: x is used as a placeholder value

| Morpehme | Meaning | Default? |
| --- | --- | --- |
| ,x, | A verb | Yes |
| .x. | A macro |     |
| “x” | A comment |     |
| ‘x’ | A value |     |
| <   | Defines “put-together” or some form of non-negative thing | Yes |
| &#x60;-  | Negates anything |     |
| (x) | Shows an expression that must be calculated before its surroundings |     |
| $   | A placeholder value, for either a variable provided an argument, or a location to place an argument |     |
`},{
            "name":'Microwave',
            "description": "A story based off a prompt from Reedsy",
            "tags": ["ideas", "stories"],
            "content":`
I sit in the staff room. The light's on, and the sun is quite high. It's nice and warm in here. It's a warm hospital, kept at the temperature of 21ºc. I like it. It's where i've lived for the last 30 years, surrounded by death, life, and rebirth in the core of the hospital, the grease that keeps the churning machine alive.

It's a medium-sized boxed off staff-room. There's a table in the middle, the counter and mini-kitchen. There are posters on the walls related to medical conditions and stress alleviation. It's an old staff-room, set in the core of the oldest part of the hospital. This entire hospital is desperately expanding to meet the needs of those in the nearby city, an onion of age. Walking through this part of the hospital is difficult.

There aren't many windows in here save for that high frosted window, full of light and hope at this moment. The light shines in with midday vigor and lights up the old, peeling walls and the stained dropped ceiling.

It's 07:34, at least i think so.  My circadian rhythm is completely off after hours on and hours off and i don't even know what the time is. 

The door swings wide open and a cold breeze comes in. It's probably the winter, a bleak and cold time.

A nurse in a light blue gown sits down on the chair. Another one comes in a short while later, dressed in a similarly cheap green gown. .

“I've been working with Antony.” the one in the light blue gown sparked. “how can we make sure his poor leg is fixed quickly? He's got a big conference to go to next week, and he'll be gutted if he can't go.”

“He's… getting better as we speak. Soon we'll be able to put on a cast, and, send ‘im off”, shrugged the green gown one. “I think we just need, to wait, unfortunately. Different people… some are just slower." they commented.

“I almost got hit this morning. People shouldn't really cut off on the motorway”, the first nurse reported. "I could have been the next Antony, imagine that!”

“Yeah, how's, James, by the way?” asked the second nurse, laden with some form of concern. “I've heard that the hot steel didn't do any… good” he continued..

“Not very well. His face is still trying to recover, poor thing” said the first nurse. “It's horrible, what these companies have been doing to people”.

“Maybe we should... unionise” the second nurse joked dryly.

“Yeah, maybe we should” the first nurse replied, unaware of the joke. “It's just a big joke, this government. Do you think it'd work?”

Before the first nurse could reply, another nurse burst through the door and seated themselves at the table. 

“Poor Rachel, all on her own” the newly-arrived nurse worried. “I checked up on her, she's still troubled, poor thing.”

“Could we get her any form of help?” the same nurse continued. “It's a bit concerning and CAHMS isn't coming until later this Thursday, sadly”

Rachel, from what i've heard at sunrise, is really struggling with the stress of her life. Poor Rachel, all on her own. 

“Yeah, maybe we should give her, some warm, tea?”, said the endearing second nurse. “She likes… chamomile, i've heard".

“That reminds me! Antony needs some tea too”, the first nurse said. “He likes chamomile too, says it's good for the bones”.

From what i've heard, tea is not their only similarity. They also go to the same college and drive the same type of car.

For most of the time here this morning, i have been sitting around passively, thinking about life and listening to the constant stream of conversation. I like my job. I get to have 24/7 access to electricity, all the staff love and endear me, and i'm probably one of the main reasons most of the staff are alive today. I'm probably the nurse nurse, maybe?

The blue-dressed nurse also stood up and put a teabag in a paper cup and set the kettle to boil. The newly-arrived nurse was also at the kitchen, preparing her pot noodles.

The nurse who had just arrived came up to me, opened my mouth, and gave me pot noodles. My mouth was closed around them and it was time to warm them up after some panicked stabs at the buttons.

WHIRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR

The nurse checked on the pot noodles. They're still raw.

WHIRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR

Again, they came up to look, obviously quite hurried.

WHIRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRDINGDING

And at last, i was done. They opened my mouth and took the pot noodles from my now-humid interior.

The kettle, which i had not noticed was shining it's “i'm heating up the water” light, was beggining to bubble itself apart, a 20 year old contraption. 

The blue-dressed nurse took the pre-prepared paper cups with their teabags and added some water from the kettle. They passed a cup on to the other nurse. They walked out of the room, most likely to their patients.

Just after they left, a doctor stumbled in and collapsed onto a chair. They looked quite tired. It's not the normal “i had a long day” tired, but a much deeper “fuck the system” tired. Deep wrinkles criss-crossed across their 30 year old face, and their eyelids dropped in such a way that it was like they had wanted to stay shut for many more hours last night, for way too many “last night”s.

“You know what, we should unionise!” they said. “It's exhausting having to work for hours. I've done 5 hours of work and i have to do 2 more!” 

“You're lucky” the newly-arrived nurse said, their face young, hiding years of abuse by their alarm clock and the pager. “I have 12 and i've only done 3 so far.”

The doctor took out a box of homemade pasta. It was well made, so maybe it was bulk-made in 

Again, it's time for my duty. Still moist from the pot noodles, some homemade pasta was emplaced within.

WHIRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRDINGDING

My mouth was opened again and the nice warm pasta was taken out.

A nurse comes in. He was pale white. “i had a rough shift” they said. ”There was a lot of blood earlier. A LOT"

“So much blood… so much” he complained. “At least it's quiet now so i can focus on what's important”.

He went to the table and opened his laptop, entering what seems to be the email program “Time to email to the union. I've had enough of this silly lack of wage and too many hours”.

Suddenly, as the first few words rolled out of the fingers, came a loud beep from a pager. “REQUESTED TO PERFORM TRIAGE AT GREEN RECEPTION”,  i could barely see.

The doctor and the nurse ran off to the green reception. It was really bad, they could tell. It was just something in the air, and i could feel it, the heart-wrenching feeling of a tradegy,

It was bad. Several hours passed. Nobody came here, except the cleaner mopping the floor and singing _9 to 5_ by Dolly Parton to herself. 

Finally, the door swing open and yet another tired doctor came in. All i've seen for years is tired doctors. Ever since COVID-19 they've been exhausted. And the government isn't good either. They just pay them the bare minimum. I'm about 30 years old. Why can't i just die? 

“I'm sick and tired of it. Constant work, no breaks, no pay” the doctor complained to the dark, empty room. 

A little while afterwards, they were joined by another nurse who entered.

The nurse told the doctor the rumors about the massive motorway pileup, the new railway line, and more rambling on. 

After some thoughtful silence, the nurse pronounced “You know what, it's absolutely freezing outside. I hate it. I wish it was warmer”. The doctor replies “be careful what you wish for”.

And then it came, the faucet of superstition, the broken bones, the spilt blood, the corroded, burning metal dust of capitalism; the loud pager bleep - this time something about widespread burns to the face and body.

And again, a few more hours passed. The sun began to set and the night started beggining again.

After the sun set, some nurses came in, again exhausted like all the others before them.

“That steel refinery is a menace” one muttered. “it only deforms and destroys” they said quietly.

Another nurse also complained “I hate the motorway. It only kills, either by pollution or metal”.

And then the the third nurse, they said “The man with the broken face, he's in a coma now". "He's on the way to death”.

“We better take good care of him. We don't want to be liable. It can only hurt our wages”, said the green-suited doctor. “We just need to try our best, maybe it'll pay off”.

“Not everything is worth doing. Some things should just be left alone to fix themselves”, said the nurse with greying hair. They were at the end of the line, only a month away from retirement, left hopeless and empty, waiting for their escape from this dangerous machine.

And life went on like this, people coming and going, trying to catch a small break, until the evening.

My part of the hospital is one of the oldest parts and it is really beggining to show its age. Maybe it's the end. The cracks in the wallpaper, are they really that shallow? Or do they expand deep through the wall? I haven't seen that part. 

I hear a deep rumble from upstairs. They're moving a bed above that old tile again. I hear some debris drop onto the dropped ceiling. The old tile creaks in complaint as it is forced to handle new weight.

Another bed, another drop. Maybe it's related to the earlier bursts of activity. Suddenly, the dropped tile, unable to handle all the new weight fractures. It falls to the floor alongside a sizable pyramid of debris and a rather loud noise. A rock hits my metal head. It's not much and does not hurt.

A doctor, shocked by the noise, comes in. They open their phone.

“I'll need 3 contractors here; a ceiling tile has just fallen!” they say panicked.

A little while later, the site team arrive. They take a look at the fallen tile. They look at the hole in the ceiling. They look into the hole.

They have some worried conversation over the hole and the floor. Apparently, they can see the lights of upstairs through the rebar.

My entire part of the hospital is evacuated. It's too old to live anymore.

After a few days of blackness, and the noise fading away, the old part is evacuated. A man comes in and places some remotely-activatable bombs in the staffroom. I sit anxiously. I'm too old to be useful anymore. After a few hours, my time has come. The bombs will explode and i'll be crushed under a mountain of rubble, but first they're turning off the electricity.
`},{
            "name":'JSONLearn',
            "description": "A new way to teach and make resources (design only)",
            "tags": ["ideas", "coding"],
            "content":`
Introduction
------------

Historically, learning resources have been disparate and locked behind paywalls. I aim to change that. 

UI
--

The UI is mainly provided by HTML, CSS, and JS; alonside Bootstrap and Jquery.

### Main Screen

The main screen features two main halves; the left allowing browsing through files you have and those you can access online, and the right half allows  you to create a topic.

### Left halve

The top bit is an overview of files you have, which can be ordered in different ways.

The bottom bit allows 

### Activity area

You learn in terms of topics, have some notes to learn from, and then an activity area where you can:

*   Download the learning material in various formats:
    *   HTML
    *   PDF
    *   Markdown
    *   JSON
*   Download the activity in all forms mentioned above
*   Do an activity using a JSON database:
    *   Quiz
    *   Game

Data holding
------------

The directory structure can be compressed into a single file:

¿¿putabacktickhere¿¿putabacktickhere¿¿putabacktickheretext-plain
{
    "topics":{
        "fun-stuff":{
            "kittens":{
                "name":"Fluffy kittens"
                "markdown":¿¿putabacktickhere
                ~Kittens are |cute~file,-quiz=. I like kittens.
                ¿¿putabacktickhere,
                "questions":{
                    "title":"parent",
                    "types":[quiz] // We've just defined the type. Maybe we add questions, but we don't need to as they're already in the course material.
                }
            }
        },
        "not-fun-stuff":{
            "bubonic-plauge":{
                "name":"How rats spread bubonic plauge",
                "markdown":¿¿putabacktickhere
                ## Introduction
                BLA BLA BLA HORRIBLE STUFF HERE NOT RIPE FOR AN EXAMPLE.
                ¿¿putabacktickhere,
                "questions":{
                    "title":"parent",
                    "types":[quiz] // We've just defined the type. Maybe we add questions, but we don't need to as they're already in the course material.
                }
            }
        }
    }
}
¿¿putabacktickhere¿¿putabacktickhere¿¿putabacktickhere

This compressed format squashes the directory structure into a series of files.

JSONlearn will automatically convert between a directory structure and a JSON structure to provide the best experience.

### Quizzes

The learning material comes as a Markdown file, with [_MathJax_](https://tiddlywiki.com/#MathJax) or [_TeX_](https://tiddlywiki.com/#TeX) formula support. You can use this to teach.

The quizzes come as a JSON object, structured a bit like this:

¿¿putabacktickhere¿¿putabacktickhere¿¿putabacktickheretext-plain
"topic":{
    "questions":{
        "title":"Welsh digraphs"
        "*types":["ask", "quiz"]
        "questions":[
            ["Ll","Put your tounge on the roof of the mouth and blow", ["match", "typeQuestion"]]
            ["Dd", "Like th", ["match", "typeQuestion", "-quiz"]]
        ]
    }
}
¿¿putabacktickhere¿¿putabacktickhere¿¿putabacktickhere

The objects are the question banks. You have various items to consider. There are types, which determine how a question may be asked:

| **Type** | **Description** |
| --- | --- |
| ¿¿putabacktickherematch¿¿putabacktickhere | Asks the user to match the question and the answer in a pool of ¿¿putabacktickherematch¿¿putabacktickhere&#x60;-type questions |
| ¿¿putabacktickheretype¿¿putabacktickhere | Asks the user to type either the answer or question given the opposite half |
| ¿¿putabacktickheretypeAnswer¿¿putabacktickhere | Asks the user to type the answer, given the question |
| ¿¿putabacktickheretypeQuestion¿¿putabacktickhere | Asks the user to type the question, given the answer |
| ¿¿putabacktickherequiz¿¿putabacktickhere | Give x amount of answers available and ask the user to select the corret one |
| ¿¿putabacktickhere-¿¿putabacktickhere before any type | Do not use the ¿¿putabacktickhere*type¿¿putabacktickhere of the afterwards one |

Intresting. These can also be arrays, allowing for the &#x60;* or + at the end. The times means you have to match them all, the plus just two randomly picked. Add a decimal to the times to give the percentage (or numberto match), add a decimal to the plus to choose the bias of the random.

The ¿¿putabacktickhere*types¿¿putabacktickhere are types that apply to every question.

Now, markdown can also make its own questions using brackets of a special type. This takes in a sentene and takes the verb and splits afterwards:

> ~Carbon can bond with 4 hydrogens~

Might become:

> &#x60;["Carbon can bond with", "4 hydrogens"&#x60;]

or:

> &#x60;["Carbon can bond", "with 4 hydrogens"&#x60;]

This is determined using a fair random.

To make it easier for the compiler, in cases with multiple verbs or for specific placement, you might (read: must place a pipe character:

> ~Carbon can bond with |4 hydrogens~

will become:

> &#x60;["Carbon can bond with", "4 hydrogens"&#x60;]

See?

### Directory

To store the directory structure, the JSON file contains all we need:

¿¿putabacktickhere¿¿putabacktickhere¿¿putabacktickheretext-plain
{
    "topics":{
        "fun-stuff":{
            "kittens":{
                "name":"Fluffy kittens"
                "markdown"
            }
        },
        "not-fun-stuff":{
            "bubonic-plauge":{
                "name":"How rats spread bubonic plauge",
                "markdown"
            }
        }
    }
}
¿¿putabacktickhere¿¿putabacktickhere¿¿putabacktickhere

Note how all topics are stored in topics; this is the root directory. Each subobject is either a topic (if it contains any of ¿¿putabacktickherename¿¿putabacktickhere, ¿¿putabacktickheremarkdown¿¿putabacktickhere, or ¿¿putabacktickherequestions¿¿putabacktickhere), or a folder containing topics.

### Expanded directory structure.

In expanded structure, every key-value pair inside the topic is a seperate file; questions is a JSON array, markdown is a markdown file, and the name is contained in ¿¿putabacktickherename.txt¿¿putabacktickhere.

This allows easier editing by external programs.

MarkRound
---------

MarkRound Is the markup language. It features these additions:

*   Questions in-text
    *   ¿¿putabacktickhere~ ~¿¿putabacktickhere hold the question
    *   ¿¿putabacktickhere|¿¿putabacktickhere seperates the question and answer
    *   ¿¿putabacktickherex,y¿¿putabacktickhere are the question asking types
    *   ¿¿putabacktickhere=¿¿putabacktickhere finishes the question asking types 
    *   Example: ¿¿putabacktickhere~Leonardo deCaprio |played films~match,-quiz= and ~Leonardo daVinci |was an ancient polymath~match,-quiz=.¿¿putabacktickhere
        *   Produces:

¿¿putabacktickhere¿¿putabacktickhere¿¿putabacktickheretext-plain
[
	["Leonardo deCaprio", "played films", ["match", "-quiz"]],
	["Leonardo deVinci", "was an ancient polymath", ["match", "-quiz"]]
]
¿¿putabacktickhere¿¿putabacktickhere¿¿putabacktickhere

*   Tex and MathJax usage

Publication
-----------

This entire program is a FOSS program designed to make it easy to create and use learning guides. As such, it is published on my website. All code is client-side and there is no server, mainly due to limitations in hosting, but also to make it easier to share and use.

Every JSON file containing a subject is designed to be sharable on it's own, maybe using a pastebin or zip file. 

The source code is freely distributed and an electron version exists to install on the computer if in offline environments.

An online “marketplace” of sorts exists for these questions and resources. They are first checked to see if they compile before being sent onto the marketplace. It's a little like [https://lmms.io/lsp/?action=browse&category=Samples](https://lmms.io/lsp/?action=browse&category=Samples), where subjects can be downloaded with one click.

The marketplace is a seperate concept and entity and that is made clear via some subtle but noticable differences.

There is also a “propietary marketplace” but that is intentionally designed to “fail” whenever visiting and be clunky.

Differences from FlashQuizzr
----------------------------

FlashQuizzr is a simplistic Python program mainly concerned with quizzing based off simplistic JSON.

This is a sample of FlashQuizzr JSON:

¿¿putabacktickhere¿¿putabacktickhere¿¿putabacktickheretext-plain

{
    "electromagnetic":[
        ["Electromagnetic (EM) induction is ",[" generate electricity"]],
        ["EM induction is ",[" voltage is induced in a conductor or a coil when it moves through a magnetic field or when a magnetic field changes through it"]],
        ["In the motor ",[" is already a current in the conductor which experiences a force"]],
¿¿putabacktickhere¿¿putabacktickhere¿¿putabacktickhere

Note the way topics are quite simplistic; they're just a bank of questions with answers. There are no learning materials, no tags, no extra information, no places extra info can sneak in.

However, this JSON is designed to allow some form of new data to sneak in without breaking older technology.

Tech Stack
----------

*   HTML
*   CSS
*   JS
*   Jquery
*   Bootstrap
*   Vue (for various contents)

Courses
-------

Out of the box, JSONLearn provides a few example courses:

*   IGCSE
    *   Buisness
    *   CS
*   Languages
    *   English-Spanish
    *   English-Catalan
    *   Spanish-English
    *   Spanish-Catalan

Goal
----

What's the goal of all this novel stuff? To spread knowledge. By standardising learning material into a single JSON file easily shared, knowledge can be spread quickly. My website can make an offical opener of sorts and allow all clients to share knowledge.

Since the format is plaintext, it makes the format a lot more open.
`},{
            "name":'Simple Protocol',
            "description": "A web protocol designed to make the web faster and more efficent",
            "tags": ["ideas", "coding"],
            "content":`
Introduction
------------

What if instead of needing expensive servers, websites could scale at the same pace of their userbase? What if a website was not a billboard to be seen from miles away but a leaflet you could carry around? And what if data was not held by the server, but held by the clients?

My new protocol makes the internet more open and free by design.

Basic Topography
----------------

The protocol must have some form of topography for data to move around.

The protocol is formed of:

*   Navigation servers
*   Seeds
*   Clients

### Navigation Server

A navigation server contains information on the clients and the seeds, and ensures that every client “knows” where every other client is and what websites they contain. There can be multiple of them and they can even be seeds. However, there must be one large “phonebook” contained online for clients to know the navigation servers.

### Seeds

The seeds are the original hosters of information. Due to the design of the protocol, they do not need to be large-scale industrial servers; they can be some random sysadmin's laptop. 

These seeds should be online as much as possible, but given a large enough client mesh, they can afford some downtime.

### Clients

The clients are those that take from seeds and other connected clients to grab the information.

Software and Overlying Abstractions
-----------------------------------

The protocol will by abstracted away under layers of software to make it more palatable to the average user.

### Standardised Shared Content (SSC)

Standardised Shared Content (SSC) is formed of docker images containing the required software to run the website frontend. 

These are what is shared by seeders and have a mandatory size limit of 100 mb to encourage minimisation and speed of access.

These run in a sandboxed Linux environment (as is provided by docker), even on Windows. This gives us high portability and allows not just websites, but software.

The SSC is kept to one machine and has no networking capabilities; only communication with the database blueprint. The SSC can be thought of as a “RAM copy” of the website; any data changed on it is deleted unless it is inside the Database Blueprint

### Database Blueprint

The Database Blueprint can be thought of as boxes in boxes. One box in this analogy is connected to all the other boxes but will only accept boxes from the seeders; to send to other computers they must use the mail slot and ask the seeders or navigation server for permission. As long as the seeder copy allows it, the data may be sent.

The Database Blueprint allows the SSC to put items inside the boxes for semi-pernament storage. The teleportation box is the shared storage box. For example, a wiki may use the SSC to show wiki pages, but the shared boxes to hold the wiki pages and save and write from them, with account information stored in the non-SSC boxes.
`},{
            "name":'Chinese encoding system',
            "description": "A slightly more clever system for encoding Chinese characters",
            "tags": ["ideas"],
            "content":`
Currently, the leading encoding system for Chinese is Unicode. This system is not very elegant. Every single character gets a codepoint. There is little regard to the intecracies of Chinese writing. What if we changed that? What if we made an encoding system simpler and more systematic and clearer to understand?

Let's first have a crash-course on Chinese characters.

Chinese Characters are formed of components called **radicals**. There are about 250 of them and they can be put together to form **characters**.

Radicals will often be stretched and strokes within them changed to fit into a character. 

Radicals can be organised within a character in many different ways. 

The character formed by radicals might have its meaning hinted at by the meanings of radicals.

For example, take the character 休. It is formed of two radicals organised left to right; The radical for “person" (人) and the radical for “tree” (木). These two don't directly mean anything, but it kind of looks like the person is leaning on the tree, possibly taking a break. The character 休, therefore, mus mean “rest”.

So, let's devise the encoding system.

The first two numbers always encode the placement of radicals in the character:

|     |     |     |     |
| --- | --- | --- | --- |
| **Number** | **Type** | **Number** | Thing |
| 1   | Directional | 1   | Left to Right |
| 2   | Left to middle to Right |
| 3   | Top to Bottom |
| 4   | Top to middle to Bottom |
| 2   | Surround | 1   | Full |
| 2   | From N |
| 3   | From NW |
| 4   | From W |
| 5   | From SW |
| 6   | From S |
| 7   | From SE |
| 8   | From E |
| 9   | From NE |
| 3   | Overlay | 0   | Overlay |

  
And then afterwards are a series of three-digit codes for each radical in the character as defined by [radicals.csv](Chinese%20encoding%20system/radicals.csv).

After this, optionally, a 6-digit pronunciation guide. The first two digits encode the initial, the second the mid, the third the final, the fourth the final add-on, and the sixth the tone.

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Intial (spread out)** | **Mid** | **Final 1** | **Final 2** | **Tone** |
| ø   | a   | ø   | ø   | none |
| b   | o   | i   | n   | straight |
| f   | i1  | r   | ng  | up  |
| m   | i2  | a   | &#x60;-- | down |
| d   | i3  | e   |     | waving |
| t   | e   | o   |     |     |
| h   | u   | &#x60;-- |     |     |
| l   | ü   |     |     |     |
| g   | &#x60;-- |     |     |     |
| k   |     |     |     |     |
| s   |     |     |     |     |
| zh  |     |     |     |     |
| ch  |     |     |     |     |
| z   |     |     |     |     |
| o   |     |     |     |     |
| r   |     |     |     |     |
| x   |     |     |     |     |
| q   |     |     |     |     |
| w   |     |     |     |     |
| y   |     |     |     |     |
| sh  |     |     |     |     |

The beauty of this encoding system is that it allows any character to be constructed and lets new words with new characters be possible. Since it can also encode pronunciation, it provides a valuable nook for learners to jump from.

With minor changes, this system could also be used to encode other Chinese-based writing systems, such as Kanjii and Hanja.
`},{
            "name":'Biology virus simulator',
            "description": "A Processing simulator designed to emulate viruses. Java, which i was writing in, drove me insane so i had to quit.",
            "tags": ["ideas", "coding"],
            "content":`
Basic Types
-----------

These are the types of matter:

| **Type** | **Description** |
| --- | --- |
| Cell parts | Exist in every cell and are used to make things work properly |
| Non-biological | Any passive matter which does not co-ordinate "living" |
| Enzyme | Turns matter into other matter and slowly degrades |
| Hormone | Has a little "message" protein at the end of it; will reach a cell that can recieve it and transmit the protein. |
| Cell | A "living" components; recieves messages by enzyme and can reproduce and perform various functions. |
| Virus | A non-body component designed to reproduce by body |

And these are the components of matter 

| **Type** | **Part** | **Function** |
| --- | --- | --- |
| Cell parts | DNA | A key-value table of genes and their value |
| Splitters | Replicates a cell with a slight (1/50) chance of mutation |
| Antigens | The places where glucoses can "lock in" and enter the cell |
| Non-biological | Foods | Target for the enzymes |
| Glucoses | Provides energy for all cells |
| Dangerous Waste | Comes from used glucose and virus; harms the cell walls |
| Waste | A harmless version of waste; will pass safely |
| Enzyme | Food enzymes | Turns food into glucoses. |
| Waste disposers | Turns dangerous waste into brown waste |
| Virus-eaters | Turns a virus into dangerous waste. |
| Hormone | Stem turners | Floats around and turns stem cells into the needed cell |
| Enzyme commands | Will tell enzyme-makers what to make. |
| Reproducers | Tells a specified cell type to reproduce once |
| Cloners | Will initiate the reproduction mechanism for bodies |
| Cell | &#x60;[Skin-type&#x60;] Muscle | Provides movement to get foods and removes waste |
| &#x60;[Skin-type&#x60;]<br><br>Mitchocondria | Takes food being passed around the outside and turns it into glucose using “pocketed” enzymes. |
| Immuner | Will destroy a virus after a few seconds of prolonged contact and turn it into wastes |
| Enzyme-makers | Will create enzymes as others denature and are "absorbed" through antigens |
| Stem | Will move around and convert into cells as the body grows |
| Neuron | Co-ordinates hormone creation based off observations (will command for everything to be in the correct proportion as defined  by a list of percentages) |
| Virus | Virus | A simple piece of DNA encased by a cell wall with antigens; if antigens match, it attempts to enter and infect, reproducing with a slight chance of mutation. |

Looks
-----

The simulator is made using Java Processing.

The simulator uses fluid-shaped cells which change their shape based on turbulence in the water. 

The bodies (bags of cells) are suspended in the water. Every body has two layers:

*   “Functional” top layer used for the simulation
*   “Backdrop” back layer holding the cell together in a kind of circular shape

Each body is formed of a ring of skin-type cells which allow the cell to use water jets to move around. Inside the body is where most life processes occur.

Every other type of cell floats around inside the body bouncing around and attaching to hormones and glucose.

Here are some other looks:

*   Non-biologicial matter appears as small uniform circles of differing colours. 
*   Enzymes appear as black pill-shaped objects in the map with a “little” fill of colour based on functon.
*   Hormones appear as little fan shapes and have a “little” fill of colour based of message type, alongside coloured circular tips depending on message.
*   Cells appear as almost-circular blobs on the map, with many “arms" representing antigens
*   Viruses appear as tiny desert yellow circles with a singular “arm” representing antigens

Logic
-----

### Food and waste logic

Food sometimes appears in quick materialisations in the liquid and slowly floats around. These materialisations have about 100 foods inside and slowly bounce around. Bodies attempt to move to the foods.

Waste comes out of bodies and falls to the “floor” of the simulation, where it is slowly absorbed and taken away.

### Bodies

The simulator, as previously mentioned, uses “bags” of cells to define a body.

Each body's cells work hard to keep it alive. 

#### Basic life

The muscles provide locomotion for the body, allowing it to move around the environment and go towards the mean centre of food. 

As they go there, mitochondria cells will take the foods and give it to the “pocketed” enzymes to turn into glucoses, which can then float around the body and provide power to it. 

Each of the skin-type cells have two sides: the hardy outside side with few antigens and no acceptance to hormones, and the inside side with tons of antigens and acceptance to hormones.

These enzymes “denature” after a set amount of uses, meaning that enzyme-maker cells need to exist. These turn glucose into enzymes based off hormone commands which are added to a queues. If there is no hormone command in the queue, the enzyme-maker generates random enzymes. for each glucose particle.  

Hormones are the messages given by the body. There are two types: “request” and “response” hormones. “request” hormones go to a neuron cell and request the production of x hormone. The neuron cell can then choose to ignore or “respond” to the hormone based off “request” hormones.

This system allows for neurons to control the cell based off their model of the requests. Every other cell will request x hormone as their resources dwindle.

#### More advanced life

Stem cells also exist to help reproduce cells and make the body bigger as needed. These respond to response reproduction hormones. Once they recieve a response hormone, they take glucoses and split before transforming one of the cells to the required type. The type is then taken to where it is needed, either by expanding the wall or floating around

Hormones are formed of their “category”, which tells us what the message is about, and their “message”, which tells us what activity to perform. These can be though of as abstract representations of proteins.

Whenever a cell takes glucose, it will return a “dangerous waste”. These must then be turned into regular waste by enzymes and then they can be passed through the muscles and out into the environment.

Inside the cell, there is “DNA”, which, during a reproduction event, might mutate and change the antigen or increase energy efficency or myriad things. The DNA is the same for all cells.

#### Infections

Infections occur when a virus manages to bond to a muscle antigen and enter the muscle. Once inside, the virus can then eat the insides to reproduce. Viruses are extremely small, so tons of them can exist in a cell.

Once inside, the muscle is filled with hundreds of viruses before it explodes violently, launching viruses in every direction. Hopefully, parts of the body can recover due to differing antigens.

If a virus touches an immune cell, the immune cell will then:

*   Absorb the virus and analyse it for a few seconds
*   Release enzymes made to dissolve that virus 
*   Release hormones to change the antigen of all cells
*   Release antibodies which can clump virus cells together and make it more difficult for them to come into a cell

If this occurs quickly enough, then the body might be able to recover.

#### Reproduction

Bodies reproduce when they become too large to allow efficent glucose transfer. 

When a body reproduces, the cloner hormone comes out and turns the stem cells into mini factories. First, they form a large clump of stem cells. These then open up to form a cavity where the stem cells inside differentiate. Over the course of 5 minutes, the clump expands as hormones fly around and a slightly altered body comes out, following the exact porpotions of cells needed. Part of the oversized original body is dissolved into glucose to make this possible.

In the end of develeopment, eventually both of the seperate body walls will touch each other on opposing sides and then the clump is “ready”. The clump is surrounded by a membrane which allows it to pass the muscles in a sterile way before the clump leaves and the membrane turns into waste. Once the body leaves, it is independent of its parent. It has a slight antigen change as well, which makes it incompatible with glucose.

#### Death and Recovery

Every cell has a "life counter”. This counter counts down slowly to 0 every second without glucose or for every time a Dangerous Waste particle hits it. The lower the counter, the browner the cell. If glucose does not arrive quickly enough, the cell turns black and will eventually explode into a cloud of Dangerous Waste particles. If the cell is a muscle, it is sure about half of the waste will leave the cell. If the cell is internal, the whole body may be damaged. 

To recover a skin-type cell, the body will “suck together” two barrier cells to close off the cell. If this is not possible due to great distance, it is possible components of the cell will leak out. This means that each of the cells will die as they lose proximity to other cells. This results in widespread death of a body.

#### More antigen mutation logic

As a antigen mutates, it still retains the old form, but the old form may only recieve hormones and the rare glucose. The new form accepts anything.

### Body Spawning

Obviously, bodies do need to come into existence.

Bodies will spawn either on user input or on program start.

When a body spawns:

*   It will have the correct propotion of cells
*   It will have 5 glucose molecules per cell within the body
*   It will begin executing its functions immediately

User Input
----------

The user is able to do various things. They can:

*   Place a random virus cluster on the map with a random antigen selected from one random muscle cell.
*   Drag bodies around the map
*   Place food on the map
*   Zoom in and out or pan around the map
*   Spawn a body to a maximumm of three on the map 

Programming specifics
---------------------

### Standards

Each of the types and their individual objects will have their own new class made.

The PDE file must only be used to process input and output; all logic and such must be part of a seperate file.

The git repo must use gitmoji and the commits must be specific.

The first version uploaded must be able to:

*   Render bodies and all their cells properly
*   Spawn food
*   Allow bodies to move around

### Co-ordinate system

The co-ordinate system is quite simple. Body co-ordinates are defined as the center of their circle relative to the pond.

Cell co-ordinates are relative to their position relative to the cell system.

Alongside this, cells and bodies also have a momentum. This is defined as a vector of movement per tick. 

Momentum is lost according to the friction co-efficent of the water. 

### Collision handling

Oh no! This is actually quite difficult. 

Erm… each cell type has a radius as defined by its max glucose value divided by 100. This can be used for checking if distance is ok for objects.

If a collision occurs, there is a standard density to figure out how much each object moves as a result. 

Ticks
-----

20 ticks a second. Ticks every 50 ms. 

Each tick:

*   The co-ordinates change by momentum, which is the vector to move
*   Momentum is lost reverse logarithmically due to water friction for all cells.
*   Momentum, if at 0 for a long period, increases a little to represent turbulence
*   If anything is “touching” a cell, check if they're ok to enter… somehow
*   Change values as neccesary (glucose down by 1,
`},{
            "name":'Vladimir',
            "description": "A dramatic story from a mostly-innocent Reedsy prompt",
            "tags": ["ideas", "stories"],
            "content":`
**Trigger Warnings** Suicide, self-harm, exam stress, and alcohol abuse; reader discretion advised

I'm on that path again. The cold air and wet pavement pass below me as i cross the surburbia of Leeds, excited to go back home. The overcast sky make a dim and depressing place to be, but it's ok. The copy-paste houses line the street and the car fumes make my choke, but it's ok. The light rain begins and makes a sound against my coat, but it's ok. Home is soon. I like home. It's a warm retreat from reality. Surreal paintings and the strong smell of cinnamon lines every room.  There is fast wifi and plenty of opportunities to escape from the monotony of contemporary life.

Exams are coming. It's not very nice at all. I hate exams. They're always just sitting there waiting to ruin your life over using the wrong term or maybe just wording things incorrectly.

I cross the road, looking both ways for that single occasional car. None are there, so i pass. And then i see something in front of me. It's a notebook, a rather boring one at that. I look at it and pick it up. The rain has destroyed the bloody thing and smeared some ink, but it's perfectly legible. I stow it in my bag. It belongs to someone. Maybe i go find out who they are.

I continue walking, crossing endless roads and walking on some of the most boring places i've ever been. I've heard that in America it's worse. But we're the worst in Western Europe.

I enter our house, sliding the brass key into the well-oiled mechanism. With the crack of the door, the strong smell of cinnamon hits me.

I go upstairs. The walls are blue, the window wide, and the bed warm. On it, like always, sits my computer, the portal to another whole world.

I open the bloody notebook and begin reading. The first page barely contains any writer details, just blank nothingness. Maybe it's been reserved.

I open onto the next page. 2023-01-06, just after New Year. It's only 2023-02-10 today. Maybe this will be a short read.

_"First, this is a private notebook. I don't want no prying eyes. Get them all away from me. The only reason i'm writing this shitty thing with my bad writing is just to introduce something interesting in my life._

_It's Friday. After this dumb weekend i'll go back to school and do some boring crap again. Everything is so boring. I'm sick and tired of it._

_And then i have these shitty exams to do. They're not even real. They're boring mock exams, yet another vehicle of suffering. Damn the exams to eternal suffering. They'll be the death of me."_

2023-01-10.

_"School again. Fuck this place. The people suck, the culture sucks, and the subject are all so boring. The only interesting thing we ever do is the least boring thing; Lunch._ 

_What horrible goop. Grey vegetables, physically impossibly bland desserts, and the most penetrating smell of detergent ever._

_I hate this._

_Going home isn't much fun either. We got some stupid dog that's holding us hostage and my family isn't much help. We'll either waste time making it exhausted or let it bark so much i won't get any revision done. We shouldn't have got a dog. My brother loves to blame others for what they don't do. And my farther still prefers him over me._ 

_I won't be able to do a-levels. I won't be able to proceed in life, not that i want to anymore. Life is boring. The only way i cope nowadays is with that lovely stinky bottle downstairs. A few gulps and all my problems melt away. I know it's bad for me. I know it's no good. I know it's illegal, but i'll die if i can't have that."_

2023-01-16

_"FUCK THIS FUCK THIS FUCK THIS i can't anymore. I had to do an English Literature exam and everyone kept gossiping and speaking and the teacher shouted at me for being too slow and called me a failure but i just can't anymore why is everyone so horrible i wish i could just not do these exams but then i won't get a-levels and i mean like it's more suffering but i'll but i'll i actually don't know wait let me get something to help_

_The spirits aren't helping anymore. I've drunk half a bottle and i even spilt a bit here, see the arrow? All i have now is nausea, arguments, and a horrific headache. I wanted to vomit. I still do, yet i'm still angry. Fuck all this crap._ 

_I don't want to do anything more. This crap hurts, but i have to keep pushing on, even if it'll kill me._

_Wait, maybe i just do that. Let the stress kill me and be a martyr against exams. I'll just keep pushing."_

Bad idea there. You'll just cause more suffering for the school and people around you. At the exam board, they'll keep crushing souls. It's a sad machine. A very sad one at that.

2023-01-20

_"It's the last day before i can finally escape this dumb school. People keep throwing calculators and breaking each other's phones. I fucking hate these people._ 

_And i have an exam on Monday. Some dumb English paper. I hate writing. I hate writing this. I hate doing this shitty “mental health” crap that don't even work! I've wasted enough time on this! The школь's like trying some новий “mindfulness” shit but i don't get any of it!! Fuck this school and fuck this life. Maybe i'll reincarnate as some beautiful butterfly, but the way things are going, i'm probably going to become some rat or slimy snake._ 

_Why does the school force you to go choose what you want to do when you're so young? Nobody know what they want to do! I'm sick and усталь of this crap! take it away я don't like any of this shit take them all away i'm so sick and tired it's becoming exhausting please save me get rid of them i need to get something_

_What a relief. Dad's going to notice soon and beat me up but like i just can't care for it anymore i'm just too усталь to let this shit happening anymore it's just some crap dammit"_

I love English. I love working with the language, stringing characters together in such an intricate way. But it seems here… Russian was used? I don't get it maybe it's their native language. Some people just don't have such a strong grip on English, unfortunately. Sometimes i forget other nationalities even exist.

I've got to go do the dumb dishwasher before other chores. So boring and time-consuming.

I decide to read another entry before going to bed.

2023-01-25

_“I am going to explode. So many… so many… so many exams, so much stress, so much barking, so much loud shouting, so much blame-shifting, so many shit._ 

_Maybe i write what happened. I don't trust this dumb “mindfulness” crap. I thought we wanted to reduce overpopulation?_

_It all started with that dumb English literature paper. The teacher told me off for getting that dumb Tempest thing and An Inspector Calls mixed up. I don't care about the words of some random dead people._

_“An Inspector Calls”? Whatever. Telling the story by recounting experiences of other people is boring. It's just a cash-grab for our attention. I don't care for it._

_And then the dog kept on barking and even though it was the day that my brother was supposed to take care of it he just said that he had some other shit to do and obviously i wouldn't take care of it as it wasn't my day but then dad shouted at me for not doing the dog. Ugh, i'm sick and tired of it._

_See this stain? That's my blood. Obviously, bloodletting hasn't worked. I'm still as sick and tired as i always was. FUCK THIS.”_

No, recounting someone else's story through another story is a perfectly fine way to do things. If i didn't do it, you'd end up misrepresented.

I'm so tired too. So many exams, so many teachers. But i want to grow up and be the next Charles Dickens. I'll just live through this shit and do a-levels and finally finish with this stupid forced education.

I'm tired.. Should i sleep and give this person another day of suffering?  They're probably more tired. But i'm also tired. I have been for the whole week. 

I decide to read another entry.

2023-01-30

_"I'm getting sick and tired. Mock week has passed, but that doesn't mean that i'm getting any better. The dog is still growing, growing more restless. I'm getting more tired. The students are still as shitty as they are and their endless screaming is getting unbearable._ 

_Maybe i just go drink a few liters of alcohol. Or spill that much blood. Or eat the strong disinfectant, burn myself from the inside out._ 

_I'm getting sick and tired of all this crap i have to deal with. I'm too young to make these life-changing decisions on what i want to do. I can't revise, i can't do my exams, and i don't think my life is going anywhere._

_Maybe i restart. Maybe i end myself and see where i go."_

I'm shook. I'm captivated. I know you can't see this, but the handwriting is getting worse. It's shakier and more erratic. I can almost see the tears and sadness in the writing.

Fuck the GCSEs, and fuck stress.

I can't stop reading. It's getting too dangerous not to read it.

And then it hits me, the day after the last, that horrible on. 2023-02-08:

_“Help me. I can't cope. I'm going to toss away this dumb ”mindfullness" notebook and i hope whoever finds it can save me. It's not helping. I'm going to drink the strong disinfectant. 11/02/23,  1:00 AM is your deadline._  
_If nobody saves me, i'll finally rid myself of this dumb world. If someone sees me, remember Vladimir Petrova, 6 cherry lane. Bye."_

I turn the pages in desperation. Nothing but blank pages, nothing. I don't know how anybody didn't find it. I need to do something. It's worryingly close to the time. It's 2023-02-10, 23:54. They're going away soon. I have to do something! I have to do something! I have to do something!

Shit shit shit shit shit shit shit this is bad VERY BAD this is bad VERY BAD this is bad VERY BAD this is bad VERY BAD

My mind snaps in half. This is bad. VERY BAD.

I notice the wet tears on my cheeks, the swimming thoughts in my head, the grief. I've got to do something. It's time. Someone is going to die and it's going to be all my fault. This is bad. VERY BAD.

I take my phone. It's at 0%. Crap. I shove in the charger with vigor, a black lifeline. I desperately squeeze the power button. It won't turn on. Turn on you piece of crap turn on i need you turn on please someone's life depends on this turn on. You need to turn on turn on now you absolute piece of shit turn on you need to wake up please someone's life depends on this turn on please or someone will die please turn on this is bad VERY BAD

This is bad. VERY BAD.

I run through my options. Maybe i go call [116 123](tel:116123) for Samaritans. But they can't really save this poor guy. This is a very real emergency. And for that, i'll call the emergency line 999 but maybe my call won't matter but if i don't call them someone will die this is bad VERY BAD

Fuck! I run downstairs and grab the landline. As i rush upstairs, i jab the number “999" into it and press dial. The dial tone is continuing. It's brutal, that same montonous hum, the barrier between death and life. I'm waiting, i'm waiting, i'm waiting. After a brutal 15 seconds, someone picks up.

999: “Emergency line, how may i assist you?”

I freeze. This isn't the time. 

“I.. i… i….”

I keep on stammering and stuttering this is bad. VERY BAD.

999: “Hello? ”

“Someone… someone… they're considering… someone.. su… su… icide” come on stupid mouth make words now this is bad VERY BAD

999: “Ok, what do you know?”

“I was reading th.. th.. their notebook and it said that they were suffering fr.. from exam stress and they're going to com.. commit it at 01:00 tomorrow and it's late and it's an emergency and i don't know what to do” i stumbled and stammered.

999: “Do you have any details?” the operator said, concerned.

“They said their name was… was…” i struggle to remember. This is bad VERY BAD

I take a look at the notebook. “Vladimir Petrova, from… from… 6 cherry lane, probably… probably in Leeds” i stammer and stummer.

999: "Thank you for providing the information. Could you stay on the line with me while we send help to the address? We have a team ready to respond."

And then, relieved, i collapse from the trauma. Why am i such a wuss?

And i wait. And i wait. I glance at my alarm clock. 00:02. We only have an hour left. I begin to sob. Will they arrive in time.

I'm shaking. The room is shrinking. I'm covered in sweat. 

999: “They've been dispatched. ETA 00:30”

Perfect… maybe there'll be enough time.

And i sit and stare anxiously at the clock. 00:05. The time is passing slowly. The time is passing quickly. Do we have enough time? I can hear other calls in the background. There are drinking emergencies and domestic violence emergencies. Did i prevent people from getting help? Shit, is my call really that important? There's just one person when people in a terrorist attack may not be getting assistance.

Maybe i don't matter. Maybe my call is irrelevant. Maybe i'm a time-waster. This is bad. VERY BAD.

Suddenly, a voice startles me.

999: “They're almost there, hold on”

I was lost in a trance. It's 00:29

I wait in anxiety for a few seconds. It's truly exhausting. 

999: “They've just intercepted the house.” The dispatcher pauses for a second. They're breathless. They have a choked voice. They're sniffling. They're sighing. “They've found an empty bottle of floor cleaner next to his bed”. Before i can hear their drowned sobs fully develop, they mute the line.

The tears that were slowly leaving my eyes begin streaming now. I was too late. It's 00:35. 

I was too late. I was a failure. I didn't save them in time.

I hold my breath as i begin sobbing. Maybe the pain of carbon dioxide will sober me.

They drank the floor cleaner. They destroyed themselves. They burnt themselves from the inside out. They died in the worst pain possible. It was all my fault. If only i had called earlier, told them earlier, maybe they'd be alive to this day And it's all my fault. I killed Vladimir. I killed the poor guy. 

He's dead now what do i do i don't know maybe i run off to the bathroom and cry my heart out i'm not going to drink toilet cleaner because that's bad and i don't want to be another victim but it feels tempting i'm probably going to have guilt for the rest of my life what do i do oh no i don't know i'm scared someone help me please 

999: “Wait, they've just discovered him in the bathroom” the dispatcher says through sobs, disrupting my panic.

999: “He's alive!”

And then i managed to finally snatch a breath. 

999: “He's asking to talk to you”

I hear a quick turn-off of audio and then a teenage voice

Voice: “You… you… saw me?”

“Yeah, your life… meant something to me”

Voice: “Ok… what do i do now what do i do now what do i do now”

I hear a distorted tinny voice, likely from the dispatched team. “We'll send you off to CAHMS for some support” they said.

I hear the dispatcher again:

999: “Can you, by any chances, send us the notebook?”

“How?” i asked.

999: “We'll collect it from you. What's your address”

I told them my address.

999: “They'll go down to Leeds General Infirmary. You can meet him there."

I asked them if there's anything that i need to do.

999: “No, you can sleep now. They're safe.”

I hang up the phone. I put it away and sit back. I take a breath. The adrenaline drains away and the tiredness takes me away.

Tomorrow, i'll go meet this poor guy. Maybe even make friends. Tomorrow.

Tomorrow… to-mor-row… it's a thing that i'm glad someone else will experience that.
`},{
            "name":'Lyric notation system',
            "description": "A new way to write lyrics",
            "tags": ["ideas", "music"],
            "content":`
Background
----------

Historically, the current lyrical notation has been sufficent for most songs in existence; most songs were either live or came from live recordings and had a main singer and some other singers in the background sometimes.

However, with the rise of electronic music, new methods of modifying lyrics have been released and are unaccounted for in the current scheme, despite their low ability to accurately transcribe how the lyrics are made.

There are some goals of this system:

*   Reflect vocals more accurately
*   Provide instruments when needed
*   Reflect pitch changes and give a more accurate way of writing.

Features
--------

| Indicator | Meaning | Example |
| --- | --- | --- |
| ^   | Indication of song data with these accepted values:<br><br>*   ¿¿putabacktickheretitle¿¿putabacktickhere<br>*   ¿¿putabacktickhereartist¿¿putabacktickhere<br>*   ¿¿putabacktickhereyear¿¿putabacktickhere<br>*   ¿¿putabacktickherealbum¿¿putabacktickhere<br>*   ¿¿putabacktickherelength¿¿putabacktickhere<br>*   ¿¿putabacktickhereproducer¿¿putabacktickhere<br>*   ¿¿putabacktickheredata¿¿putabacktickhere (any of the below; applied to whole song and all other modifiers are relative to the data) | ^title: song ^artist: her ^year:2023 |
| /   | Indication of “higher” or “up” |     |
| &#x60;&#x60;  | Indication of “lower” or “down” | Less-important verse: &#x60;[verse 1 &#x60;&#x60;&#x60;] |
| &#x60;-  | Indication of “normal” | Normal voice: &#x60;[&#x60;[-&#x60;]&#x60;] |
| &#x60;*  | A multiplier to indicate quantity of something | Two repeats: &#x60;[chorus&#x60;*2&#x60;] |
| +   | A multiplier to indicate mass of something | Louder: &#x60;[chorus+1&#x60;] |
| (seperated by spaces from encasing) &#x60;[ &#x60;] or / / | Indicates an IPA-transcription | Non-English sound in an English song: ( /βɔ/ ) |
| ( ) to (((( )))) | Indication of how “ghosted” lyrics become, more brakcets meaning more ghosting | Backing: (ooh /)  <br>Barely audible whisper: (((( &#x60;[&#x60;[&#x60;[whispered&#x60;]&#x60;]&#x60;] you won't be able to )))) |
| &#x60;[ &#x60;] | Indicates a part | &#x60;[Chorus&#x60;] |
| &#x60;[&#x60;[ &#x60;]&#x60;] | Indicates a new singer or pitch (with the vertical marks, indicates pitched vocals) | Vococoding with two seperate lower pitches: &#x60;[&#x60;[/ - &#x60;&#x60;&#x60;*2&#x60;]&#x60;]  <br>Vocoding with doubly-loud lower pitch: &#x60;[&#x60;[/ - &#x60;&#x60;+2&#x60;]&#x60;]  <br>Singer: &#x60;[&#x60;[ John Smith&#x60;]&#x60;] |
| &#x60;[&#x60;[&#x60;[ &#x60;]&#x60;]&#x60;] | Indicates form of voicing, notable instrumental feature, or reasoning for feature (for interpretation) | Voicing: &#x60;[&#x60;[&#x60;[breathless&#x60;]&#x60;]&#x60;]  <br>Instrument: &#x60;[&#x60;[&#x60;[mocking synth&#x60;]&#x60;]&#x60;] |
| { } | Indicates non-transcribable features. | Sound effects, pitched down: {heavy breathing &#x60;&#x60;} |
| < > | A change in the music | Key change (up): <c# maj />  <br>BPM change: <125 bpm &#x60;&#x60; >  <br>All change: <c min &#x60;&#x60; 90 bpm /> |
| > < | (appended) Indicates scope of a change, if change is not indicated by a marking | &#x60;[&#x60;[&#x60;[screaming&#x60;]&#x60;]&#x60;] > Until verse 3 repeats < |
| xx: or xxx: | Indicates a change of language given an ISO language code | Change to Spanish: es:  <br>Change to Toki Pona: tok: |
| #   | Comment (at end of line) | Meaning: And here i stand # This line mainly refers to standing alone<br><br>Musical element: You're the worst! # The music swells and “breaks” apart |

Extension Scheme
----------------

Since the amount of voice effects are increasing, diversions from the above abilities are permitted, as long as they follow these rules:

*   Any markers must be fully usable using only the ASCII charset. Where characters are not available using ASCII or have been substituted, the substitutions must be documented. (just to be clear, this does not include content, so, for example, the IPA transcriptions inside / / or &#x60;[ &#x60;] are not an exception)
*   If the markers already exist, use those instead
*   A definition must be placed above the lyrics (example: “% % - Indicates half-sung lyrics”)
*   Any markers made must not make standard markers more poorly defined

Example lyrics
--------------

### Brokeup, Arca

This song uses many voice-pitch effects, which allows it to be a powerful demonstration of the system's pitch notation.

¿¿putabacktickhere¿¿putabacktickhere¿¿putabacktickheretext-plain
((Uh, uh))

[[-]]
swear, man, get off
Waitin' for my flight to take off
We're gonna faint and break up
Bloodless and gettin' paper
Uhuh, 

[[/]]
I'll push and pull it out, breed it, more
I'll push and pull it out
It's too much for me to take
It's too much for me to take
I'll push and pull it out, wait
It's too much for me to take
It's too much for me to take, ok?

[[-]]
Workin' thick on knees to push it on heels
I hear, throat sore, resting on your arms
Smeared makeup, waiting for breath take off
Feeling faded, break out, blood rush, paper
I'm a little bit twisted
Reel it back then feel it, seal it
Arms up
I'm a little bit twisted
Reel it back then feel it, seal it
Hold up
Hey, okay, I'll wait
Uh, wait
I don't feel my body leaning
Worst comes to worst
I'm feeling like a natural heathen
Superstition, blood, I'm paused
Wait for you

[[/]]
I'll push it
It's too much for me to take
It's too &#x60; much for me to take
So, where you goin'?

[[/]]
Push it on knees, push it on heels
I hear, throat sore, resting on your arms
Swear, man, get off
Waitin' for my flight to take off
We're gonna faint and break up
Bloodless and gettin' paper
I'll push and pull it out, breed it
It's too much for me to take
It's too much for me to take
It's too much
¿¿putabacktickhere¿¿putabacktickhere¿¿putabacktickhere

Oh My God, Sevdaliza
--------------------

¿¿putabacktickhere¿¿putabacktickhere¿¿putabacktickheretext-plain
[[/+2]]
Oh my God
Who should I be? (([[[chopped-off]]] ee-aah))
What is it you want when you come for me?
Every time, you're another evil
Waiting for an angel that you bring to hell

[[/]]
Oh my God
Who should I be?
What is it you want when you come for me?
Every time, you're another evil
Waiting for an angel that you bring to hell
Oh my God
Who should I be?
What is it you want when you come for me?
Every time, you're another evil
Waiting for an angel that you bring to hell

I've been through a lot in life
I live up
Distant from it all
I view myself from above
Roamin' in the fields of hope
Will it make or break me?
As my dreams are heavy, they outweigh me (([[[echoey]]] ko, ru: Ichima, спасибо))

Oh my God
Who should I be?
What is it you want when you come for me?
Every time, you're another evil
Waiting for an angel that you bring to hell
Oh my God
Who should I be?
What is it you want when you come for me?
Every time, you're another evil
Waiting for an angel that you bring to hell

True that you're not alone
True that you're not alone
(Will it make or break me?)
True that you're not alone
True that you're not alone

O-o-oh my God
Who should I be?
What is it you want when you come for me?
Every time, you're another evil
Waiting for an angel that you bring to hell
Oh my God
Who should I be?
What is it you want when you come for me?
Every time, you're another evil
Waiting for an angel that you bring to hell

[[[repeated, echoey, choral]] en:
True that you're not alone

[[[echoey]]] ko, ru:
Ichima, спасибо

[[[repeated, echoey, choral]] en:
True that you're not alone

Oh my God
Who should I be?
What is it you want when you come for me?
Every time, you're another evil
Waiting for an angel that you bring to hell

(True that you're not alone)

[[/]] [[[frequently cut-off]]]
Oh my God
Who should I be?
What is it you want when you come for me?
Every time, you're another evil
Waiting for an angel that you bring to hell
¿¿putabacktickhere¿¿putabacktickhere¿¿putabacktickhere
`},{
            "name":'LMMS Fork',
            "description": "A new design for LMMS",
            "tags": ["ideas", "music", "coding"],
            "content":`
Introduction
------------

LMMS has historically been quite poor, a small abandoned piece of software with barely any work done to it. It no longer has direction, a push, or dominance in the market. IT SUCKS!!!!

This is why i plan to make it work. My main goal is to get better then all the other DAWs, create a new shared format, and make LMMS more user firendly.

Mission Statement
-----------------

I, 56independent, strongly belive in the power of FOSS software. I love music, especially electronic music by the likes of Iglooghost, SOPHIE, Sevdaliza, Arca, and various others. However, the FOSS space has not had a DAW able to compete with propietary software.

I believe that if i were to make the best so far, LMMS, and make it better, that LMMS might be on a fast track to being the best DAW, bringing in developers moe professional then me.

Main direction
--------------

My project is made to:

*   Supercharge LMMS with features
*   Make LMMS beautiful
*   Make a converter from other DAWs into LMMS
*   Make a “shared format” using plaintext.

Inspiration
-----------

Inspiration can be taken from Tanatacrul's conversion of MuseScore 2 into 3 and then 4.

This proejct could take from this idea, and move LMMS from this “LMMS 1” phase and make it a LMMS 2 to build a platform for others to convert it into LMMS 3.

Time
----

A lot of time will have to be spent on this. This is why i have determined to spend my study leave making 

Features
--------

Here is a simple feature comparison

| **Feature** | **FL Studio** | **LMMS** |
| --- | --- | --- |
| Plugin support | Serum, Omnisphere, Kontakt | Limited plugin compatibility |
| Advanced automation | Complex automation patterns and curves | Basic automation system |
| Mixing and mastering tools | Advanced EQ and compression options | Basic mixing and mastering tools |
| MIDI support | Advanced MIDI editing tools, external MIDI controller support | Limited MIDI tools |
| Piano roll | Ability to add multiple notes at once, extensive editing options | Basic piano roll |
| Sample library | Wide range of sounds and instruments | Limited sample library |
| Audio recording | Multi-track recording, direct recording from microphone or external instrument | Basic audio recording system |
`},{
            "name":'ShareArea',
            "description": "A way to share projects with that Minetest block modding system",
            "tags": ["ideas", "coding"],
            "content":`
Introduction
------------

ShareArea is the place for mods created with this tool to be shared. It bears a strong resemblence to Scratch's area for sharing, but takes most project management features from Git. 

Browsing Area
-------------

The browsing area, by default, is the place for finding project. It features several “banks” of projects, which are little collections based off metrics, bearing a similarity to ContentDB's and Scratch's system.

This place is optimised for engagement and discovery, ensuring an even spread across quality projects.

Project Page
------------

There is a project page for each project.

Each project page acts differently for the type of project:

| Type | Acting | Possibilities |
| --- | --- | --- |
| Mod | Shows the contents of a mod and their activities | Allows editing |
| Modpack | Shows the component mods and allows individual editing, as well as viewing “ghost things” from other mods |
| Game | Shows the component mods and modpacks, allowing editing of all things but also focus onto individual mods and modpacks |

Alongside this, there are tools available for project managment, not too disimilar from a real git hosting site.

**Non-owner views**

Comments are issues; people can choose if their comment is a “comment” or “request” or “issue”. Contributors can change them.

Each project allows viewing the mod, and any change automatically “forks” the project onto the user's userspace. This allows them to work indepenedently.

Each fork can then be “pull-together” requested back into the original, which allows new changes to come.

**Owner views**

The owner themselves sees some new functionalities. They have several levels of "done” they can add to a comment, and can also merge forks onto the project (but only after a pull-together request has been made).

Whenever they upload to a project, they make a new "commit" to the project, alongside a message of what has changed and further optional description.

They can also organise their project into “versions”. This is an almost-identical copy of Git's tagging system.

Similarities
------------

As we have seen above, there are similarities to common platforms:

*   Scratch
    *   A place to “remix” and “see inside” projects
    *   Browsing similarities
    *   Overall user interface similarities
*   ContentDB
    *   Browsing
    *   Comments
    *   “Project approval” and “comment approval” systems
*   Git repos
    *   Project management powers
    *   Backwards-compatible with standard git commands (but in an opauqe way)

ContentDB integration
---------------------

For higher quality mods made with this project, ContentDB may be integrated. This means that every commit and version, the ContentDB will updated accordingly.

The backend takes care of Lua conversion and publishment with a “proxy git repository” for the Lua code only.

When a developer wishes to integrate from the ShareArea, they must first submit an “official request” to the admins, who will review the mod before it is sent over.

This approval procedure ensures that no “crappy" mods enter ContentDB. This important step prevents ContentDB from having to enforce a ban on ShareArea mods.

Anything from ContentDB will be given to the individual project owners, making sure they don't have to leave the ShareArea.

Should a project owner wish to leave ShareArea in lieu of ContentDB, this should be fully allowed. The ShareArea MUST make this transition as easy as possible.

This transition is done by the following procedures:

*   Allowance to clone the git repositories needed
*   Control over the ContentDB repository
*   Removal from ShareArea alongside a JSON export of what was there

Hosting
-------

ShareArea follows the same hositng approach as ContentDB; be centralised and hosted on a single server, but also allow others to host their own versions.

The entire thing can come as a docker container hosted on the docker marketplace. This makes it almost trivial to install and run and allows private hosting.

The benifits of self-hosting such a niche product are not know, but might be good for environment where a large group of people need management of their own mods whilst also needing the security of a closed system.

Project roadmap
---------------

This project should ONLY be pursued AFTER the main creator is mostly prepared for client usage. If this is not done, then this whole project might be a waste of time.
`},{
            "name":'Irrational encoding',
            "description": "A very silly way to encode info",
            "tags": ["ideas", "coding"],
            "content":`
By using an irrational number, one can store data. For this proof-of-concept, we will be using ASCII codes.

Encoder

1.  Convert input string to ASCII
2.  Take each ASCII code and look for it inside the irrational number
3.  If the index is longer then the code, put two codes together and look again
4.  Once found, write down the index. 
5.  Write down the length of the sequence at the end seperated by a dash
6.  Write a comma
7.  Give out the sequence in terminal

Decoder

1.  Take in the sequence
2.  Parse it into a list where every even element is a index and every odd element is a length
3.  For each item in the list, calculate to the index plus length times two
4.  Take the numbers from them and read them down into ASCII
5.  Print out the ASCII 

This compression system helps ensure all your words still exist.
`},{
            "name":'FOSS Funding Company (FFC)',
            "description": "A company designed to share donations",
            "tags": ["ideas"],
            "content":`
Introduction
------------

Historically, the FOSS community and software body has been underfunded and mainly relied on passion and love. This has resulted in an inoptimal development speed and the dominance of propietary software.

Money is the universal unit of appreciation and quality of services, and it is also the gateway to better software and dominance over the Propietary software.

As such, i propose this company to distribute funds.

Goal
----

The goal of this company is to provide a single body for EVERYONE to be able to donate to. This means that FOSS donations are simplified. The company also gives FOSS developers a fair payment.

Repository guidelines
---------------------

Repositories, to be eligable to this program must:

*   Already have a team of at least 5 different contributors having sent pull requests in the past month
*   Have a ten-point “quality-level” tag system judging the quality of pull requests and issues based on:
    *   How innovative they are
    *   How stable they are
    *   How important they are
*   Send a “opt-in” letter to the program, possibly on request

Once done, they can then opt-in and be approved by community vote with admin veto if neccary.

Opting in will give several benifits:

*   A weekly income based off contributor amounts (allowing much easier progress)

Basic workflow
--------------

A contributor will use the website's repository finder to find a repository they enjoy, based off a tag system which lets them sort based on:

*   Main languages used
*   Type
*   Main technologies used
*   Average contribution score from the past 7 days (which means that developers can target poorly-moving repositories for more money)

From there, they can then go to the repository and either add issues or pull requests based off issues. Both are ranked by the development team using the tag system. However, they are only calculated and counted as part of the payout if they're merged.

### Introduction to intrested parties

**Donors** are those who give money to the charity. This then gives them benifits as well as gives the FOSS world the money it desperately needs

**Repositories** Are the FOSS software which opt-in to recieve money from the charity. They are encouraged to gain developers and a  higher average feature score if they want more money. This money can be spent as the developers wish; they may use it for servers or themselves

**Contributors** Are those that contribute to a repository. They recieve most of the money for their changes

Cash Flow
---------

### Basics

The company is a non-profit charity. It takes money from donators and distributes it among contributors and repositories based on the value of a pull request to 

### Financing

The corporation is funded by donators and sponsors, who give money to FOSS. The incentive is that funding it will have a result in your life. Any person who uses FOSS software but has no development skill may pay the company; this results in effects directly available to them.

Since this is a single body which you donate to, the logistics of donations are much easier for the payer.

Here are reasons people may wish to donate:

*   Make FOSS software competitive against propietary, which benifits everyone as other individuals do not want to pay for propietary software when FOSS can do the job  million times beet
*   A very easy way to give back; no time needs to be spent, just a little of your wage (which i guess is an abstracted form of time)

Paying also gives some “convinience features”, starting at €1 per week:

*   A single place to sync software and versions across devices
*   A place to find novel and intresting software
*   Access to the monthly newsletter, which adds to the newsletter about the changes and the finances, giving payers a better idea of what's happening

And at €5 per week:

*   A little badge next to their name on the website should they be in the top 50
*   Stickers and other memorabilia
*   A single place to get all alpha builds, built hourly (bleeding edge!)
*   Voting access for various policies
*   Choice of fund direction

The fund direction is very helpful; they can channel 50% of their donations to a specific repository.

### Contributors

Contributors get, by default on their first pull request, and until their latest pull request is a week old:

*   Stickers and other memorabilia
*   Voting access for various policies

To figure out the money given to a contributor per week, we use the dummy value of €1.

From here, we then use a series of mutations for each contribution:

1.  Per contribution
    1.  The amount has 10% removed if it's an issue, which should incentivise pull requests (which are what moves the development forwards most directly).
    2.  The average ranking per issue or pull request (as applicable) is then calculated.
    3.  Each point that the contribution has above the average, 10% is added to the payout, which incetivises work on poorly-commited repositories the most. If the contribution is less, nothing is removed.
    4.  1% of the remaining money is then given out to the repository (as a small payment for the code review and feature ranking (which is incentivised to be wide due to the “above the average” clause)), with the rest going to the contributor.

So, for example, if a developer made a level-7 pull request on a repository with an average score of 6, then they earn €1.50 (€1.40 after repository payout), but if they did the same  on a place with a average score of 2, they earn as much as €3.50 (€3.40 after repository payout).

The total payout is then calculated finding these rules and then adjusted to the actual budget, with each contribution's earnings being given the correct transformation.

If a developer submits enough poor pull requests to push a repository down by 0.1 feature points on average, they have the “repository tax” increased by 1%.

### Repositories

Each repository gets a set amount of money based on money given to contributors; it's 1% of this money, incentivising good contributors. This also forces repositories to promote themselves and get all the good contributors.

Low average feature scores results in low money amounts. This should incentivise them to get high-quality contributions.

Every week, just before payout, a repository will be checked to ensure feature rankings are fair. Being unfair may lead to being removed from the program.

Goals
-----

### Intended incentives

This charity should incentivise:

*   Repositories to sign up to recieve money.
*   Contributors contributing to the worst repositories the most
*   As many contributions as possible
*   Both pull requests and issues
*   Merging and ranking of pull requests
*   Donations to help improve FOSS as a whole

### Job replacement

Should the charity earn enough money, it might be a viable alternative to jobs. Should the funding be enough, contributors may then flock to the program.

This may lead to a “tragedy of the commons” kind of situation, where more contributors working makes all the contirbutors lose money. As such, contributor enrollment is kept under control to keep wage at a sensible level. Possibly, to avoid this, part of the enrollment process may be asking for at least €50 donated per week in total 

Advert
------

### To clients

#### In general

Looking for a way to give back to the community and make a positive impact on the world? Consider donating to the world of Free and Open-Source Software (FOSS)!

FOSS is a global movement that promotes the use and development of software that is freely available for anyone to use, modify, and distribute. This software is created by a community of developers who work together to create innovative, powerful tools that can be used for everything from simple word processing to advanced machine learning and data analysis.

By supporting FOSS, you're helping to ensure that everyone has access to the tools they need to succeed, regardless of their financial means. This includes small businesses, non-profit organizations, and individuals who may not have the resources to purchase expensive proprietary software.

But FOSS isn't just about providing access to software - it's also about building a more open and collaborative world. FOSS developers work together to share knowledge, exchange ideas, and create solutions to some of the world's biggest problems. From healthcare to education, FOSS is transforming the way we work and live.

So why donate to FOSS? By supporting this important movement, you're helping to:

*   Promote open access to technology and knowledge
*   Foster innovation and creativity
*   Empower individuals and organizations to achieve their goals
*   Build a more collaborative and equitable world

Your donation, no matter how small, can make a big difference in the world of FOSS. By contributing to this movement, you're helping to create a better future for us all. So please, consider donating today and join us in the fight for a more open and collaborative world!

#### Other methods

Other methods of promotion are also given:

*   Supermarket donation buckets, where people donate to FOSS
*   Online “donate” buttons funding this charity, meaning users don't have to pick and choose their favourite developers.

### To Repositories

Are you a part of a FOSS repository that's been struggling to get the support it needs? We're here to help!

Our charity organization is dedicated to supporting FOSS communities and the repositories that they rely on. By joining our program, you'll have access to a neat stream of funding that can help your repository thrive.

We have a simple opt-in process that only requires a letter of interest from your team. Once you're a part of the program, you'll be able to benefit from our unique contribution ranking system that incentivizes developers to work on repositories with the most potential for improvement.

But that's not all - joining our program also means that your repository will have a greater presence in the FOSS community. Our program promotes the best and most innovative repositories, giving your team the recognition it deserves.

Don't let your repository be left behind. Join our program today and watch your project flourish!

### To Contributors

Attention all developers and open-source enthusiasts!

Are you tired of contributing to projects that are underfunded and underappreciated? Do you want to make a real difference in the world of open-source software?

Look no further than our non-profit charity for open-source development. By joining our program, you can receive funding for your contributions to open-source repositories, and help push the world of FOSS forward.

We offer a unique system of payouts based on the quality of your pull requests and issues, incentivizing contributors to work on the worst repositories the most. And with our 10-point quality-level tag system, you can rest assured that your contributions will be evaluated fairly and transparently.

Not only will you be making a difference in the world of FOSS, but you'll also be joining a community of like-minded individuals who share your passion for open-source development. And with the potential for job replacement, you could even turn your passion for open-source into a full-time career.

Join us today and start making a difference in the world of open-source software!
`},{
            "name":'Meaningful contributions exam board',
            "description": "An exam board focused on making subjects worth learning, for everyone involved..",
            "tags": ["ideas", "meaningful-contribs"],
            "content":`
Introduction
------------

I have an idea for an exam board which focuses not on what students can do in a closed, sterile environment, but more rather what they can do in the real world and how they can communicate with others like they will in real life.

Assessment
----------

There is no “final exam” given to students at which point they then are free to forget what they learnt; instead, coursework assignments are given, forcing students to perform meaningful contributions for meaningful grades. These contributions are done over a long period, thus increasing the time spent working on it and the amount remembered. Since work done more closely follows real-world work, there is a motivation to work harder.

Learning
--------

Learning how to do the coursework is given by teachers during “project time”. No contributions may be made until each learning time is finished. This ensures that students who already know how are not given an unfair advantage.

Standardisation
---------------

Every candidate is made equal by most exam boards. The same must be done here.

Candidates are only allowed to spend time in after-school “project time”, spent working on the projects. Anything added outside of “project time” will not be counted unless an equal amount of “project time” is lost. Since every candidate has the same amount of time, they should have the same advantage.

“Project time” should be equal across schools; that is, three weekdays have “project time” after school, where all candidates in the school work together.

Intent
------

The exam board is intended to be used in collaboration with another exam board, as a form of "extra credit" kind of project.

The exam board allows candidates to use their grades from this exam board alongside traditional GCSE grades, resulting in the ability of choice and portfolio addition.

Benifits
--------

This exam board, unlike many others, focuses on benifiting both the candidate and the world around them.

The candidate benifits from having a project added to their portfolio and grades, whilst the world benifits from having useful contributions given to it.

Choices
-------

Each candidate may choose a maximumm of two subjects; this ensures that skills reflect those that they desire to use in the future.  If two subjects are chosen, students must spend more “project time”, one unit for each subject. 

Transparency
------------

Candidates may see the sheets used for marking their work and review them. Should an issue appear, such as misunderstanding or incorrect marking, they may request an examination of the examination and their specific worries.

Years
-----

Year 10 is spent learning the theory, alongside practice rounds of creation. Prerequisites are also learnt here.

Year 11 is spent performing the coursework and relearning parts of year 10, where actual contribution is made.
`},{
            "name":'Modulang guide',
            "description": "A conlang with isolating and polysynthetic features",
            "tags": ["conlangs", "ideas"],
            "content":`
Introduction
------------

Modulang is a new language designed to combine both isolating and polysynthetic features. The volcabulary features no abstract concepts at its core (but there are a series of mixtures).

It's mainly a personal language to make me happy.

Modulang is in VSO order, and uses the simple concept of modifying nouns to get what you want. 

Phonology
---------

Most of the letters are quite similar to English, but there are a few weird letters that will catch you out:

*   ‘ or tt - Pronounce as a glottal stop, as in the stereotypical British “bu’er”.
*   ñ or ny - Pronounce the n as in “canyon”, without the y
*   ŋ or ng - Pronounce as a "g-infected n" in “nothing”
*   đ or bf - Make your lips a circle and blow for an f-like sound
*   ß or sh - Harsh s as in “harsh”
*   x - As in scottish “loch”
*   ll - As in Spanish “mantequilla”
*   ii - As in Catalan  “illa”
*   ħ - No English equivelent, IPA н. See how it's pronounced.
*   à - As in a of English “awesome”
*   è - As in e of English “elegant”

There are also a series of diacritics available:

*   The acute (ó, can also be O!) accent stresses a syllable, like in the difference between “record” (unstresed e) and “récord” (stressed e)
*   The circumflex (ô, can also be oo) accent lengthens the vowel, like in the difference between "hop” (short o) and hope” (long o)
*   The dieresis (ö, can also be .o) accent makes the pronunciation of that vowel seperate from the previous one
*   The merger (bçf, can also be (bf)) letter will merge the mouth-shapes of two different sounds and ask you to pronounce them together

This language also sometimes uses tones, but luckily they're not too common. Tone markings come after their syllables.

*   &#x60;- means a high tone
*   / means a rising tone
*   · or . means a neutral tone
*   &#x60;&#x60; means a descending tone
*   &#x60;_ means a low tone
*   The absence of a marker represents that any tone may be used.

Building a sentence
-------------------

### Selecting a system

There are two main systems of construction: the synthetic and the isolating system.

The isolating system focuses more on freedom and isolated blocks of meaning. This system is very freeing and allows you to use individual modifiers as little “concept blocks”. For example. the particle for respect, “àf” would usually be added to a noun, but on its own can make a sentence representing the concept of respect. “àf.”. You might even add a verb modifier to make it mean the act of being respectful, like, say “àf az aslle”.

The synthetic system prefers a more rigid system; every word MUST begin with a noun-root. However, this system has more restrictive phonotatics to make a more flowing system of pronunciation. A translation of the isolating “àf az aslle”. might be “iàn-ôv-àf-asié-pase” using the polysynthetic system, which means “person of respect &#x60;[verb modifer&#x60;]”. Note how the sentence flows better and quicker. This is actually a whole word, but the dashes are used to seperate morphemes to prevent confusion. In some space-saving texts, the dashes may be removed to make “iànôvàfasiépase”, which makes the individual morphemes harder to seperate but the ink costs cheaper. This is similar to how the government of USSR got rid of the definite “вю” and indefinite “ан” articles in russian and replaced them with cases; it moved a constant ink cost to a ink cost to learn cases once. This was much cheaper. 

In most high-grade literature a mix of both systems is used based on what the sentence need; emphasis on choppy blocks or a nice long flowing word?

### Noun roots

Noun roots are what will begin most sentences. All “pure” noun roots will always stand for some simple, concrete concept. “concrete” simply means that you can interact with it physically.

Since the noun roots are concrete, they are also often onomatopoeic. Animal is “mû” after cow sounds, and wheels are “ xoxúpa” or “ đođúa-” after the sounds of the spokes and the tyres. Moving air is  “đouou” due to your mouth movements whilst saying, and water is “u’u' ” after the swallowing sounds made from it.

However, there are quite a few “shortenings”, which are contractions of common combinations leading to shorter sentences. These can encode abstract concepts. Take the word “ârtellé/”, which means alive. It is formed of the three morphemes “ârté", “dell”, and "aßlle”, which literally means "&#x60;[heart&#x60;]-&#x60;[adjective&#x60;]-&#x60;[to do&#x60;]". The slash at the end represents a rising tone. This means multiple contracts with the same letters can be used.

### Tangent: The transcription system

Sitting between the English translation and the modulang sentence is a confusing system of brackets and dashes, representing the morphemes. This is vital for understanding the structure of a sentence and its individual components.

Let's decode it.

*   A square bracket (&#x60;[xyz&#x60;]) denotes an individual morpheme and what it means
*   A bracket ((xyz)) represents a compond concept forrmed of a few morphemes merged together
*   A dash (-) represents a boundary between morphemes
*   A space ( ) represents a boundary between two words.
*   A underscore around a morpheme group (&#x60;_xyz&#x60;_) represents optionality

### Adding to the nouns: making verbs and more specific concepts

Once you have a noun root, you can now add to it. We'll build “i love you”

The grammar here is simple. First, you specify what you mean with the noun by adding other nouns or particles if you need. For example, to represent love, you might want to use “&#x60;[heart&#x60;]-&#x60;[for&#x60;]-&#x60;[person&#x60;]”, or “ârté-fû-iàn”. A contraction exists, so we'll use that “âre-fû&#x60;&#x60;ià/”. Note the tones.

Now, let's dive into the verb system. We begin by adding the verb morpheme, “-asié-”. Then we add the “to do” and “habitual” morphemes, “pase” and ”âis" respectively. Then we add the subjects and recipients, to do “person-first” and “person-second”, so we get “is” and “tu” respectively.

We mix them all-together and get “âre-fû&#x60;&#x60;ià/-asié-pase-âis-is-tu”, literally “(love)-&#x60;[verb&#x60;]-&#x60;[to do&#x60;]-&#x60;[habitual tense&#x60;]-&#x60;[first person&#x60;]-&#x60;[second person&#x60;]”. We can put the sentence together and get “âre-fû&#x60;&#x60;ià/asiépaseâisistu”. It's a very long word.

Since verbs are subjective and rely on reader and speaker interpretation, there is no intransivity (forcing a verb not to have an object). This means that there is a lot of freedom in verbs. English “kill” and “die” are two different verbs forcing either having or not having a subject, which is basically killing the essence of verbs. Take, for example, “rasié fût is tu”. This means, literally “&#x60;[sleep&#x60;]-&#x60;[future tense&#x60;]-&#x60;[subject i&#x60;]-&#x60;[object you&#x60;]”, so it means “i will make you sleep”, which is a threat. Then, with the same verb, we can do “rasié fût is”, which is like the above but with no “you”, so it means “i will sleep”.

### Optimising

We've already optimised with the contraction, but there's more we can do. A lot of stuff can be inferred from the usage of the slot system. For example, the verb morpheme, to do morpheme, present tense morpheme, and first person morpheme are all optional. This means we can shrink our sentence down to “âre-fû&#x60;&#x60;ià/-âis-tu-o”, by adding an o to the second person to specify them as the object.

As we can see, we have managed to lose a lot of morphemes very easily. These contractions and defaults are what makes the language bearable. 

### An overview of structure

What we have just done is built a sentence. To recap:

> &#x60;[concept&#x60;]-&#x60;[explainers&#x60;]-&#x60;[people doing things&#x60;]-&#x60;[some extra particles&#x60;]

The sentence we built was in VSO order, where the noun root was turned into a verb. VSO is not the only way to structure a sentence; you can have all of the ways.

### More advanced structure

Let's look at cases, adjectives, and more free structures.

We can add a case-particle to a sentence and make the things inside more free. We should now free ourselves from thinking in terms of slots, but instead in terms of “concepts”. 

Each concept is formed of a noun-root and modifiers. Multiple noun roots can be put together into the same concept via “combinator morphemes”, which combine nouns into one. You can then add modifiers to add connotations and other cool info. Let's make an adjective, for the feeling we get from air. First, we start with the breezy kind of air, to get "đouđou". Then, we add the adjective and feeling morphemes, to get “đouđou-dell-fêll”. This adjective, now fully developed, can be used to describe a noun by adding a little ending stating where it belongs, such as “danes” or “daf”

For example, if instead we wanted an OSV version of "âre-fû&#x60;&#x60;ià/-âis-tu-o””, we first split our thing into the consituent parts:

*   âre-fû&#x60;&#x60;ià/-âis - Habitual verb
*   is - Subject
*   tu - Object

Let's first explain cases before readjusting. Cases allow us to tell what role a concept plays. They always go at the end of their respective concept, and allow the brain to readjust for a new concept to arrive.

So, now, we can add a case to both the subject and the object and put them together to get:

> tu-o-is-è-âre-fû&#x60;&#x60;ià/-âis

The o gives the object case and the è the subject case. Note how they're both at the end.

Let's also look at the isolating version of the VSO sentence. In the isolating structure, we do not get to use shortenings like “âre-fû&#x60;&#x60;ià/-âis”, instead explicitly having to define every morpheme.

It is quite simple to convert; find the synthetic version of the morpheme with the correct meaning, and then convert that to the isolating one.

> ârte fûr zàn, alâiz tû o.

We can see how every morpheme is now a seperate word, and that the sentence is a lot harsher and less flowing. Commas can be used to seperate concepts.

Also, what we said about “VSO” is false; put what's important first. VSO is good for begginers (and is also the only order that does not need cases), but the whole reason for the case system is to allow other word orders.

For example, take these three sentences (each meaning “i will make you sleep”):

*   rasié fût is tu
*   tu o rasié fût is é
*   is é rasié fût tu o

In each of these, a different thing is stressed. In the first, the act of sleeping is the most important. In the second, the “you” is the most important, and in the first, the “i”.

More nuanced usage
------------------

### Writing a poem

Let's make a poem with the isolating script. We'll use some special features, like individual morphemes.

Why not a simplistic one along these lines?

> Love.
> 
> Does it exist?
> 
> Not with humans for me!

We will begin by translating love into it's modulang equivalent, “ârte fûr zàn”.

Now, let's see about the next one. It's a question, so we'll be adding “ma” at the end. We can phrase it simply as “&#x60;[pronoun of previous mentioned object&#x60;]-&#x60;[thing&#x60;]-&#x60;[adjective&#x60;]-&#x60;[to exist&#x60;]-&#x60;[adjective refers to previous noun&#x60;]-&#x60;[question&#x60;]”

This becomes “eur d'es exle d'antés ma?” (note for speakers of languages which use “d'” as a contraction: Remember that the apostrophe is a glottal stop, not a contraction. It's pronounced “d-es”, with a hard attack on the second syllable, if you will, and not “dës”, where the diersis seperates syllables).

And then we can finally translate the last sentence to make "&#x60;[pronoun for second-previous noun&#x60;]-&#x60;[with&#x60;]-&#x60;[people&#x60;]-&#x60;[adjective&#x60;]-&#x60;[point of view&#x60;]-&#x60;[first person&#x60;]-&#x60;[negative&#x60;]-&#x60;[exclamation&#x60;]”

This becomes “tođ lô zàn dell exle fûr ist ne, pak!”

So, a translation would be:

> ârte fûr zàn…
> 
> eur d'es exle d'antés ma?
> 
> tođ lô zàn dell exle fûr ist ne, pak!

Since this is a poem, we can also decorate it with tones with no ill-effects.

> ârte fûr zàn…&#x60;&#x60;
> 
> eur d'es exle d'antés ma?/
> 
> tođ lô zàn&#x60;&#x60; dell exle fûr/ ist ne-, pak&#x60;&#x60;!

### Insults

No language is fun without insults. Luckily, modulang has a series of modifier to express insult. You'll be seeing the letter ħ a lot here.

The first thing we'll look at is the “bad” morpheme for adjectives, “ixħre”. This can be used to express that something is bad. You might say “đox d'es ixħre” (synthetic: đor-dell-ikré) to express a bad thorn. 

You might use the negative connotation morpheme for expressing something is negative, like “mokû-âis-mû-è-iàn-o-ħ'a”  (“the animal eats people”, or "&#x60;[food&#x60;]-&#x60;_&#x60;[verb&#x60;]&#x60;_-&#x60;[habitual&#x60;]-&#x60;[animal&#x60;]-&#x60;[people&#x60;]-&#x60;[negative connotation&#x60;]"), where “ħ'a” is used as a morpheme to say that the whole thing is just… bad. 

That's not it. You can say that people are bad, by doing “&#x60;[thing&#x60;]-&#x60;_&#x60;[verb&#x60;]-&#x60;[present&#x60;]&#x60;_-&#x60;[bad&#x60;]-&#x60;[second person&#x60;]”, or “pá-ikré-tu”. Then at the end you, can choose any of four (yes, four to choose from!) disrespect modifiers:

|     |     |     |     |
| --- | --- | --- | --- |
| **Meaning** | **Anyltical morpheme** | **Synthetic morpheme** | **Notes** |
| Friendly disrespect | đut | đllut | Gentle banter disrespect |
| disrespect | ħ’ħuk | &#x60;-đuit- | (sounds like spitting) for disrespect anybody |
| absolute disrespect | ħit | &#x60;-ħrít- | Only for serious disrespect to what you hate |
| THE WORST DISRESPECT | ħ’ħ’llđllđ | ħ’llđllđ | Extremely offensive; never use except with the very worst |

I'd like to warn you not to use the last two if possible; the first two are plenty for those you generally dislike.

If you REALLY hate the person and want them to be tortured for eternity in hell, you might want to use the last morpheme (if you can say it).

Since this is a conlang, i kindly request you follow this rule and keep this a horrific swear word for the times it becomes important.

Comparisons
-----------

Modulang is an intresting language, similar but not equal to other languages.

*   **Mandarin Chinese** - Whilst modulang has tones and a isolating system, it uses far less tones, has no homophones, and only really has words for concrete nouns.
*   **Esperanto** - Modulang might have similar affix-based word construction, but it allows far more flexibility in sentence order. Modulang has less abstract nouns and is much smaller.
*   **Toki Pona** - Modulang has a larger volcabulary, richer system of affixes, free-er word order, but a similar number system and a nice isolating system.
`},{
            "name":'ModCreator',
            "description": "A (failed) webapp that makes mods",
            "tags": ["ideas", "minetest", "coding"],
            "content":`
Introduction
------------

ModCreator is a revolutionary idea, giving Minetes mod development an edge giving it simplicity and the ease of pushing blocks together

User Perspective
----------------

### Overview

The user first enters the software and meets a screen where they can access their latest projects, make a new project, or open one from elsewhere. This is not too dissimilar from the Blender opening screen.

Once they have managed to get into a project, they are met by a blockly-powered scratch-like interface. This interface is used to allow users to drag blocks together and create functionality.

There is a sidebar to the left containing the blocks and their categories, a space in the middle to arrange code together, and a sidebar to the right containing other tools. This bears heavy resemblance to Scratch.

Up on the top, there is a series of tabs. One contains the code, one the “thing” specifics, and another a little asset editor and modifier.

Expanding on the right-hand-side sidebar, there is a testing area containing a virtual Minetest play-area. In most cases, it allows you to control the world. You can then play on it using the green circle. This starts the minetest world and allows some form of testing.

“thing” specifics include things like luminosity and other things as well as other stuff.

Every block of code starts with a simple “event”. This, on the backend, registers a callback. 

### “thing”s

“thing"s refer to units of mods, which can be anything from a function to a node. “thing”s have ways to interact with other “thing”s. 

For example, moreblocks might have a function for creating nodes. This function is a “thing”. Once called, more “thing”s appear in the sidebar, under “dynamic content”, to represent content not  preserved.

### Publishment

After testing, the mod can go through a dedicated publishment stage.

This is done by:

*   User requesting publishment
*   The upload of the source and maybe the workproject files
*   The storage in an appropiate place

As much as possible is automated, making use of automatic JS-based button-pushers to navigate the website.

#### ShareArea

ShareArea is the default place for publishing mods. This place is similar to the Scratch sharing place, where people can view the source code, change it, and other stuff.

This is the reccomended approach

#### ContentDB Approach

ContentDB is the default place to host Minetest mods. As such, there is an approach i have made to allow.

It starts with ShareArea; by publishing a mod to ShareArea, one can then make a “ContentDB integration” request, and await approval. After approval has been created, then the ContentDB is published

There is a disclaimer before a proposal can be made:

> Beware: ContentDB publishment should only be used with high-quality mods. The administrators and moderators of ShareArea reserve the right to arbitrarily prevent any content being published to ContentDB. 
> 
> The creator of any content must accept all warranty and responsibility for any harm produced. 

Aside from this, there is also a git manager available. This allows people to manage a git repository directly from ModCreator, which by default is mainly composed of the Lua files and an “assets” directory containing the workspace. The IDE understands how to open such projects.

The ModCreator interface has its own ContentDB frontend based off the API and makes the git publishment process much easier and simpler. Anything that ContentDB has an issue with is automatically forwarded to the IDE. This disclaimer exists for such publishment:

> DISCLAIMER: The creators of ModCreator and associated projects take absolutely no responsibility or warranty for mods sent to ContentDB using this approach.
> 
> The creators of ModCreator do not condone the publishment of  unnaceptable mods to ContentDB. By using this service, the users accept all responsibility for anything that happens to their mods published to ContentDB.

### Data flow

Data flows in this way when the green button is pressed (which activates the entire stack from blocks to working mod):

*   Blocks
*   Export into lua files (every “thing” becomes a seperate file)
*   Copy into a Minetest instance
*   Enablement
*   Running

Should an error occur, the little screen shows us the error.

The export is one of the more complicated things.

Blocks
------

The blocks are sorted into multiple categories. Most categories allow Lua Logic

Looking at the API, there are also several blocks designed to interact with the API.

Example project
---------------

Let's look at a very simple two-thing mod. It has a function and two blocks.

Each of these are a “thing” and can work together. Functions can be part of a “local scope” and be part of a thing.

Distribution
------------

The code and whole software is open-source. There are a few main methods to distribute code, automatically compiled and made. Each way uses the basic method of HTML file access, but are differently complicated. 

Here they are organised in simplicity to get:

### Single-file

The entire project can be “squeezed” into a single file, a bit like tiddlywiki. This file can then be easily shared among people via a USB stick, making this an appealing method for installation.

However, user settings can't be saved and it might get memory intensive.

### Website

Since the project is made in HTML and CSS by default, this entire project can be put on a website. This is quite simple to access; just type a URL, have internet, and you're ready to go.

Since the source is public, anyone can host their own website.

### Docker version

The docker version is basically the website but hosted locally on a docker container, using the docker marketplace. This is by far the easiest for me to do, just pull and run with a volume. 

The docker volume is basically a “pocket website”, the website version pushed into a box.

### Electron App

The project can also be downloaded as a whole electron app, made available by either a package manager such as apt, or a download from a website. This isn't too dissimilar from Trilium, which works perfectly fine like this. 

### Source

Since the source is browser-compiled, the source code itself can be used to load the website. It's not as easy to share as the above examples, but that's ok.

Minetest Community
------------------

### Problems

There are various problems which can be caused by this software.

The first is unfulfilled promises and stuff. This is caused by me not having the passion to make this doomed project. Maybe i prove myself wrong.

The next is poor-quality mods. Mods come in this intermediate “exported” format from the blockly editor, so it may not be optimised in the way a text-only mod may be.

Goods
-----

### Begginer friendliness

By introducing the Minetest API as a series of self-documenting blocks, the entire community stands to benifit. When a player does manage to make a few mods, they may feel that the limited nature of my program forces them to write text. Since the text is from their code, they understand what it means and may be better.

Long-term stuff
---------------

### Lua imports

One should be able to import a lua project using the Minetest API into the program. This allows a rather direct interface with the exisiting body of mods. 

### “advanced” translation

There will be a more advanced version of blocks better reflecting the Lua syntax. Since this “advanced” version
`},{
            "name":'Minetest Virus Simulator',
            "description": "A (failed) mod idea",
            "tags": ["ideas", "minetest", "coding"],
            "content":`
Introduction
------------

I have been told to make a virus simulator for CS, so here we are.

I'm planning to get it finished by today.

Specifics
---------

### Spread

The infection spreads VERY easily. However, it requires humans are close together or share spaces.

The infection appears physically as little, tiny dots on the floor. They are only visible with close inspection. As soon as a player touches them, they stand a 25% ±10 chance of contracting the disease. The dots will eventually decay after 74 real uptime hours of continuous existence. 

Once a player contracts the disease, they are forced to suffer it for 2-10 hours of playtime. The longer the infection, the weaker it is to allow the player to live longer.

The player will constantly be sending dots everywhere; it's there choice whether or not they wish to infect others. 

### Natural propogation

Dots will naturally appear with a 0.1% chance around someone with no infection. This means diseases will constantly appear.

### Infections

When you get infected, by default you start

Symptoms vary by strain:

*   Loud coughing annoying the player and spreading TONS of dots EVERYWHERE.
*   Random drops of inventory items after shaking the camera
*   Slow health ablation
*   Neausa, resulting in random movement to the player
*   Sensitivity, resuting in health loss for hitting blocks

### Prevention

Infection is very difficult to remove from surfaces by design. They cannot be mined, instead saying “Whether you were trying to get rid of a block or a virus, your tool slips and you cannot get the damn thing!”.

The best prevention is quarantining infected players. There are also “antiviral sprays”, which will kill up to 99% of dots. However, the dots that do survive will no longer work with the antiviral spray.

Antiviral sprays can be crafted specific to a strain by using a cotton swab and using that with an existing antiviral spray in the “lab machine”.

Masks can be worn on the head. They decay quickly and can become expensive as their amount increases. However, chance of being infected drops to 1%.

The most effective prevention, however, is social distancing; if players keep to their own places in the map, nothing spreads.

### Mutation

Each virus belongs to a certain strain. Every strain has certain variables and a colour. Some strains will die quickly, others won't. For example, one infection may have a high chance of dot propogation but low decay time, and another a low chance but higher decay times. Whichever infects will inflence future viruses.

Some dots, with a 10% chance, will randomly “step” one of their variables. They can then conserve it and use it for themselves.

Strains are classified by their “name”, which is a random base-64 string within the dot.

Programming
-----------

### Data storage

Data is stored in world files, containing what each name's hex-value means, alongside other things.

### Todo

See [Todo List](Minetest%20Virus%20Simulator/Todo%20List.md).
`},{
            "name":'Meaningful Contributions Exam Board - Programming',
            "description": "A subject for the meaningful contributions exam board",
            "tags": ["ideas", "meaningful-contribs"],
            "content":`
Syllabus
--------

The programming subject will teach to students:

*   FOSS workflow practices
*   Communication between developers
*   Reading code

Learning
--------

Before partaking in coursework, candidates will first be taught by teachers about how to:

*   Make an account on either Gitlab or Github
*   Find issues and reply to them
*   Create a clear plan to fix issues
*   State their intent to resolve the issues
*   Create branches to write code in
*   Find relevant code to edit
*   Create edits with good code quality
*   Make well-written pull requests with the code made
*   Have higher chances of having merged pull requests
*   How to adapt the pull requests to fit the assesment guidelines

These skills can be very helpful in the future.

Coursework
----------

Each candidate may select any of two projects on a board-approved lists These projects:

*   Have developers with enough time to review changes
*   Use a workflow revolving around issue creation, issue closing, and pull request merging
*   Are hosted on Gitlab or Github
*   Are non-trivial and used by a large pool of people. 
*   Have developers who mahority agree to be part of the exam

Marking
-------

Each merged pull request is marked on a 1-10 scale for each of the following qualities seperately by three unaccosiated examiners:

*   Code quality
    *   Follows the project's standards
    *   Is clear and easy to read
    *   Does not overuse comments
    *   Is conscise
*   Non-triviality
    *   Adds difficult-to-implement or tedious-to-implement features
    *   Improves the project's usability
*   Approval by the community
    *   Recieves a mostly positive reception
    *   Is merged within a 10% margin of the average time to merge a pull request.

The average scores are added up and the total score of all pull requests out of the total possible determines the “marks” available for the contributions.

Benifits
--------

Candidates recieve:

*   Vital communication skills from:
    *   Figuring out what the issue refers to
    *   Helping bring out the details of an ambiguous issue
    *   Campaigning for their pull request
*   The ability to navigate code from:
    *   Figuring out what to change
*   The ability to write code from:
    *   Applying the relevant changes
*   The ability to navigate git repositories from:
    *   Writing and fixing issues
    *   Making pull requests

The projects recieve:

*   Meaningful contributions from candidates motivated to make good contributions due to the marking scheme
*   Issue resolution from candidates motivated to make pull requests
*   Developers aquainted with the software from candidates needing to navigate the code
*   A burst of activity each academic year from candidates working each academic year
`},{
            "name":'Pseudocode Guidelines',
            "description": "My guidelines for writing pseudocode in list form (my most common format)",
            "tags": ["ideas", "coding"],
            "content":`
Introduction
------------

I have found lists to be a good way to conceptualise programs. By specifying the steps required to perform an activity using English, i can save effort needed to look up functions and only need to use my brain to think about programming.

Key:
----

*   Guideline
    *   Information relevant to the example
        1.  Good example step 1
        2.  Good example step 2
    *   Information relevant to the example
        1.  Bad example step 1
        2.  Bad example step 2

Guidelines:
-----------

*   Program flow should approximate flow of information and topology in target language
    *   Target language is Python
        1.  Function x:
            1.  Do y
            2.  Do x
    *   Target language is Python
        1.  Do y
        2.  Do x
        3.  1 and 2 in a function
*   Use steps which will only take 1-10 lines of code or instructions, whichever is more appropiate, to implement. Any bigger, subdivide (excusable if planning general flow, normally before going deeper), any smaller, merge items. Try to minimise amount of code needed using own knowledge.
    *   Target language is Brainfuck
        1.  Loop and decrement ¿¿putabacktickherecell 70¿¿putabacktickhere until it's ¿¿putabacktickhere0¿¿putabacktickhere
    *   Target language is Python
        *   Too long (Can be done with a single call to ¿¿putabacktickhere.append()¿¿putabacktickhere)
            1.  Copy ¿¿putabacktickherelist¿¿putabacktickhere into ¿¿putabacktickherelist2¿¿putabacktickhere
            2.  Subtract items from ¿¿putabacktickherelist2¿¿putabacktickhere
            3.  Find at index length +1 add data
        *   Too big (1 will take a lot of code to perform)
            1.  See if a given Brainfuck program halts given random input when appropiate
            2.  If it halts, tell the user that it wil
*   No words which can be replaced by flow like ¿¿putabacktickhereafter¿¿putabacktickhere or ¿¿putabacktickherebefore¿¿putabacktickhere
    *   Target language is Trillium script
        1.  Do y
        2.  Make x button
        3.  Do l
        4.  Do g
    *   Target language is Trilium script
        1.  Make x button
        2.  Before making x button, do y
        3.  do g after l
*   Be explicit in variable names
    *   Target language is Python
        1.  Add 3 to ¿¿putabacktickhereindexer¿¿putabacktickhere
        2.  Replace the value in ¿¿putabacktickherestring¿¿putabacktickhere with that in ¿¿putabacktickherelist[indexer]¿¿putabacktickhere
    *   Target language is Python
        *   Add 3 to the indexer
        *   Replace the value in the string previously mentioned and make it ¿¿putabacktickherelist[index]¿¿putabacktickhere
*   Keep to a consistent set of terse and clear keywords (the goal here is not to impress but to be clear) and maybe call explicit functions
    *   Target language is Python
        *   Add 3 to ¿¿putabacktickhereindexer¿¿putabacktickhere
        *   ¿¿putabacktickherelaunch()¿¿putabacktickhere ¿¿putabacktickherelist[index]¿¿putabacktickhere amount of missiles
        *   ¿¿putabacktickhere.pop()¿¿putabacktickhere ¿¿putabacktickherelist[index]¿¿putabacktickhere
    *   Target language is Python
        *   increment ¿¿putabacktickhereindexer¿¿putabacktickhere by 3
        *   ¿¿putabacktickherelaunch()¿¿putabacktickhere ¿¿putabacktickherelist[index]¿¿putabacktickhere quantity of missiles
        *   Get rid of the quantity inside ¿¿putabacktickherelist[index]¿¿putabacktickhere
`},
    ]

    return markdowns
}