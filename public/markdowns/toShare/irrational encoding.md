# irrational encoding
By using an irrational number, one can store data. For this proof-of-concept, we will be using ASCII codes.

Encoder

1.  Convert input string to ASCII
2.  Take each ASCII code and look for it inside the irrational number
3.  If the index is longer then the code, put two codes together and look again
4.  Once found, write down the index. 
5.  Write down the length of the sequence at the end seperated by a dash
6.  Write a comma
7.  Give out the sequence in terminal

Decoder

1.  Take in the sequence
2.  Parse it into a list where every even element is a index and every odd element is a length
3.  For each item in the list, calculate to the index plus length times two
4.  Take the numbers from them and read them down into ASCII
5.  Print out the ASCII 

This compression system helps ensure all your words still exist.