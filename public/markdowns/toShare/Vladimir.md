# Story written
**Trigger Warnings** Suicide, self-harm, exam stress, and alcohol abuse; reader discretion advised

I'm on that path again. The cold air and wet pavement pass below me as i cross the surburbia of Leeds, excited to go back home. The overcast sky make a dim and depressing place to be, but it's ok. The copy-paste houses line the street and the car fumes make my choke, but it's ok. The light rain begins and makes a sound against my coat, but it's ok. Home is soon. I like home. It's a warm retreat from reality. Surreal paintings and the strong smell of cinnamon lines every room.  There is fast wifi and plenty of opportunities to escape from the monotony of contemporary life.

Exams are coming. It's not very nice at all. I hate exams. They're always just sitting there waiting to ruin your life over using the wrong term or maybe just wording things incorrectly.

I cross the road, looking both ways for that single occasional car. None are there, so i pass. And then i see something in front of me. It's a notebook, a rather boring one at that. I look at it and pick it up. The rain has destroyed the bloody thing and smeared some ink, but it's perfectly legible. I stow it in my bag. It belongs to someone. Maybe i go find out who they are.

I continue walking, crossing endless roads and walking on some of the most boring places i've ever been. I've heard that in America it's worse. But we're the worst in Western Europe.

I enter our house, sliding the brass key into the well-oiled mechanism. With the crack of the door, the strong smell of cinnamon hits me.

I go upstairs. The walls are blue, the window wide, and the bed warm. On it, like always, sits my computer, the portal to another whole world.

I open the bloody notebook and begin reading. The first page barely contains any writer details, just blank nothingness. Maybe it's been reserved.

I open onto the next page. 2023-01-06, just after New Year. It's only 2023-02-10 today. Maybe this will be a short read.

_"First, this is a private notebook. I don't want no prying eyes. Get them all away from me. The only reason i'm writing this shitty thing with my bad writing is just to introduce something interesting in my life._

_It's Friday. After this dumb weekend i'll go back to school and do some boring crap again. Everything is so boring. I'm sick and tired of it._

_And then i have these shitty exams to do. They're not even real. They're boring mock exams, yet another vehicle of suffering. Damn the exams to eternal suffering. They'll be the death of me."_

2023-01-10.

_"School again. Fuck this place. The people suck, the culture sucks, and the subject are all so boring. The only interesting thing we ever do is the least boring thing; Lunch._ 

_What horrible goop. Grey vegetables, physically impossibly bland desserts, and the most penetrating smell of detergent ever._

_I hate this._

_Going home isn't much fun either. We got some stupid dog that's holding us hostage and my family isn't much help. We'll either waste time making it exhausted or let it bark so much i won't get any revision done. We shouldn't have got a dog. My brother loves to blame others for what they don't do. And my farther still prefers him over me._ 

_I won't be able to do a-levels. I won't be able to proceed in life, not that i want to anymore. Life is boring. The only way i cope nowadays is with that lovely stinky bottle downstairs. A few gulps and all my problems melt away. I know it's bad for me. I know it's no good. I know it's illegal, but i'll die if i can't have that."_

2023-01-16

_"FUCK THIS FUCK THIS FUCK THIS i can't anymore. I had to do an English Literature exam and everyone kept gossiping and speaking and the teacher shouted at me for being too slow and called me a failure but i just can't anymore why is everyone so horrible i wish i could just not do these exams but then i won't get a-levels and i mean like it's more suffering but i'll but i'll i actually don't know wait let me get something to help_

_The spirits aren't helping anymore. I've drunk half a bottle and i even spilt a bit here, see the arrow? All i have now is nausea, arguments, and a horrific headache. I wanted to vomit. I still do, yet i'm still angry. Fuck all this crap._ 

_I don't want to do anything more. This crap hurts, but i have to keep pushing on, even if it'll kill me._

_Wait, maybe i just do that. Let the stress kill me and be a martyr against exams. I'll just keep pushing."_

Bad idea there. You'll just cause more suffering for the school and people around you. At the exam board, they'll keep crushing souls. It's a sad machine. A very sad one at that.

2023-01-20

_"It's the last day before i can finally escape this dumb school. People keep throwing calculators and breaking each other's phones. I fucking hate these people._ 

_And i have an exam on Monday. Some dumb English paper. I hate writing. I hate writing this. I hate doing this shitty “mental health” crap that don't even work! I've wasted enough time on this! The школь's like trying some новий “mindfulness” shit but i don't get any of it!! Fuck this school and fuck this life. Maybe i'll reincarnate as some beautiful butterfly, but the way things are going, i'm probably going to become some rat or slimy snake._ 

_Why does the school force you to go choose what you want to do when you're so young? Nobody know what they want to do! I'm sick and усталь of this crap! take it away я don't like any of this shit take them all away i'm so sick and tired it's becoming exhausting please save me get rid of them i need to get something_

_What a relief. Dad's going to notice soon and beat me up but like i just can't care for it anymore i'm just too усталь to let this shit happening anymore it's just some crap dammit"_

I love English. I love working with the language, stringing characters together in such an intricate way. But it seems here… Russian was used? I don't get it maybe it's their native language. Some people just don't have such a strong grip on English, unfortunately. Sometimes i forget other nationalities even exist.

I've got to go do the dumb dishwasher before other chores. So boring and time-consuming.

I decide to read another entry before going to bed.

2023-01-25

_“I am going to explode. So many… so many… so many exams, so much stress, so much barking, so much loud shouting, so much blame-shifting, so many shit._ 

_Maybe i write what happened. I don't trust this dumb “mindfulness” crap. I thought we wanted to reduce overpopulation?_

_It all started with that dumb English literature paper. The teacher told me off for getting that dumb Tempest thing and An Inspector Calls mixed up. I don't care about the words of some random dead people._

_“An Inspector Calls”? Whatever. Telling the story by recounting experiences of other people is boring. It's just a cash-grab for our attention. I don't care for it._

_And then the dog kept on barking and even though it was the day that my brother was supposed to take care of it he just said that he had some other shit to do and obviously i wouldn't take care of it as it wasn't my day but then dad shouted at me for not doing the dog. Ugh, i'm sick and tired of it._

_See this stain? That's my blood. Obviously, bloodletting hasn't worked. I'm still as sick and tired as i always was. FUCK THIS.”_

No, recounting someone else's story through another story is a perfectly fine way to do things. If i didn't do it, you'd end up misrepresented.

I'm so tired too. So many exams, so many teachers. But i want to grow up and be the next Charles Dickens. I'll just live through this shit and do a-levels and finally finish with this stupid forced education.

I'm tired.. Should i sleep and give this person another day of suffering?  They're probably more tired. But i'm also tired. I have been for the whole week. 

I decide to read another entry.

2023-01-30

_"I'm getting sick and tired. Mock week has passed, but that doesn't mean that i'm getting any better. The dog is still growing, growing more restless. I'm getting more tired. The students are still as shitty as they are and their endless screaming is getting unbearable._ 

_Maybe i just go drink a few liters of alcohol. Or spill that much blood. Or eat the strong disinfectant, burn myself from the inside out._ 

_I'm getting sick and tired of all this crap i have to deal with. I'm too young to make these life-changing decisions on what i want to do. I can't revise, i can't do my exams, and i don't think my life is going anywhere._

_Maybe i restart. Maybe i end myself and see where i go."_

I'm shook. I'm captivated. I know you can't see this, but the handwriting is getting worse. It's shakier and more erratic. I can almost see the tears and sadness in the writing.

Fuck the GCSEs, and fuck stress.

I can't stop reading. It's getting too dangerous not to read it.

And then it hits me, the day after the last, that horrible on. 2023-02-08:

_“Help me. I can't cope. I'm going to toss away this dumb ”mindfullness" notebook and i hope whoever finds it can save me. It's not helping. I'm going to drink the strong disinfectant. 11/02/23,  1:00 AM is your deadline._  
_If nobody saves me, i'll finally rid myself of this dumb world. If someone sees me, remember Vladimir Petrova, 6 cherry lane. Bye."_

I turn the pages in desperation. Nothing but blank pages, nothing. I don't know how anybody didn't find it. I need to do something. It's worryingly close to the time. It's 2023-02-10, 23:54. They're going away soon. I have to do something! I have to do something! I have to do something!

Shit shit shit shit shit shit shit this is bad VERY BAD this is bad VERY BAD this is bad VERY BAD this is bad VERY BAD

My mind snaps in half. This is bad. VERY BAD.

I notice the wet tears on my cheeks, the swimming thoughts in my head, the grief. I've got to do something. It's time. Someone is going to die and it's going to be all my fault. This is bad. VERY BAD.

I take my phone. It's at 0%. Crap. I shove in the charger with vigor, a black lifeline. I desperately squeeze the power button. It won't turn on. Turn on you piece of crap turn on i need you turn on please someone's life depends on this turn on. You need to turn on turn on now you absolute piece of shit turn on you need to wake up please someone's life depends on this turn on please or someone will die please turn on this is bad VERY BAD

This is bad. VERY BAD.

I run through my options. Maybe i go call [116 123](tel:116123) for Samaritans. But they can't really save this poor guy. This is a very real emergency. And for that, i'll call the emergency line 999 but maybe my call won't matter but if i don't call them someone will die this is bad VERY BAD

Fuck! I run downstairs and grab the landline. As i rush upstairs, i jab the number “999" into it and press dial. The dial tone is continuing. It's brutal, that same montonous hum, the barrier between death and life. I'm waiting, i'm waiting, i'm waiting. After a brutal 15 seconds, someone picks up.

999: “Emergency line, how may i assist you?”

I freeze. This isn't the time. 

“I.. i… i….”

I keep on stammering and stuttering this is bad. VERY BAD.

999: “Hello? ”

“Someone… someone… they're considering… someone.. su… su… icide” come on stupid mouth make words now this is bad VERY BAD

999: “Ok, what do you know?”

“I was reading th.. th.. their notebook and it said that they were suffering fr.. from exam stress and they're going to com.. commit it at 01:00 tomorrow and it's late and it's an emergency and i don't know what to do” i stumbled and stammered.

999: “Do you have any details?” the operator said, concerned.

“They said their name was… was…” i struggle to remember. This is bad VERY BAD

I take a look at the notebook. “Vladimir Petrova, from… from… 6 cherry lane, probably… probably in Leeds” i stammer and stummer.

999: "Thank you for providing the information. Could you stay on the line with me while we send help to the address? We have a team ready to respond."

And then, relieved, i collapse from the trauma. Why am i such a wuss?

And i wait. And i wait. I glance at my alarm clock. 00:02. We only have an hour left. I begin to sob. Will they arrive in time.

I'm shaking. The room is shrinking. I'm covered in sweat. 

999: “They've been dispatched. ETA 00:30”

Perfect… maybe there'll be enough time.

And i sit and stare anxiously at the clock. 00:05. The time is passing slowly. The time is passing quickly. Do we have enough time? I can hear other calls in the background. There are drinking emergencies and domestic violence emergencies. Did i prevent people from getting help? Shit, is my call really that important? There's just one person when people in a terrorist attack may not be getting assistance.

Maybe i don't matter. Maybe my call is irrelevant. Maybe i'm a time-waster. This is bad. VERY BAD.

Suddenly, a voice startles me.

999: “They're almost there, hold on”

I was lost in a trance. It's 00:29

I wait in anxiety for a few seconds. It's truly exhausting. 

999: “They've just intercepted the house.” The dispatcher pauses for a second. They're breathless. They have a choked voice. They're sniffling. They're sighing. “They've found an empty bottle of floor cleaner next to his bed”. Before i can hear their drowned sobs fully develop, they mute the line.

The tears that were slowly leaving my eyes begin streaming now. I was too late. It's 00:35. 

I was too late. I was a failure. I didn't save them in time.

I hold my breath as i begin sobbing. Maybe the pain of carbon dioxide will sober me.

They drank the floor cleaner. They destroyed themselves. They burnt themselves from the inside out. They died in the worst pain possible. It was all my fault. If only i had called earlier, told them earlier, maybe they'd be alive to this day And it's all my fault. I killed Vladimir. I killed the poor guy. 

He's dead now what do i do i don't know maybe i run off to the bathroom and cry my heart out i'm not going to drink toilet cleaner because that's bad and i don't want to be another victim but it feels tempting i'm probably going to have guilt for the rest of my life what do i do oh no i don't know i'm scared someone help me please 

999: “Wait, they've just discovered him in the bathroom” the dispatcher says through sobs, disrupting my panic.

999: “He's alive!”

And then i managed to finally snatch a breath. 

999: “He's asking to talk to you”

I hear a quick turn-off of audio and then a teenage voice

Voice: “You… you… saw me?”

“Yeah, your life… meant something to me”

Voice: “Ok… what do i do now what do i do now what do i do now”

I hear a distorted tinny voice, likely from the dispatched team. “We'll send you off to CAHMS for some support” they said.

I hear the dispatcher again:

999: “Can you, by any chances, send us the notebook?”

“How?” i asked.

999: “We'll collect it from you. What's your address”

I told them my address.

999: “They'll go down to Leeds General Infirmary. You can meet him there."

I asked them if there's anything that i need to do.

999: “No, you can sleep now. They're safe.”

I hang up the phone. I put it away and sit back. I take a breath. The adrenaline drains away and the tiredness takes me away.

Tomorrow, i'll go meet this poor guy. Maybe even make friends. Tomorrow.

Tomorrow… to-mor-row… it's a thing that i'm glad someone else will experience that.