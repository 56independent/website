# Programming
Syllabus
--------

The programming subject will teach to students:

*   FOSS workflow practices
*   Communication between developers
*   Reading code

Learning
--------

Before partaking in coursework, candidates will first be taught by teachers about how to:

*   Make an account on either Gitlab or Github
*   Find issues and reply to them
*   Create a clear plan to fix issues
*   State their intent to resolve the issues
*   Create branches to write code in
*   Find relevant code to edit
*   Create edits with good code quality
*   Make well-written pull requests with the code made
*   Have higher chances of having merged pull requests
*   How to adapt the pull requests to fit the assesment guidelines

These skills can be very helpful in the future.

Coursework
----------

Each candidate may select any of two projects on a board-approved lists These projects:

*   Have developers with enough time to review changes
*   Use a workflow revolving around issue creation, issue closing, and pull request merging
*   Are hosted on Gitlab or Github
*   Are non-trivial and used by a large pool of people. 
*   Have developers who mahority agree to be part of the exam

Marking
-------

Each merged pull request is marked on a 1-10 scale for each of the following qualities seperately by three unaccosiated examiners:

*   Code quality
    *   Follows the project's standards
    *   Is clear and easy to read
    *   Does not overuse comments
    *   Is conscise
*   Non-triviality
    *   Adds difficult-to-implement or tedious-to-implement features
    *   Improves the project's usability
*   Approval by the community
    *   Recieves a mostly positive reception
    *   Is merged within a 10% margin of the average time to merge a pull request.

The average scores are added up and the total score of all pull requests out of the total possible determines the “marks” available for the contributions.

Benifits
--------

Candidates recieve:

*   Vital communication skills from:
    *   Figuring out what the issue refers to
    *   Helping bring out the details of an ambiguous issue
    *   Campaigning for their pull request
*   The ability to navigate code from:
    *   Figuring out what to change
*   The ability to write code from:
    *   Applying the relevant changes
*   The ability to navigate git repositories from:
    *   Writing and fixing issues
    *   Making pull requests

The projects recieve:

*   Meaningful contributions from candidates motivated to make good contributions due to the marking scheme
*   Issue resolution from candidates motivated to make pull requests
*   Developers aquainted with the software from candidates needing to navigate the code
*   A burst of activity each academic year from candidates working each academic year