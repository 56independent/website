# ModCreator
Introduction
------------

ModCreator is a revolutionary idea, giving Minetes mod development an edge giving it simplicity and the ease of pushing blocks together

User Perspective
----------------

### Overview

The user first enters the software and meets a screen where they can access their latest projects, make a new project, or open one from elsewhere. This is not too dissimilar from the Blender opening screen.

Once they have managed to get into a project, they are met by a blockly-powered scratch-like interface. This interface is used to allow users to drag blocks together and create functionality.

There is a sidebar to the left containing the blocks and their categories, a space in the middle to arrange code together, and a sidebar to the right containing other tools. This bears heavy resemblance to Scratch.

Up on the top, there is a series of tabs. One contains the code, one the “thing” specifics, and another a little asset editor and modifier.

Expanding on the right-hand-side sidebar, there is a testing area containing a virtual Minetest play-area. In most cases, it allows you to control the world. You can then play on it using the green circle. This starts the minetest world and allows some form of testing.

“thing” specifics include things like luminosity and other things as well as other stuff.

Every block of code starts with a simple “event”. This, on the backend, registers a callback. 

### “thing”s

“thing"s refer to units of mods, which can be anything from a function to a node. “thing”s have ways to interact with other “thing”s. 

For example, moreblocks might have a function for creating nodes. This function is a “thing”. Once called, more “thing”s appear in the sidebar, under “dynamic content”, to represent content not  preserved.

### Publishment

After testing, the mod can go through a dedicated publishment stage.

This is done by:

*   User requesting publishment
*   The upload of the source and maybe the workproject files
*   The storage in an appropiate place

As much as possible is automated, making use of automatic JS-based button-pushers to navigate the website.

#### ShareArea

ShareArea is the default place for publishing mods. This place is similar to the Scratch sharing place, where people can view the source code, change it, and other stuff.

This is the reccomended approach

#### ContentDB Approach

ContentDB is the default place to host Minetest mods. As such, there is an approach i have made to allow.

It starts with ShareArea; by publishing a mod to ShareArea, one can then make a “ContentDB integration” request, and await approval. After approval has been created, then the ContentDB is published

There is a disclaimer before a proposal can be made:

> Beware: ContentDB publishment should only be used with high-quality mods. The administrators and moderators of ShareArea reserve the right to arbitrarily prevent any content being published to ContentDB. 
> 
> The creator of any content must accept all warranty and responsibility for any harm produced. 

Aside from this, there is also a git manager available. This allows people to manage a git repository directly from ModCreator, which by default is mainly composed of the Lua files and an “assets” directory containing the workspace. The IDE understands how to open such projects.

The ModCreator interface has its own ContentDB frontend based off the API and makes the git publishment process much easier and simpler. Anything that ContentDB has an issue with is automatically forwarded to the IDE. This disclaimer exists for such publishment:

> DISCLAIMER: The creators of ModCreator and associated projects take absolutely no responsibility or warranty for mods sent to ContentDB using this approach.
> 
> The creators of ModCreator do not condone the publishment of  unnaceptable mods to ContentDB. By using this service, the users accept all responsibility for anything that happens to their mods published to ContentDB.

### Data flow

Data flows in this way when the green button is pressed (which activates the entire stack from blocks to working mod):

*   Blocks
*   Export into lua files (every “thing” becomes a seperate file)
*   Copy into a Minetest instance
*   Enablement
*   Running

Should an error occur, the little screen shows us the error.

The export is one of the more complicated things.

Blocks
------

The blocks are sorted into multiple categories. Most categories allow Lua Logic

Looking at the API, there are also several blocks designed to interact with the API.

Example project
---------------

Let's look at a very simple two-thing mod. It has a function and two blocks.

Each of these are a “thing” and can work together. Functions can be part of a “local scope” and be part of a thing.

Distribution
------------

The code and whole software is open-source. There are a few main methods to distribute code, automatically compiled and made. Each way uses the basic method of HTML file access, but are differently complicated. 

Here they are organised in simplicity to get:

### Single-file

The entire project can be “squeezed” into a single file, a bit like tiddlywiki. This file can then be easily shared among people via a USB stick, making this an appealing method for installation.

However, user settings can't be saved and it might get memory intensive.

### Website

Since the project is made in HTML and CSS by default, this entire project can be put on a website. This is quite simple to access; just type a URL, have internet, and you're ready to go.

Since the source is public, anyone can host their own website.

### Docker version

The docker version is basically the website but hosted locally on a docker container, using the docker marketplace. This is by far the easiest for me to do, just pull and run with a volume. 

The docker volume is basically a “pocket website”, the website version pushed into a box.

### Electron App

The project can also be downloaded as a whole electron app, made available by either a package manager such as apt, or a download from a website. This isn't too dissimilar from Trilium, which works perfectly fine like this. 

### Source

Since the source is browser-compiled, the source code itself can be used to load the website. It's not as easy to share as the above examples, but that's ok.

Minetest Community
------------------

### Problems

There are various problems which can be caused by this software.

The first is unfulfilled promises and stuff. This is caused by me not having the passion to make this doomed project. Maybe i prove myself wrong.

The next is poor-quality mods. Mods come in this intermediate “exported” format from the blockly editor, so it may not be optimised in the way a text-only mod may be.

Goods
-----

### Begginer friendliness

By introducing the Minetest API as a series of self-documenting blocks, the entire community stands to benifit. When a player does manage to make a few mods, they may feel that the limited nature of my program forces them to write text. Since the text is from their code, they understand what it means and may be better.

Long-term stuff
---------------

### Lua imports

One should be able to import a lua project using the Minetest API into the program. This allows a rather direct interface with the exisiting body of mods. 

### “advanced” translation

There will be a more advanced version of blocks better reflecting the Lua syntax. Since this “advanced” version