# Story
I sit in the staff room. The light's on, and the sun is quite high. It's nice and warm in here. It's a warm hospital, kept at the temperature of 21ºc. I like it. It's where i've lived for the last 30 years, surrounded by death, life, and rebirth in the core of the hospital, the grease that keeps the churning machine alive.

It's a medium-sized boxed off staff-room. There's a table in the middle, the counter and mini-kitchen. There are posters on the walls related to medical conditions and stress alleviation. It's an old staff-room, set in the core of the oldest part of the hospital. This entire hospital is desperately expanding to meet the needs of those in the nearby city, an onion of age. Walking through this part of the hospital is difficult.

There aren't many windows in here save for that high frosted window, full of light and hope at this moment. The light shines in with midday vigor and lights up the old, peeling walls and the stained dropped ceiling.

It's 07:34, at least i think so.  My circadian rhythm is completely off after hours on and hours off and i don't even know what the time is. 

The door swings wide open and a cold breeze comes in. It's probably the winter, a bleak and cold time.

A nurse in a light blue gown sits down on the chair. Another one comes in a short while later, dressed in a similarly cheap green gown. .

“I've been working with Antony.” the one in the light blue gown sparked. “how can we make sure his poor leg is fixed quickly? He's got a big conference to go to next week, and he'll be gutted if he can't go.”

“He's… getting better as we speak. Soon we'll be able to put on a cast, and, send ‘im off”, shrugged the green gown one. “I think we just need, to wait, unfortunately. Different people… some are just slower." they commented.

“I almost got hit this morning. People shouldn't really cut off on the motorway”, the first nurse reported. "I could have been the next Antony, imagine that!”

“Yeah, how's, James, by the way?” asked the second nurse, laden with some form of concern. “I've heard that the hot steel didn't do any… good” he continued..

“Not very well. His face is still trying to recover, poor thing” said the first nurse. “It's horrible, what these companies have been doing to people”.

“Maybe we should... unionise” the second nurse joked dryly.

“Yeah, maybe we should” the first nurse replied, unaware of the joke. “It's just a big joke, this government. Do you think it'd work?”

Before the first nurse could reply, another nurse burst through the door and seated themselves at the table. 

“Poor Rachel, all on her own” the newly-arrived nurse worried. “I checked up on her, she's still troubled, poor thing.”

“Could we get her any form of help?” the same nurse continued. “It's a bit concerning and CAHMS isn't coming until later this Thursday, sadly”

Rachel, from what i've heard at sunrise, is really struggling with the stress of her life. Poor Rachel, all on her own. 

“Yeah, maybe we should give her, some warm, tea?”, said the endearing second nurse. “She likes… chamomile, i've heard".

“That reminds me! Antony needs some tea too”, the first nurse said. “He likes chamomile too, says it's good for the bones”.

From what i've heard, tea is not their only similarity. They also go to the same college and drive the same type of car.

For most of the time here this morning, i have been sitting around passively, thinking about life and listening to the constant stream of conversation. I like my job. I get to have 24/7 access to electricity, all the staff love and endear me, and i'm probably one of the main reasons most of the staff are alive today. I'm probably the nurse nurse, maybe?

The blue-dressed nurse also stood up and put a teabag in a paper cup and set the kettle to boil. The newly-arrived nurse was also at the kitchen, preparing her pot noodles.

The nurse who had just arrived came up to me, opened my mouth, and gave me pot noodles. My mouth was closed around them and it was time to warm them up after some panicked stabs at the buttons.

WHIRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR

The nurse checked on the pot noodles. They're still raw.

WHIRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR

Again, they came up to look, obviously quite hurried.

WHIRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRDINGDING

And at last, i was done. They opened my mouth and took the pot noodles from my now-humid interior.

The kettle, which i had not noticed was shining it's “i'm heating up the water” light, was beggining to bubble itself apart, a 20 year old contraption. 

The blue-dressed nurse took the pre-prepared paper cups with their teabags and added some water from the kettle. They passed a cup on to the other nurse. They walked out of the room, most likely to their patients.

Just after they left, a doctor stumbled in and collapsed onto a chair. They looked quite tired. It's not the normal “i had a long day” tired, but a much deeper “fuck the system” tired. Deep wrinkles criss-crossed across their 30 year old face, and their eyelids dropped in such a way that it was like they had wanted to stay shut for many more hours last night, for way too many “last night”s.

“You know what, we should unionise!” they said. “It's exhausting having to work for hours. I've done 5 hours of work and i have to do 2 more!” 

“You're lucky” the newly-arrived nurse said, their face young, hiding years of abuse by their alarm clock and the pager. “I have 12 and i've only done 3 so far.”

The doctor took out a box of homemade pasta. It was well made, so maybe it was bulk-made in 

Again, it's time for my duty. Still moist from the pot noodles, some homemade pasta was emplaced within.

WHIRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRDINGDING

My mouth was opened again and the nice warm pasta was taken out.

A nurse comes in. He was pale white. “i had a rough shift” they said. ”There was a lot of blood earlier. A LOT"

“So much blood… so much” he complained. “At least it's quiet now so i can focus on what's important”.

He went to the table and opened his laptop, entering what seems to be the email program “Time to email to the union. I've had enough of this silly lack of wage and too many hours”.

Suddenly, as the first few words rolled out of the fingers, came a loud beep from a pager. “REQUESTED TO PERFORM TRIAGE AT GREEN RECEPTION”,  i could barely see.

The doctor and the nurse ran off to the green reception. It was really bad, they could tell. It was just something in the air, and i could feel it, the heart-wrenching feeling of a tradegy,

It was bad. Several hours passed. Nobody came here, except the cleaner mopping the floor and singing _9 to 5_ by Dolly Parton to herself. 

Finally, the door swing open and yet another tired doctor came in. All i've seen for years is tired doctors. Ever since COVID-19 they've been exhausted. And the government isn't good either. They just pay them the bare minimum. I'm about 30 years old. Why can't i just die? 

“I'm sick and tired of it. Constant work, no breaks, no pay” the doctor complained to the dark, empty room. 

A little while afterwards, they were joined by another nurse who entered.

The nurse told the doctor the rumors about the massive motorway pileup, the new railway line, and more rambling on. 

After some thoughtful silence, the nurse pronounced “You know what, it's absolutely freezing outside. I hate it. I wish it was warmer”. The doctor replies “be careful what you wish for”.

And then it came, the faucet of superstition, the broken bones, the spilt blood, the corroded, burning metal dust of capitalism; the loud pager bleep - this time something about widespread burns to the face and body.

And again, a few more hours passed. The sun began to set and the night started beggining again.

After the sun set, some nurses came in, again exhausted like all the others before them.

“That steel refinery is a menace” one muttered. “it only deforms and destroys” they said quietly.

Another nurse also complained “I hate the motorway. It only kills, either by pollution or metal”.

And then the the third nurse, they said “The man with the broken face, he's in a coma now". "He's on the way to death”.

“We better take good care of him. We don't want to be liable. It can only hurt our wages”, said the green-suited doctor. “We just need to try our best, maybe it'll pay off”.

“Not everything is worth doing. Some things should just be left alone to fix themselves”, said the nurse with greying hair. They were at the end of the line, only a month away from retirement, left hopeless and empty, waiting for their escape from this dangerous machine.

And life went on like this, people coming and going, trying to catch a small break, until the evening.

My part of the hospital is one of the oldest parts and it is really beggining to show its age. Maybe it's the end. The cracks in the wallpaper, are they really that shallow? Or do they expand deep through the wall? I haven't seen that part. 

I hear a deep rumble from upstairs. They're moving a bed above that old tile again. I hear some debris drop onto the dropped ceiling. The old tile creaks in complaint as it is forced to handle new weight.

Another bed, another drop. Maybe it's related to the earlier bursts of activity. Suddenly, the dropped tile, unable to handle all the new weight fractures. It falls to the floor alongside a sizable pyramid of debris and a rather loud noise. A rock hits my metal head. It's not much and does not hurt.

A doctor, shocked by the noise, comes in. They open their phone.

“I'll need 3 contractors here; a ceiling tile has just fallen!” they say panicked.

A little while later, the site team arrive. They take a look at the fallen tile. They look at the hole in the ceiling. They look into the hole.

They have some worried conversation over the hole and the floor. Apparently, they can see the lights of upstairs through the rebar.

My entire part of the hospital is evacuated. It's too old to live anymore.

After a few days of blackness, and the noise fading away, the old part is evacuated. A man comes in and places some remotely-activatable bombs in the staffroom. I sit anxiously. I'm too old to be useful anymore. After a few hours, my time has come. The bombs will explode and i'll be crushed under a mountain of rubble, but first they're turning off the electricity.