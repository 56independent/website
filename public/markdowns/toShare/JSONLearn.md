# JSONLearn
Introduction
------------

Historically, learning resources have been disparate and locked behind paywalls. I aim to change that. 

UI
--

The UI is mainly provided by HTML, CSS, and JS; alonside Bootstrap and Jquery.

### Main Screen

The main screen features two main halves; the left allowing browsing through files you have and those you can access online, and the right half allows  you to create a topic.

### Left halve

The top bit is an overview of files you have, which can be ordered in different ways.

The bottom bit allows 

### Activity area

You learn in terms of topics, have some notes to learn from, and then an activity area where you can:

*   Download the learning material in various formats:
    *   HTML
    *   PDF
    *   Markdown
    *   JSON
*   Download the activity in all forms mentioned above
*   Do an activity using a JSON database:
    *   Quiz
    *   Game

Data holding
------------

The directory structure can be compressed into a single file:

```text-plain
{
    "topics":{
        "fun-stuff":{
            "kittens":{
                "name":"Fluffy kittens"
                "markdown":`
                ~Kittens are |cute~file,-quiz=. I like kittens.
                `,
                "questions":{
                    "title":"parent",
                    "types":[quiz] // We've just defined the type. Maybe we add questions, but we don't need to as they're already in the course material.
                }
            }
        },
        "not-fun-stuff":{
            "bubonic-plauge":{
                "name":"How rats spread bubonic plauge",
                "markdown":`
                ## Introduction
                BLA BLA BLA HORRIBLE STUFF HERE NOT RIPE FOR AN EXAMPLE.
                `,
                "questions":{
                    "title":"parent",
                    "types":[quiz] // We've just defined the type. Maybe we add questions, but we don't need to as they're already in the course material.
                }
            }
        }
    }
}
```

This compressed format squashes the directory structure into a series of files.

JSONlearn will automatically convert between a directory structure and a JSON structure to provide the best experience.

### Quizzes

The learning material comes as a Markdown file, with [_MathJax_](https://tiddlywiki.com/#MathJax) or [_TeX_](https://tiddlywiki.com/#TeX) formula support. You can use this to teach.

The quizzes come as a JSON object, structured a bit like this:

```text-plain
"topic":{
    "questions":{
        "title":"Welsh digraphs"
        "*types":["ask", "quiz"]
        "questions":[
            ["Ll","Put your tounge on the roof of the mouth and blow", ["match", "typeQuestion"]]
            ["Dd", "Like th", ["match", "typeQuestion", "-quiz"]]
        ]
    }
}
```

The objects are the question banks. You have various items to consider. There are types, which determine how a question may be asked:

| **Type** | **Description** |
| --- | --- |
| `match` | Asks the user to match the question and the answer in a pool of `match`\-type questions |
| `type` | Asks the user to type either the answer or question given the opposite half |
| `typeAnswer` | Asks the user to type the answer, given the question |
| `typeQuestion` | Asks the user to type the question, given the answer |
| `quiz` | Give x amount of answers available and ask the user to select the corret one |
| `-` before any type | Do not use the `*type` of the afterwards one |

Intresting. These can also be arrays, allowing for the \* or + at the end. The times means you have to match them all, the plus just two randomly picked. Add a decimal to the times to give the percentage (or numberto match), add a decimal to the plus to choose the bias of the random.

The `*types` are types that apply to every question.

Now, markdown can also make its own questions using brackets of a special type. This takes in a sentene and takes the verb and splits afterwards:

> ~Carbon can bond with 4 hydrogens~

Might become:

> \["Carbon can bond with", "4 hydrogens"\]

or:

> \["Carbon can bond", "with 4 hydrogens"\]

This is determined using a fair random.

To make it easier for the compiler, in cases with multiple verbs or for specific placement, you might (read: must place a pipe character:

> ~Carbon can bond with |4 hydrogens~

will become:

> \["Carbon can bond with", "4 hydrogens"\]

See?

### Directory

To store the directory structure, the JSON file contains all we need:

```text-plain
{
    "topics":{
        "fun-stuff":{
            "kittens":{
                "name":"Fluffy kittens"
                "markdown"
            }
        },
        "not-fun-stuff":{
            "bubonic-plauge":{
                "name":"How rats spread bubonic plauge",
                "markdown"
            }
        }
    }
}
```

Note how all topics are stored in topics; this is the root directory. Each subobject is either a topic (if it contains any of `name`, `markdown`, or `questions`), or a folder containing topics.

### Expanded directory structure.

In expanded structure, every key-value pair inside the topic is a seperate file; questions is a JSON array, markdown is a markdown file, and the name is contained in `name.txt`.

This allows easier editing by external programs.

MarkRound
---------

MarkRound Is the markup language. It features these additions:

*   Questions in-text
    *   `~ ~` hold the question
    *   `|` seperates the question and answer
    *   `x,y` are the question asking types
    *   `=` finishes the question asking types 
    *   Example: `~Leonardo deCaprio |played films~match,-quiz= and ~Leonardo daVinci |was an ancient polymath~match,-quiz=.`
        *   Produces:

```text-plain
[
	["Leonardo deCaprio", "played films", ["match", "-quiz"]],
	["Leonardo deVinci", "was an ancient polymath", ["match", "-quiz"]]
]
```

*   Tex and MathJax usage

Publication
-----------

This entire program is a FOSS program designed to make it easy to create and use learning guides. As such, it is published on my website. All code is client-side and there is no server, mainly due to limitations in hosting, but also to make it easier to share and use.

Every JSON file containing a subject is designed to be sharable on it's own, maybe using a pastebin or zip file. 

The source code is freely distributed and an electron version exists to install on the computer if in offline environments.

An online “marketplace” of sorts exists for these questions and resources. They are first checked to see if they compile before being sent onto the marketplace. It's a little like [https://lmms.io/lsp/?action=browse&category=Samples](https://lmms.io/lsp/?action=browse&category=Samples), where subjects can be downloaded with one click.

The marketplace is a seperate concept and entity and that is made clear via some subtle but noticable differences.

There is also a “propietary marketplace” but that is intentionally designed to “fail” whenever visiting and be clunky.

Differences from FlashQuizzr
----------------------------

FlashQuizzr is a simplistic Python program mainly concerned with quizzing based off simplistic JSON.

This is a sample of FlashQuizzr JSON:

```text-plain

{
    "electromagnetic":[
        ["Electromagnetic (EM) induction is ",[" generate electricity"]],
        ["EM induction is ",[" voltage is induced in a conductor or a coil when it moves through a magnetic field or when a magnetic field changes through it"]],
        ["In the motor ",[" is already a current in the conductor which experiences a force"]],
```

Note the way topics are quite simplistic; they're just a bank of questions with answers. There are no learning materials, no tags, no extra information, no places extra info can sneak in.

However, this JSON is designed to allow some form of new data to sneak in without breaking older technology.

Tech Stack
----------

*   HTML
*   CSS
*   JS
*   Jquery
*   Bootstrap
*   Vue (for various contents)

Courses
-------

Out of the box, JSONLearn provides a few example courses:

*   IGCSE
    *   Buisness
    *   CS
*   Languages
    *   English-Spanish
    *   English-Catalan
    *   Spanish-English
    *   Spanish-Catalan

Goal
----

What's the goal of all this novel stuff? To spread knowledge. By standardising learning material into a single JSON file easily shared, knowledge can be spread quickly. My website can make an offical opener of sorts and allow all clients to share knowledge.

Since the format is plaintext, it makes the format a lot more open.