# Pseudocode Guidelines
Introduction
------------

I have found lists to be a good way to conceptualise programs. By specifying the steps required to perform an activity using English, i can save effort needed to look up functions and only need to use my brain to think about programming.

For an example of this list-based pseudocode, see [Detailed Algorithm](56i-distro/Interpreter%20In%20Haskell/Stages/Lexer/Detailed%20Algorithm.md).

Key:
----

*   Guideline
    *   Information relevant to the example
        1.  Good example step 1
        2.  Good example step 2
    *   Information relevant to the example
        1.  Bad example step 1
        2.  Bad example step 2

Guidelines:
-----------

*   Program flow should approximate flow of information and topology in target language
    *   Target language is Python
        1.  Function x:
            1.  Do y
            2.  Do x
    *   Target language is Python
        1.  Do y
        2.  Do x
        3.  1 and 2 in a function
*   Use steps which will only take 1-10 lines of code or instructions, whichever is more appropiate, to implement. Any bigger, subdivide (excusable if planning general flow, normally before going deeper), any smaller, merge items. Try to minimise amount of code needed using own knowledge.
    *   Target language is Brainfuck
        1.  Loop and decrement `cell 70` until it's `0`
    *   Target language is Python
        *   Too long (Can be done with a single call to `.append()`)
            1.  Copy `list` into `list2`
            2.  Subtract items from `list2`
            3.  Find at index length +1 add data
        *   Too big (1 will take a lot of code to perform)
            1.  See if a given Brainfuck program halts given random input when appropiate
            2.  If it halts, tell the user that it wil
*   No words which can be replaced by flow like `after` or `before`
    *   Target language is Trillium script
        1.  Do y
        2.  Make x button
        3.  Do l
        4.  Do g
    *   Target language is Trilium script
        1.  Make x button
        2.  Before making x button, do y
        3.  do g after l
*   Be explicit in variable names
    *   Target language is Python
        1.  Add 3 to `indexer`
        2.  Replace the value in `string` with that in `list[indexer]`
    *   Target language is Python
        *   Add 3 to the indexer
        *   Replace the value in the string previously mentioned and make it `list[index]`
*   Keep to a consistent set of terse and clear keywords (the goal here is not to impress but to be clear) and maybe call explicit functions
    *   Target language is Python
        *   Add 3 to `indexer`
        *   `launch()` `list[index]` amount of missiles
        *   `.pop()` `list[index]`
    *   Target language is Python
        *   increment `indexer` by 3
        *   `launch()` `list[index]` quantity of missiles
        *   Get rid of the quantity inside `list[index]`