# Simple Protocol
Introduction
------------

What if instead of needing expensive servers, websites could scale at the same pace of their userbase? What if a website was not a billboard to be seen from miles away but a leaflet you could carry around? And what if data was not held by the server, but held by the clients?

My new protocol makes the internet more open and free by design.

Basic Topography
----------------

The protocol must have some form of topography for data to move around.

The protocol is formed of:

*   Navigation servers
*   Seeds
*   Clients

### Navigation Server

A navigation server contains information on the clients and the seeds, and ensures that every client “knows” where every other client is and what websites they contain. There can be multiple of them and they can even be seeds. However, there must be one large “phonebook” contained online for clients to know the navigation servers.

### Seeds

The seeds are the original hosters of information. Due to the design of the protocol, they do not need to be large-scale industrial servers; they can be some random sysadmin's laptop. 

These seeds should be online as much as possible, but given a large enough client mesh, they can afford some downtime.

### Clients

The clients are those that take from seeds and other connected clients to grab the information.

Software and Overlying Abstractions
-----------------------------------

The protocol will by abstracted away under layers of software to make it more palatable to the average user.

### Standardised Shared Content (SSC)

Standardised Shared Content (SSC) is formed of docker images containing the required software to run the website frontend. 

These are what is shared by seeders and have a mandatory size limit of 100 mb to encourage minimisation and speed of access.

These run in a sandboxed Linux environment (as is provided by docker), even on Windows. This gives us high portability and allows not just websites, but software.

The SSC is kept to one machine and has no networking capabilities; only communication with the database blueprint. The SSC can be thought of as a “RAM copy” of the website; any data changed on it is deleted unless it is inside the Database Blueprint

### Database Blueprint

The Database Blueprint can be thought of as boxes in boxes. One box in this analogy is connected to all the other boxes but will only accept boxes from the seeders; to send to other computers they must use the mail slot and ask the seeders or navigation server for permission. As long as the seeder copy allows it, the data may be sent.

The Database Blueprint allows the SSC to put items inside the boxes for semi-pernament storage. The teleportation box is the shared storage box. For example, a wiki may use the SSC to show wiki pages, but the shared boxes to hold the wiki pages and save and write from them, with account information stored in the non-SSC boxes.