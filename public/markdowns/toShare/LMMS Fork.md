# LMMS Fork
Introduction
------------

LMMS has historically been quite poor, a small abandoned piece of software with barely any work done to it. It no longer has direction, a push, or dominance in the market. IT SUCKS!!!!

This is why i plan to make it work. My main goal is to get better then all the other DAWs, create a new shared format, and make LMMS more user firendly.

Mission Statement
-----------------

I, 56independent, strongly belive in the power of FOSS software. I love music, especially electronic music by the likes of Iglooghost, SOPHIE, Sevdaliza, Arca, and various others. However, the FOSS space has not had a DAW able to compete with propietary software.

I believe that if i were to make the best so far, LMMS, and make it better, that LMMS might be on a fast track to being the best DAW, bringing in developers moe professional then me.

Main direction
--------------

My project is made to:

*   Supercharge LMMS with features
*   Make LMMS beautiful
*   Make a converter from other DAWs into LMMS
*   Make a “shared format” using plaintext.

Inspiration
-----------

Inspiration can be taken from Tanatacrul's conversion of MuseScore 2 into 3 and then 4.

This proejct could take from this idea, and move LMMS from this “LMMS 1” phase and make it a LMMS 2 to build a platform for others to convert it into LMMS 3.

Time
----

A lot of time will have to be spent on this. This is why i have determined to spend my study leave making 

Features
--------

Here is a simple feature comparison

| **Feature** | **FL Studio** | **LMMS** |
| --- | --- | --- |
| Plugin support | Serum, Omnisphere, Kontakt | Limited plugin compatibility |
| Advanced automation | Complex automation patterns and curves | Basic automation system |
| Mixing and mastering tools | Advanced EQ and compression options | Basic mixing and mastering tools |
| MIDI support | Advanced MIDI editing tools, external MIDI controller support | Limited MIDI tools |
| Piano roll | Ability to add multiple notes at once, extensive editing options | Basic piano roll |
| Sample library | Wide range of sounds and instruments | Limited sample library |
| Audio recording | Multi-track recording, direct recording from microphone or external instrument | Basic audio recording system |