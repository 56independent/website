# Biology virus simulator
Basic Types
-----------

These are the types of matter:

| **Type** | **Description** |
| --- | --- |
| Cell parts | Exist in every cell and are used to make things work properly |
| Non-biological | Any passive matter which does not co-ordinate "living" |
| Enzyme | Turns matter into other matter and slowly degrades |
| Hormone | Has a little "message" protein at the end of it; will reach a cell that can recieve it and transmit the protein. |
| Cell | A "living" components; recieves messages by enzyme and can reproduce and perform various functions. |
| Virus | A non-body component designed to reproduce by body |

And these are the components of matter 

| **Type** | **Part** | **Function** |
| --- | --- | --- |
| Cell parts | DNA | A key-value table of genes and their value |
| Splitters | Replicates a cell with a slight (1/50) chance of mutation |
| Antigens | The places where glucoses can "lock in" and enter the cell |
| Non-biological | Foods | Target for the enzymes |
| Glucoses | Provides energy for all cells |
| Dangerous Waste | Comes from used glucose and virus; harms the cell walls |
| Waste | A harmless version of waste; will pass safely |
| Enzyme | Food enzymes | Turns food into glucoses. |
| Waste disposers | Turns dangerous waste into brown waste |
| Virus-eaters | Turns a virus into dangerous waste. |
| Hormone | Stem turners | Floats around and turns stem cells into the needed cell |
| Enzyme commands | Will tell enzyme-makers what to make. |
| Reproducers | Tells a specified cell type to reproduce once |
| Cloners | Will initiate the reproduction mechanism for bodies |
| Cell | \[Skin-type\] Muscle | Provides movement to get foods and removes waste |
| \[Skin-type\]<br><br>Mitchocondria | Takes food being passed around the outside and turns it into glucose using “pocketed” enzymes. |
| Immuner | Will destroy a virus after a few seconds of prolonged contact and turn it into wastes |
| Enzyme-makers | Will create enzymes as others denature and are "absorbed" through antigens |
| Stem | Will move around and convert into cells as the body grows |
| Neuron | Co-ordinates hormone creation based off observations (will command for everything to be in the correct proportion as defined  by a list of percentages) |
| Virus | Virus | A simple piece of DNA encased by a cell wall with antigens; if antigens match, it attempts to enter and infect, reproducing with a slight chance of mutation. |

Looks
-----

The simulator is made using Java Processing.

The simulator uses fluid-shaped cells which change their shape based on turbulence in the water. 

The bodies (bags of cells) are suspended in the water. Every body has two layers:

*   “Functional” top layer used for the simulation
*   “Backdrop” back layer holding the cell together in a kind of circular shape

Each body is formed of a ring of skin-type cells which allow the cell to use water jets to move around. Inside the body is where most life processes occur.

Every other type of cell floats around inside the body bouncing around and attaching to hormones and glucose.

Here are some other looks:

*   Non-biologicial matter appears as small uniform circles of differing colours. 
*   Enzymes appear as black pill-shaped objects in the map with a “little” fill of colour based on functon.
*   Hormones appear as little fan shapes and have a “little” fill of colour based of message type, alongside coloured circular tips depending on message.
*   Cells appear as almost-circular blobs on the map, with many “arms" representing antigens
*   Viruses appear as tiny desert yellow circles with a singular “arm” representing antigens

Logic
-----

### Food and waste logic

Food sometimes appears in quick materialisations in the liquid and slowly floats around. These materialisations have about 100 foods inside and slowly bounce around. Bodies attempt to move to the foods.

Waste comes out of bodies and falls to the “floor” of the simulation, where it is slowly absorbed and taken away.

### Bodies

The simulator, as previously mentioned, uses “bags” of cells to define a body.

Each body's cells work hard to keep it alive. 

#### Basic life

The muscles provide locomotion for the body, allowing it to move around the environment and go towards the mean centre of food. 

As they go there, mitochondria cells will take the foods and give it to the “pocketed” enzymes to turn into glucoses, which can then float around the body and provide power to it. 

Each of the skin-type cells have two sides: the hardy outside side with few antigens and no acceptance to hormones, and the inside side with tons of antigens and acceptance to hormones.

These enzymes “denature” after a set amount of uses, meaning that enzyme-maker cells need to exist. These turn glucose into enzymes based off hormone commands which are added to a queues. If there is no hormone command in the queue, the enzyme-maker generates random enzymes. for each glucose particle.  

Hormones are the messages given by the body. There are two types: “request” and “response” hormones. “request” hormones go to a neuron cell and request the production of x hormone. The neuron cell can then choose to ignore or “respond” to the hormone based off “request” hormones.

This system allows for neurons to control the cell based off their model of the requests. Every other cell will request x hormone as their resources dwindle.

#### More advanced life

Stem cells also exist to help reproduce cells and make the body bigger as needed. These respond to response reproduction hormones. Once they recieve a response hormone, they take glucoses and split before transforming one of the cells to the required type. The type is then taken to where it is needed, either by expanding the wall or floating around

Hormones are formed of their “category”, which tells us what the message is about, and their “message”, which tells us what activity to perform. These can be though of as abstract representations of proteins.

Whenever a cell takes glucose, it will return a “dangerous waste”. These must then be turned into regular waste by enzymes and then they can be passed through the muscles and out into the environment.

Inside the cell, there is “DNA”, which, during a reproduction event, might mutate and change the antigen or increase energy efficency or myriad things. The DNA is the same for all cells.

#### Infections

Infections occur when a virus manages to bond to a muscle antigen and enter the muscle. Once inside, the virus can then eat the insides to reproduce. Viruses are extremely small, so tons of them can exist in a cell.

Once inside, the muscle is filled with hundreds of viruses before it explodes violently, launching viruses in every direction. Hopefully, parts of the body can recover due to differing antigens.

If a virus touches an immune cell, the immune cell will then:

*   Absorb the virus and analyse it for a few seconds
*   Release enzymes made to dissolve that virus 
*   Release hormones to change the antigen of all cells
*   Release antibodies which can clump virus cells together and make it more difficult for them to come into a cell

If this occurs quickly enough, then the body might be able to recover.

#### Reproduction

Bodies reproduce when they become too large to allow efficent glucose transfer. 

When a body reproduces, the cloner hormone comes out and turns the stem cells into mini factories. First, they form a large clump of stem cells. These then open up to form a cavity where the stem cells inside differentiate. Over the course of 5 minutes, the clump expands as hormones fly around and a slightly altered body comes out, following the exact porpotions of cells needed. Part of the oversized original body is dissolved into glucose to make this possible.

In the end of develeopment, eventually both of the seperate body walls will touch each other on opposing sides and then the clump is “ready”. The clump is surrounded by a membrane which allows it to pass the muscles in a sterile way before the clump leaves and the membrane turns into waste. Once the body leaves, it is independent of its parent. It has a slight antigen change as well, which makes it incompatible with glucose.

#### Death and Recovery

Every cell has a "life counter”. This counter counts down slowly to 0 every second without glucose or for every time a Dangerous Waste particle hits it. The lower the counter, the browner the cell. If glucose does not arrive quickly enough, the cell turns black and will eventually explode into a cloud of Dangerous Waste particles. If the cell is a muscle, it is sure about half of the waste will leave the cell. If the cell is internal, the whole body may be damaged. 

To recover a skin-type cell, the body will “suck together” two barrier cells to close off the cell. If this is not possible due to great distance, it is possible components of the cell will leak out. This means that each of the cells will die as they lose proximity to other cells. This results in widespread death of a body.

#### More antigen mutation logic

As a antigen mutates, it still retains the old form, but the old form may only recieve hormones and the rare glucose. The new form accepts anything.

### Body Spawning

Obviously, bodies do need to come into existence.

Bodies will spawn either on user input or on program start.

When a body spawns:

*   It will have the correct propotion of cells
*   It will have 5 glucose molecules per cell within the body
*   It will begin executing its functions immediately

User Input
----------

The user is able to do various things. They can:

*   Place a random virus cluster on the map with a random antigen selected from one random muscle cell.
*   Drag bodies around the map
*   Place food on the map
*   Zoom in and out or pan around the map
*   Spawn a body to a maximumm of three on the map 

Programming specifics
---------------------

### Standards

Each of the types and their individual objects will have their own new class made.

The PDE file must only be used to process input and output; all logic and such must be part of a seperate file.

The git repo must use gitmoji and the commits must be specific.

The first version uploaded must be able to:

*   Render bodies and all their cells properly
*   Spawn food
*   Allow bodies to move around

### Co-ordinate system

The co-ordinate system is quite simple. Body co-ordinates are defined as the center of their circle relative to the pond.

Cell co-ordinates are relative to their position relative to the cell system.

Alongside this, cells and bodies also have a momentum. This is defined as a vector of movement per tick. 

Momentum is lost according to the friction co-efficent of the water. 

### Collision handling

Oh no! This is actually quite difficult. 

Erm… each cell type has a radius as defined by its max glucose value divided by 100. This can be used for checking if distance is ok for objects.

If a collision occurs, there is a standard density to figure out how much each object moves as a result. 

Ticks
-----

20 ticks a second. Ticks every 50 ms. 

Each tick:

*   The co-ordinates change by momentum, which is the vector to move
*   Momentum is lost reverse logarithmically due to water friction for all cells.
*   Momentum, if at 0 for a long period, increases a little to represent turbulence
*   If anything is “touching” a cell, check if they're ok to enter… somehow
*   Change values as neccesary (glucose down by 1,