# Maths system
Introduction
------------

This new system is designed to make maths operators intuitive. Every operator here is formed of multiple morphemes. Morphemes can be combined in several ways and make it simple to come up with new operators. The entire system is in SVO order normally.

Every operator is a "verb" and they can accept multiple inputs.

Arithmetic
----------

For example, let's see how we'd replace +, \*, ^, and higher. First, we specify the level of which the operator functions. That is 1, 2, and 3 respectively. And then we add the "put-together" morpheme, <, and that specifies we put things together:

`2*2 = 2,2<,2`

`4^3 = 4,3<,3`

Now, if we wanted to do the logical opposite, -, /, sqrt(), and others, we can just add a negation:

`4-7 = 4,1< -,7`

`sqrt(4) = 2,3< -,4`

But there's a secret! It turns out that some things are "default values" and do not need to be specified. For example, 1 and < are both default values, so this would be perfectly valid:

`2+43 = 2,,43`

And so would this:

`4-2 = 3,-,2`

Commas and Brackets
-------------------

The commas are optional and can be replaced with spaces for style (but spaces must never be moved as in some cases they create ambiguity):

`400-230 = 400 - 230`

Brackets are perfectly valid as order-specifiers:

`(400*2)+3 = (400,2,2),,3`

Decimals and Negative numbers
-----------------------------

So, let's try some other stuff. what about negative numbers and decimals? There is a "pure" way to do that; just multiply by 10 by a negative number.

To make negative numbers, just use the negative form with no object:

`400 -`

Since there's no object, the subject can go anywhere:

`- 400`

So, to make 0.4, we can do.

`4,2<,(10,3<,(- 1))`

Assignment
----------

Now, let's move onto some assignments.

Assignment will always act on a "letter". The letter is assumed to be the output given no valid letter or assignment operator. Assignment uses =, and we have several degrees of accuracy, from high (', equals) to mid (;, approximately), low (., does not equal). We can also specify a magnitude using < and - like we did with arithmetic, alongside our accuracy degrees.

`3 = 3 - > 3,=',3`

`7 ~ 6.9 - > 7,=;,(69,2<,(10,3<,(- 1)))`

`4 > 3 - > 4,='<,3`

`6 <= x - > 6,=;,x`

A "letter" is just a place to store data; they are encased in the following:

*   .a. - Reusable macro that acts on data
*   'a' - Some data is stored inside and is retrieved whenever it is the subject (but will not do anything to data)
*   "a" - Fragment of text; can be used anywhere and will automatically be ignored.

Macros
------

Now, let's look into simplifying stuff. We can make macros, which are like functions. To define a macro, we make a letter (a) and surround it in the macro syntax ( .a. )

For example, to make our very own exponent operator, we can do:

`{$,2<,(10,3<,($))} ,=', $1.e.$2`

Note how the dollar sign acts as an "argument placeholder" The numbers are assigned in order from left to right, with the next one to appear regardless of brackets is given a number.

Once we have made a macro, it becomes our own little operator. For example, we can define 6.2 as:

`62.e.(- 1)`

But what if we want to shorten it even more? We can do overloading to allow us to specify different things given different arguments.

```text-plain
{$,2<,(10,3<,($))} ,=', $1.e.$2
{$,2<,(10,3<,(- 1))} ,=', $1.e.
```

So now, if argument two is not suppled, we see if the next definition exists and accepts our arguments.

`62.e.`

Iteration
---------

Now then, let's try some iteration. We just need to supply a set.

`{62, 43, 342, 24, 67}.e.`

Here, the macro is now run for everything, giving us 6.2, 4.3, 34.2, 2.4, and 6.7 respectively.

But what if we wanted different exponents? We use different brackets. There are two main types; strict-matching, which is a list of arguments for each argument in the first set, and re-doing, which loops over the strict-matched set for each element.

See this strict matching on both sides

`{23, 43, 99}.e.{(- 1), 2, 4}`

This gives out 2.3, 430, and 99,000 respectively.

And non-strict matching here:

`{23, 43, 99}.e.[(- 1), 2, 4]`

Here are the results::

2.3, 4.3, 9.9, 230, 430, 990, 23,000, 43,000, 99,000.

Note how the re-doing works here; for each element in the square brackets ( \[\] ), the first set is iterated over. This is incredibly powerful.

Influences from Linguistics and Programming
-------------------------------------------

The programmers and linguists among you may now already be seeing some similarities with linguistics and programming. This is true; programming and linguistics were massive influences.

First of all, this system is designed using a system of logical meaning units, like languages and some programming languages are designed. This is a natural result of a linguist making a mathematical system. It also makes sense.

Also, iteration and defining macros also comes from a cross between maths and programming.

### Tangent: Editors and interpreters

Seeing as this maths notation system shares a lot in combination witn programming, i definately feel that an editor would make it a lot easier to use, from brackets going smaller in reverse porpotion to the nesting depth to colouring things up.

Interpreters are also an intresting idea, but one must remember one thing: treat it like parsing calculator expressions, not a programming language!

Input and ouput should be done through the usage of assignments, either without a subject or operator.

Whitespace should be ignored. The language is designed to be usable without whitespace.

Standard Library
----------------

Just like a programming language has a standard library to prevent endless pain, so does this. Just copy in the relevant macros and you can now use higher-level functions. For example, one might copy in this wrapper function for multiplication.:

`($,2<,$),=',$1.*.$2`

I must stress, however, that the standard library must only be used when neccesary; students and learners of operations must use the original system for a long time before moving onto this.

Reference
---------

Note: x is used as a placeholder value

| Morpehme | Meaning | Default? |
| --- | --- | --- |
| ,x, | A verb | Yes |
| .x. | A macro |     |
| “x” | A comment |     |
| ‘x’ | A value |     |
| <   | Defines “put-together” or some form of non-negative thing | Yes |
| \-  | Negates anything |     |
| (x) | Shows an expression that must be calculated before its surroundings |     |
| $   | A placeholder value, for either a variable provided an argument, or a location to place an argument |     |