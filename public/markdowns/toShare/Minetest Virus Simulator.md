# Minetest Virus Simulator
Introduction
------------

I have been told to make a virus simulator for CS, so here we are.

I'm planning to get it finished by today.

Specifics
---------

### Spread

The infection spreads VERY easily. However, it requires humans are close together or share spaces.

The infection appears physically as little, tiny dots on the floor. They are only visible with close inspection. As soon as a player touches them, they stand a 25% ±10 chance of contracting the disease. The dots will eventually decay after 74 real uptime hours of continuous existence. 

Once a player contracts the disease, they are forced to suffer it for 2-10 hours of playtime. The longer the infection, the weaker it is to allow the player to live longer.

The player will constantly be sending dots everywhere; it's there choice whether or not they wish to infect others. 

### Natural propogation

Dots will naturally appear with a 0.1% chance around someone with no infection. This means diseases will constantly appear.

### Infections

When you get infected, by default you start

Symptoms vary by strain:

*   Loud coughing annoying the player and spreading TONS of dots EVERYWHERE.
*   Random drops of inventory items after shaking the camera
*   Slow health ablation
*   Neausa, resulting in random movement to the player
*   Sensitivity, resuting in health loss for hitting blocks

### Prevention

Infection is very difficult to remove from surfaces by design. They cannot be mined, instead saying “Whether you were trying to get rid of a block or a virus, your tool slips and you cannot get the damn thing!”.

The best prevention is quarantining infected players. There are also “antiviral sprays”, which will kill up to 99% of dots. However, the dots that do survive will no longer work with the antiviral spray.

Antiviral sprays can be crafted specific to a strain by using a cotton swab and using that with an existing antiviral spray in the “lab machine”.

Masks can be worn on the head. They decay quickly and can become expensive as their amount increases. However, chance of being infected drops to 1%.

The most effective prevention, however, is social distancing; if players keep to their own places in the map, nothing spreads.

### Mutation

Each virus belongs to a certain strain. Every strain has certain variables and a colour. Some strains will die quickly, others won't. For example, one infection may have a high chance of dot propogation but low decay time, and another a low chance but higher decay times. Whichever infects will inflence future viruses.

Some dots, with a 10% chance, will randomly “step” one of their variables. They can then conserve it and use it for themselves.

Strains are classified by their “name”, which is a random base-64 string within the dot.

Programming
-----------

### Data storage

Data is stored in world files, containing what each name's hex-value means, alongside other things.

### Todo

See [Todo List](Minetest%20Virus%20Simulator/Todo%20List.md).