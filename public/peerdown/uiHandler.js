var delayTimer

$("#markdown-entry").on("input", function () {
    markdownSource = $(this).val()

    $("#output").html(renderMarkdown(markdownSource))

    // 500 ms timeout for other client's sakes
    clearTimeout(delayTimer);
    delayTimer = setTimeout(function() {
        publishMarkdown(markdownSource)
        throwPing()
    }, 500);
})

$("#copy").click(() => {
    copyToClipboard(window.location.href)
})

$("#username").on("input", function () {
    pastName = me.username
    username = $(this).val()

    putInURL("username", username) // FIXME: A hack for easier testing!

    me.username = username

    // TODO: Do something with the username

    // Look, i will, past 56i. Just not here ok? Ok, maybe just an API call.

    serverMessage = {
        type: "usernameChange",
        data: {
            "past": pastName,
            "now": me.username
        }
    }

    me.messageQueue.push(JSON.stringify(serverMessage))
})

$("#id").on("change", function () {
    me.isNew = true
    beClient($(this).val())
})

// Draw crap
function writePeerId(){
    $("#id").val(me.peerId)
    //putInURL("peerId", me.peerId) // Commented out as it becomes annoying to test.

    $("#url").text(window.location.href)
}

function handleChanges(documentArray) {
    changeElement = $("#changeMonitor")

    for (i=0; i<documentArray.length; i++) {
        if (i > 0) { // This is probably a bad hack. But whatever.
            let divId = "changeMonitor-edit-" + i

            changeElement.prepend(
                $("<div>")
                    .attr("id", divId)
                    .attr("class", "row")
                    .css({
                        "min-height":"10vh",
                        "min-width":"75vw",
                    })
                    .append(
                        $("<p>").text(documentArray[i].nickname + " changed:")
                    )
                )

            var dmp = new diff_match_patch();

            var text1 = documentArray[i-1].markdown;
            var text2 = documentArray[i].markdown;
            
            var d = dmp.diff_main(text1, text2);
            
            dmp.diff_cleanupSemantic(d);

            var ds = dmp.diff_prettyHtml(d);
            
            console.log(ds)
            console.log(dmp)

            $("#" + divId).append($("<div>").html(ds));
        }
    }

    $("#markdown-entry").val(documentArray[documentArray.length-1].markdown)
    renderMarkdown($("#markdown-entry").val())
}

function renderUsers(userList){
    element = $("#userList")
    element.empty()

    list = $("<ul>")

    for (i=0; i<userList.length(); i++){
        list.append(
            $("<li>").text(userList[i])
        )
    }

    element.append(list)
}
