// Controls the memory view

function writeTable(memory){
    table = $("<table>").addClass("table")

    const width = 15
    
    type = queryStates()[3].replace("btn-", "")

    console.log(type)

    for (i=0; i < memory.length; i++) {
        if (i % width == 0) {
            row = $("<tr>")
            table.append(row)
        }

        var value = memory[i]

        switch (type) {
            case "hex":
                value = value.toString(16)
                break
            case "bin":
                value = value.toString(2)
                break
            case "dec":
                value = value.toString(10)
                break
            case "ascii":
                value = String.fromCharCode(value)
                break
        }

        row.append(
            $("<td>").text(value)
            )
    }

    $("#memories").append(table)
}

function initialiseMemory() {
    $("#memories").empty()

    tape = new Uint8Array(300)

    tape[0] = 1
    tape[1] = 15
    tape[2] = 200

    writeTable(tape)
}

function updateMemory(memory) {
    // Takes in a array of integers and will portray it
    $("#memories").empty()

    writeTable(memory)
}