$(document).ready(function() {
    function compile() {
        clicked = queryStates()

        useOriginalOnly = (clicked[0] == "btn-scheme-original") ? true : false;
        flow = clicked[1].replace("btn-mem-", "")

        code = $("#code").val()

        checkMatchyNess(code, useOriginalOnly)

        if (! useOriginalOnly) {
            code = processMine(code)
        }

        $("#outputArea").text("Code now running (it's very slow!)")
        $("#run").attr("disabled", true);

        interpretMain(code)
    }

    $("#btn-run")
        .click(() => {
            compile()
        })

    initialiseMemory()
})