[Verse 1]
I don't fuck with love
I don't have the time
I only fuck.

[Verse 2]
I don't fuck with the big gamble
I don't fuck with the risks
I only talk

[Verse 3]
And i'm not going to see it
And i'm not going to meet it
And i'm not going to have it
I ignore.

[Verse 4]
It might be sitting in front of me
It might be looking for me
It might be taunting me
I look the other way.

[Bridge]
Late-night texts
Deepest desires
New feelings

[Verse 6]
And i watch from a distance
But the butterflies are right next to them
And i get pulled in

[Verse 7]
Small talk
And i'm there
At the centre

[Bridge]
But they don't align
But they don't want it
So i stop caring

