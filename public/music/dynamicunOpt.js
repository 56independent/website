function makeCard(cardInfo, parent) {
    quality = cardInfo.quality
    badgeType = quality == "excellent" ? "success" : quality == "very good" ? "info" : quality == "good" ? "dark" : quality == "ok" ? "warning" : "";

    console.log(quality, badgeType)

    let card = $("<div>")
        .addClass("card project")
        .append(
            $("<div>")
            .addClass("card-body")
            .append(
                $("<h5>")
                    .addClass("card-title")
                    .html(cardInfo.title),
                quality ? $("<span>").attr("class", "badge bg-" + badgeType).text("Rated class-" + quality) : $("<p>"),
                $("<p>")
                    .addClass("card-text")
                    .html(cardInfo.description),
            )
            .children()
            .addClass("m-2")
        );

    if (cardInfo.albumart.length != 0){
        card.append($("<img>")
            .attr("src", "album covers/" + cardInfo.albumart)
            .attr("width", "75%")
            .attr("class", "mt-auto"))
    }

    if (! $("#wav").is(":checked")) {
        useSmall = true
    } else {
        useSmall = false
    }

    for (let i = 0; i < cardInfo.filenamesBig.length; i++) {
        
        let filename = (useSmall) ? cardInfo.filenamesSmall[i] : cardInfo.filenamesBig[i]
    
        buttonId = filename.replace(/[\s\./]/g, "-") + "-button"

        card.append($("<button>")
            .attr("id", buttonId)
            .attr("class", "btn-primary" + (cardInfo.albumart.length == 0 ? " mt-auto" : ""))
            .text("Play: " + filename).click(() =>{
                mainType = "audio"

                if (filename.match(/\.(.+)/)[1] == "mp4"){
                    mainType = "video"
                }

                card.append($(`
                <code>${filename}</code>
                <${mainType} controls width="75%" autoplay>
                    <source src="${filename}" type="${mainType}/${filename.match(/\.([^.]+)$/)[1]}">
                    Your browser does not support the multimedia element.
                </audio>
                `));

                $("#" + buttonId).prop("disabled", true)
        }))
    }

    parent.append(card);
}


function optimised() {return false}
