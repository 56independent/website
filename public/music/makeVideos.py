'''
Here are requirements:

* Start by making an image, and then extend it over the length of the piece in a video.
* Use Python, imagemagick for making the image, and ffmpeg for making it a video. Use the CLI for these applications through python for future maintainability
* Seperate the JSON-reading and video-making components for future extensibility should the JSON become deprecated.
* Read from a JSON object stored in a JS file
* Make a 16:9 video, 1920*1080, the length of a wav file 
* If given the album covers (contained in the `album covers` directory), open it and make it occupy the entire left region as a 9:9 square, otherwise insert placeholder art as according to a file path stored in a variable at the top of the file which may be modified.
* If given multiple sound files for the same object, keep the image the same but insert a 5-second gap between them, in the video.
* Given the title, put it centred atop the 7:9 square on the right
* Given the description, put it below the title and with a smaller font. Ensure the description fits.
* Keep everything on a black background with white text
* Give all text a reasonable margin from the edges

And these functions:
* `ReadJSON` - Responsible for taking the JSON (taken from the `queue.json` file containing pieces to process) and converting it into what i need as a 2D array.
* `MakeImage` - Turns some of the data into an image to be processed later, stored in `videos/images`
* `MakeVideo` - Turns the image into the final video given the filename
* `Mainloop` - Handles looping over the arry and organising each video
'''

import sys
import os
import json
import subprocess
import textwrap

def checkFiles(fileListBig, fileListSmall):
    ok = False

    smallvalid = False
    bigvalid = True

    for filename in fileListSmall:
        if not os.path.isfile(filename):
            smallvalid = False
        elif filename == fileListSmall[1:]:
            smallvalid = True

    for filename in fileListBig:
        if not os.path.isfile(filename):
            bigvalid = False
        elif filename == fileListBig[1:]:
            bigvalid = True
    
    if bigvalid:
        return fileListBig
    elif smallvalid:
        return fileListSmall
    else:
        return fileListBig if len(fileListBig) > len(fileListSmall) else fileListSmall
        

def readJSON(contents):
    projects = json.loads(contents)
    pieces = projects["projects"]
    
    array = [] # Data: title, description, cover, filenames[]
    
    indexer = 0
    
    for piece in pieces:
        currentObj = pieces[indexer]
        
        print(currentObj)
        
        files = checkFiles(currentObj["filenamesBig"], currentObj["filenamesSmall"])
    
        array.append([currentObj["title"], currentObj["description"], currentObj["albumart"], files])
    
        indexer += 1
    
    print(array)
    return array    

def makeImage(title, description, cover):
    if os.path.isfile("album covers/" + cover):
        cover = "album covers/" + cover 
    else:
        cover = "new-series2.png"
    
    descriptionClean = textwrap.fill(description, 80).replace('"', '\\"')
    
    command = "convert -size 1920x1080 xc:black \( \"" + cover + "\" -resize 1080x1080 \) -geometry +0+0 -composite -fill white -gravity northwest -pointsize 80 -annotate +1100+10 \"" + title + "\" -fill white -gravity northwest -pointsize 20 -annotate +1100+150 \"" + descriptionClean + "\" \"videos/images/" + title + ".jpg\""
    
    print(command)
    
    try:
        subprocess.run(command, shell=True, check=True)
    except subprocess.CalledProcessError as e:
        print(f"Error: {e}")

def makeVideo(image, files):
    print(files)
    for filename in files:
        print(filename)
        #if os.path.isfile(filename):
        if True:
            start_index = filename.rfind("/") + 1  # Find the last occurrence of '/'
            end_index = filename.find(".", start_index)  # Find the first occurrence of '.' after the last '/'
            videoName = filename[start_index:end_index]
        
            #command = "ffmpeg -framerate 25 -i 'videos/images/" + image + "' -i '" + filename + "' -c:v libvpx-vp9 -c:a libopus -b:v 1M -b:a 192k 'videos/" + videoName + ".webm'"
            #command = "ffmpeg -loop 1 -framerate 1 -i 'videos/images/" + image + "' -i " + filename + " -map 0 -map 1:a -c:v libx264 -preset ultrafast -tune stillimage -vf fps=10,format=yuv420p -c:a aac -shortest 'videos/" + videoName + ".mp4'"
            command = f'ffmpeg -loop 1 -framerate 1 -i "videos/images/{image}" -i "{filename}" -map 0 -map 1:a -c:v libx264 -preset ultrafast -tune stillimage -vf fps=10,format=yuv420p -c:a aac -shortest "videos/{videoName}.mp4"'


            print(command)
            
            try:
                subprocess.run(command, shell=True, check=True)
            except subprocess.CalledProcessError as e:
                print(f"Error: {e}")


def mainloop(pieces):
    for i in range(len(pieces)):
        makeImage(pieces[i][0], pieces[i][1], pieces[i][2])
        print(pieces[i][0])
        makeVideo(pieces[i][0] + ".jpg", pieces[i][3])
    
thing = sys.argv[1]

print(thing)

if "{" in thing and "}" in thing: # This is to check if it contains raw JSON
    data = thing
else:
    with open(thing) as jsons:
        data = jsons.read()

array = readJSON(data)

mainloop(array)

