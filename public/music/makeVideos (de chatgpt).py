import json
import os
import subprocess

def create_video(project):
    # Check if the files in the array exist
    if not all(os.path.exists(file_path) for file_path in project['filenamesBig']):
        print(f"Files do not exist for project: {project['title']}")
        return

    # Image creation using ImageMagick
    image_path = f"output_image_{project['title']}.png"
    title = project['title'].replace(' ', '_')  # Replace spaces in the title with underscores
    max_text_width = 1200  # Set your desired maximum text width
    cmd_image = f"convert -size 1920x1080 xc:black -gravity center -fill white -font Arial -pointsize 40 -size {max_text_width}x -annotate +0+0 '{title}' -annotate +0+50 '{project['description']}' '{image_path}'"
    subprocess.run(cmd_image, shell=True)

    # Video creation using FFmpeg
    video_path = f"output_video_{title}.mp4"
    cmd_video = f"ffmpeg -loop 1 -i '{image_path}' -i '{project['filenamesBig'][0]}' -t {get_audio_duration(project['filenamesBig'][0])} -filter_complex '[0:v][1:a]concat=n=2:v=1:a=1[outv][outa]' -map '[outv]' -map '[outa]' -s 1920x1080 '{video_path}'"
    subprocess.run(cmd_video, shell=True)

    # Remove temporary image file
    os.remove(image_path)


def get_audio_duration(filename):
    # Get audio duration using FFmpeg
    cmd_duration = f"ffmpeg -i '{filename}' -f null -"
    result = subprocess.run(cmd_duration, stderr=subprocess.PIPE, text=True, shell=True)
    duration_line = [line for line in result.stderr.split('\n') if 'Duration' in line]
    if duration_line:
        duration_str = duration_line[0].split(', ')[0].split('Duration: ')[1]
        total_seconds = sum(float(x) * 60 ** i for i, x in enumerate(reversed(duration_str.split(":"))))
        return total_seconds
    return 0

def main():
    # Read JSON from music.json
    with open('music.json', 'r') as file:
        data = json.load(file)
        projects = data['projects']

    # Process each project
    for project in projects:
        create_video(project)

if __name__ == "__main__":
    main()

