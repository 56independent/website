// This code is avauilanleableatleable under the MIT license and all text is under CC-SA
function run() {

    /* {
            "title":"",
            "description":"",
            "albumart":"general.png",
            "quality":"",
            "filenamesBig":[""],
            "filenamesSmall":[""]
        },*/

    let projects = [
        {
            "title":"Death is your deadline.",
            "description":"We're dancing the boundary and we're enjoying it. Are you? We should enjoy it. Without you, it breaks the whole point.",
            "albumart":"general.png",
            "quality":"very good",
            "filenamesBig":["finished/Death is your deadline..mp3"],
            "filenamesSmall":["finished/Death is your deadline..mp3"]
        },
        {
            "title":"(fear is sometimes a very good thing)",
            "description":"How else would you know to stay away from me?",
            "albumart":"general.png",
            "quality":"very good",
            "filenamesBig":["finished/(fear is sometimes a very good thing).mp3"],
            "filenamesSmall":["finished/(fear is sometimes a very good thing).mp3"]
        },
        {
            "title":"Humo loco",
            "description":"A crazy-dark piece that maintains the same hazy darkness all the way through with plenty of change seen just past the crazy darkness. Go underwater at 02:20 to clean yourself of the murder of my other pieces.",
            "albumart":"general.png",
            "quality":"very good",
            "filenamesBig":["finished/humo loco.mp3"],
            "filenamesSmall":["finished/humo loco.mp3"]
        },
        {
            "title":"They know, don't they?",
            "description":"I'm bad at keeping secrets. They know what i'm trying to do for a living and all the gory details. Fuck this.",
            "albumart":"general.png",
            "quality":"very good",
            "filenamesBig":["finished/they know, don't they.mp3"],
            "filenamesSmall":["finished/they know, don't they.mp3"]
        },
        {
            "title":"The crushing job",
            "description":"Welcome to your job at the curshing industry! This is only the start of something much bigger then you! ou!",
            "albumart":"general.png",
            "quality":"very good",
            "filenamesBig":["finished/crushing job.mp3"],
            "filenamesSmall":["finished/crushing job.mp3"]
        },
        {
            "title":"200 damned cups of coffee!!!!!",
            "description":"Your heart beats too fast for you to live no longer. You're dead! Dead! Dead as a doornail! Dead! Dead! Dead as a doornail double-dipped in double-strength hydrochloric acid and incinerated and melted down into both the euro and the pound and the dollar all other earthly currency then frozen and transported across death valley unprotected and rekilled and rekiled and ground up for dust to kill other doornails!",
            "albumart":"general.png",
            "quality":"very good",
            "filenamesBig":["finished/200 damned cups of coffee!!!!!.mp3"],
            "filenamesSmall":["finished/200 damned cups of coffee!!!!!.mp3"]
        },
        {
            "title":"You're crushing orphans",
            "description":"Welcome to your new job! You're crushing orphans! Let's go for a tour through the factory; see the machines (02:50)? Frolic in the fields (3:43)! Picnic (5:24)! Ok, to work (5:36)! Isn't it enchanting (5:54)? What do you mean its unethical?",
            "albumart":"general.png",
            "quality":"very good",
            "filenamesBig":["finished/You're crushing orphans.mp3"],
            "filenamesSmall":["finished/You're crushing orphans.mp3"]
        },
        {
            "title":"Brain-feelingsssssssssssss",
            "description":"This file is very large (90 mb). It offers a much harsher soundscape as compared to far, far wider (below).",
            "albumart":"general.png",
            "quality":"good",
            "filenamesBig":["finished/brainfeelingsssssssssssssss.mp3"],
            "filenamesSmall":["finished/brainfeelingsssssssssssssss.mp3"]
        },
        {
            "title":"V - [Ending]",
            "description":"Microtonals barrier piece V (as remixed from off-posed)",
            "albumart":"microtonals.png",
            "quality":"excellent",
            "filenamesBig":["finished/V - Ending.wav"],
            "filenamesSmall":["finished/V - Ending.mp3"]
        },
        {
            "title":"IV - [Dance]",
            "description":"Microtonals barrier piece IV (as remixed from off-posed)",
            "albumart":"microtonals.png",
            "quality":"excellent",
            "filenamesBig":["finished/IV - Dance.wav"],
            "filenamesSmall":["finished/IV - Dance.mp3"]
        },
        {
            "title":"III - [Transitions]",
            "description":"Microtonals barrier piece III",
            "albumart":"microtonals.png",
            "quality":"excellent",
            "filenamesBig":["finished/III - Transitions.wav"],
            "filenamesSmall":["finished/III - Transitions.mp3"]
        },
        {
            "title":"II - [Stories]",
            "description":"Microtonals barrier piece II (as remixed from saviest)",
            "albumart":"microtonals.png",
            "quality":"excellent",
            "filenamesBig":["finished/II - Stories.wav"],
            "filenamesSmall":["finished/II - Stories.mp3"]
        },
        {
            "title":"I - [General Vibes]",
            "description":"Microtonals barrier piece I (as remixed from saviest)",
            "albumart":"microtonals.png",
            "quality":"excellent",
            "filenamesBig":["finished/I - General Vibes.wav"],
            "filenamesSmall":["finished/I - General Vibes.mp3"]
        },
        {
            "title":"Y que?! (DROWNING DURING A NIGHT SWIM HE'S PEEING -- NEAT)",
            "description":"And so what? I'm happy and naïve and i don't want that ever to change (DROWNING DURING A NIGHT SWIM HE'S PEEING NEAT)",
            "albumart":"general.png",
            "quality":"excellent",
            "filenamesBig":["finished/y que.mp3"],
            "filenamesSmall":["finished/y que.mp3"]
        },
        {
            "title":"Brain-feelings",
            "description":"Please listen to what my brain feels like sometimes all the time, other then that, listen to what my freelancing \"job\" sounds like (it sounds like knives and screams for help), not that you have to do anything, i just like being edgy sometimes, like right now, ahahahaahahaaahahhha whatever lol fuck.",
            "albumart":"general.png",
            "quality":"excellent",
            "filenamesBig":["finished/brainfeelings.mp3"],
            "filenamesSmall":["finished/brainfeelings.mp3"]
        },
        {
            "title":"Loquísimo",
            "description":"The knocking and the noise is annoying and drives me insane please shut up ahahahahahahahahahahhahaahhahahahahahaaaahahahahhahahahahahah i'm so awkward, damn!",
            "albumart":"general.png",
            "quality":"very good",
            "filenamesBig":["finished/loquísimo.mp3"],
            "filenamesSmall":["finished/loquísimo.mp3"]
        },
        {
"title":"Tear",
"description":"Begin the fall and watch the horror of realising derealisation fall onto you.<br><br><a href=\"lyrics/emv/tear.txt\">Lyrics</a>",
"albumart":"EMV.png",
"quality":"excellent",
"filenamesBig":["elmeuvida/tear.mp3",],
"filenamesSmall":["elmeuvida/tear.mp3",]
},{
"title":"La Caida Petita",
"description":"Descend into pure madness. Good luck!<br><br><a href=\"lyrics/emv/caida petita.txt\">Lyrics</a>",
"albumart":"EMV.png",
"quality":"excellent",
"filenamesBig":["elmeuvida/caida petita gran.mp3",],
"filenamesSmall":["elmeuvida/caida petita gran.mp3",]
},{
"title":"The Alarm",
"description":"Wake up! Reality calls!",
"albumart":"EMV.png",
"quality":"excellent",
"filenamesBig":["elmeuvida/the alarm.mp3",],
"filenamesSmall":["elmeuvida/the alarm.mp3",]
},{
"title":"Lifesick",
"description":"What does it feel like to take a holiday whilst not in this world?<br><br><a href=\"lyrics/emv/lifesick.txt\">Lyrics</a>",
"albumart":"EMV.png",
"quality":"excellent",
"filenamesBig":["elmeuvida/lifesick.mp3",],
"filenamesSmall":["elmeuvida/lifesick.mp3",]
},{
"title":"Jan Kama Ala",
"description":"No, he's not coming.<br><br><a href=\"lyrics/emv/jan kama ala.txt\">Lyrics</a>",
"albumart":"EMV.png",
"quality":"excellent",
"filenamesBig":["elmeuvida/jan kama ala.mp3",],
"filenamesSmall":["elmeuvida/jan kama ala.mp3",]
},{
"title":"Caer Por la Caida Grandisimo",
"description":"This took way too long to render with all the FX i put into the music. Feel the brutal grind of reality.<br><br><a href=\"lyrics/emv/caer por la caída grandísimo.txt\">Lyrics</a>",
"albumart":"EMV.png",
"quality":"excellent",
"filenamesBig":["elmeuvida/caer por la caída grandísimo.mp3",],
"filenamesSmall":["elmeuvida/caer por la caída grandísimo.mp3",]
},{
"title":"¿Y si levántese?",
"description":"And what if i do wake up? I'll fall again. There's no point.<br><br><a href=\"lyrics/emv/Y si levántese.txt\">Lyrics</a>",
"albumart":"EMV.png",
"quality":"excellent",
"filenamesBig":["elmeuvida/Y si levántese.mp3"],
"filenamesSmall":["elmeuvida/Y si levántese.mp3"]
},
        /*{
            "title":"El Meu Vida",
            "description":"An album diving into depersonalisation with the most horrific soundscapes possible.",
            "albumart":"EMV.png",
            "quality":"excellent",
            "filenamesBig":[
            "elmeuvida/tear.mp3",
"elmeuvida/caida petita gran.mp3",
"elmeuvida/the alarm.mp3",
"elmeuvida/lifesick.mp3",
"elmeuvida/jan kama ala.mp3",
"elmeuvida/caer por la caída grandísimo.mp3",
"elmeuvida/¿Y si levántese?.mp3"],
            "filenamesSmall":[
            "elmeuvida/tear.mp3",
"elmeuvida/caida petita gran.mp3",
"elmeuvida/the alarm.mp3",
"elmeuvida/lifesick.mp3",
"elmeuvida/jan kama ala.mp3",
"elmeuvida/caer por la caída grandísimo.mp3",
"elmeuvida/¿Y si levántese?.mp3"]
        },*/
        {
            "title":"[Sample] The most terrifying piece of <i>El Meu Vida</i>",
            "description":"I'm working on my new album and accidentally created this little piece here. It's nightmare fuel, at least to me. It's deigned to help you feel what dereleasiation sometimes makes you realise about the world, or at least, what you think you realised. Enjoy true fear wash over you as the mangled screams, voices, and cries for help wash over you 🤗🤗🤗",
            "albumart":"el meu vida sample.png",
            "quality":"excellent",
            "filenamesBig":["finished/caida petita.mp3"],
            "filenamesSmall":["finished/caida petita.mp3"]
        },
        {
            "title":"Far, far, far, far, far wider",
            "description":"An ambient track consisting of an hour and eleven minutes of slow, dreary crying. A paulstretched version (20*) of a song i made. Listen to the whistling harmonics before the screams arrive.",
            "albumart":"microtonals.png",
            "quality":"good",
            "filenamesBig":["finished/far far wider.mp3"],
            "filenamesSmall":["finished/far far wider.mp3"]
        },
        {
            "title":"Junction",
            "description":"Welcome to the hissingly loud junction at the centre. Hear the mishmosh behind the glass.",
            "albumart":"microtonals.png",
            "quality":"good",
            "filenamesBig":["finished/junction.wav"],
            "filenamesSmall":["finished/junction.mp3"]
        },
        {
            "title":"You are causing Me Great Pain",
            "description":"This track follows the story of a young girl being home alone. There's not much to do in the house except think. Cue boredom, playing with pans, lilting, and other things before the car comes home. Be careful, it's not as it seems.",
            "albumart":"microtonals.png",
            "quality":"excellent",
            "filenamesBig":["finished/you are causing me great pain.wav"],
            "filenamesSmall":["finished/you are causing me great pain.mp3"]
        },
        {
            "title":"Tee",
            "description":"Follow a gravely slow microtonal harmonic progression as it evolves into an ambient walk between the lines. Be carefiul, though, beyond 24 TET is a deeply deep rabbithole of information not meant for your western eyes.",
            "albumart":"microtonals.png",
            "quality":"excellent",
            "filenamesBig":["finished/tee.wav"],
            "filenamesSmall":["finished/tee.mp3"]
        },
        {
            "title":"Walking in those hills",
            "description":"Follow my footsteps in the dirt road of the hills - It's the quickest way home. Be careful, the distant gunshots won't be distant for long, those damned hunters.",
            "albumart":"microtonals.png",
            "quality":"excellent",
            "filenamesBig":["finished/walking in those hills.wav"],
            "filenamesSmall":["finished/walking in those hills.mp3"]
        },
        {
            "title":"2024 Overture",
            "description":"Welcome, 2024. This piece explores microtones, \"weird sampling\" (in which samples have all forms of parameters attached to the ending, like loops), synths, and the different paces of time. It attempts to predict 2024. NOTE: THIS IS NEITHER A PREDICTION NOR HARBRINGER FOR 2024 AND THOSE WHO SAY IT IS HAVE NOT GOT THE MENTAL CAPACITY TO PREDICT THIS YEAR WITH ANY DEGREE OF ACCURACY. PLEASE REFRAIN FROM MAKING PREDICTIONS ABOUT THE CHAOTIC FUTURE BASED OFF THE MUSIC OF A WEIRD BRITISH PRODUCER BECAUSE IT'S NOT ACCURATE, FUNNY, OR INTRESTING. PLEASE GO AWAY AND STOP PESTERING ME. FINAL WARNING. THIS PIECE DOES LIE PLEASE DO NOT LISTEN PLEASE DO NOT LOOK PLEASE DO NOT HEAR IT'S A BAD IDEA LOOK AWAY",
            "albumart":"microtonals.png",
            "quality":"excellent",
            "filenamesBig":["finished/2024 overture.wav"],
            "filenamesSmall":["finished/2024 overture.mp3"]
        },
        {
            "title":"Branger (Goodbye 2023)",
            "description":"Published in the final 4 hours of 2023, this piece waves goodbye to yet another year whilst casting uncertainty at the future. What direction will it all go? I don't know. Life feels like a hallucination anyways, so whatever. Watch out for the low shakes at the beggining, the wiping away of the rain, orders to kill, gunshots (dangerous fireworks), unheard sounds, and microtones! They don't have good intentions!<br><br><a href='lyrics/branger.txt' title=\"Be careful, it's not the most accurate. Whatever, though, on with the show!\">Interpretation and lyrics</a>",
            "albumart":"microtonals.png",
            "quality":"excellent",
            "filenamesBig":["finished/branger.wav"],
            "filenamesSmall":["finished/branger.mp3"]
        },
        {
            "title":"Argument with _",
            "description":"Argue with the logical fallacies user-expert every day and find out why i'm always so damn irritated",
            "albumart":"BONES FEAST II.png",
            "quality":"very good",
            "filenamesBig":["finished/Argument with .wav"],
            "filenamesSmall":["finished/Argument with .wav"]
        },{
            "title":"Aharmonicism",
            "description":"Please abandon your harmonic expectations upon playing this piece.",
            "albumart":"BONES FEAST II.png",
            "quality":"good",
            "filenamesBig":["finished/Aharmonicism.wav"],
            "filenamesSmall":["finished/Aharmonicism.wav"]
        },
        {
            "title":"Яростька",
            "description":"Яарость by itself means \"rage\" in Russian, a built up anger against something important. However, since its in itd diminutive (cute) form, it's less of an expression like Anger II and more gentle.",
            "albumart":"BONES FEAST II.png",
            "quality":"very good",
            "filenamesBig":["finished/яростька.wav"],
            "filenamesSmall":["finished/яростька.wav"]
        },{
            "title":"Anger II",
            "description":"A piece focused on a piano riff designed to express ultimate rage. This is a piano solo, which allows me to play without having to come upw ith some accompiment.",
            "albumart":"BONES FEAST II.png",
            "quality":"very good",
            "filenamesBig":["finished/anger II.wav"],
            "filenamesSmall":["finished/anger II.wav"]
        },{
            "title":"Anger I",
            "description":"Anger I is a small piece",
            "albumart":"BONES FEAST II.png",
            "quality":"good",
            "filenamesBig":["finished/anger I.wav"],
            "filenamesSmall":["finished/anger I.wav"]
        },
        {
            "title":"Sophistication",
            "description":"",
            "albumart":"BONES FEAST II.png",
            "quality":"good",
            "filenamesBig":["finished/sophistication.wav"],
            "filenamesSmall":["finished/sophistication.wav"]
        },
        {
            "title":"fun poeice",
            "description":"",
            "albumart":"BONES FEAST II.png",
            "quality":"good",
            "filenamesBig":["finished/funpoeice.wav"],
            "filenamesSmall":["finished/funpoeice.wav"]
        },
        {
            "title":"Poorly-Written Dystopia Novel",
            "description":"Yet another qtractor piece, this time experimenting with synth plugins. I'm still not good at jamming with myself, though.",
            "albumart":"BONES FEAST II.png",
            "quality":"good",
            "filenamesBig":["finished/poorly-written dystopia novel.wav"],
            "filenamesSmall":["finished/poorly-written dystopia novel.mp3"]
        },
        {
            "title":"Keyboard",
            "description":"I decided to try recording MIDI from a keyboard and using that as an alternative workflow to the \"guess and click\" system i was using earlier with LMMS. Cue a weekend suffering with Qtractor before finally coughing this ugly thing up. Note that this was my very first workflow too (which i had used in 2018), using the exact same physical MIDI keyboard but with <iAria Maestosa<i> instead.",
            "albumart":"BONES FEAST II.png",
            "quality":"good",
            "filenamesBig":["finished/keyboard.wav"],
            "filenamesSmall":["finished/keyboard.mp3"]
        },

        {
            "title":"The Middle<sup>th</sup> Tone",
            "description":"A piece designed to explore microtonal harmony and its unique quality. In this case, it's 24 TET as LMMS allows pitch bending by 0.5 on each note, so 12 TET (the standard scale for most of Western mainstream music) can be made into 24TET. Listen out for the intresting chord loop at 04:00. Some synths may sound out-of-tune. This is due to the fact that each quarter-sharp or quarter-flat note is effectively detuned by 50 cents (with 100 cents being between individual notes of 12TET) from its natural counterpart, which is often the maximal interval instruments with individual note tuning, like Pianos, Lyres, or in some cases, Accordions can get up to before going back into tune onto another note.",
            "albumart":"microtonals.png",
            "quality":"excellent",
            "filenamesBig":["finished/the middleth tone.wav"],
            "filenamesSmall":["finished/the middleth tone.mp3"]
        },
        {
            "title":"The Shake",
            "description":"Yet another \"story piece\" where you can make the story from what you hear. Listen out for the out-of-tune lyre (a tiny bit like a guitar) and the ocarina samples (which sounds like seagulls).",
            "albumart":"BONES FEAST II.png",
            "quality":"excellent",
            "filenamesBig":["finished/the shake.wav"],
            "filenamesSmall":["finished/the shake.mp3"]
        },{
            "title":"Nihilism is a Pussy",
            "description":"A song about Nihilism and how it destroys the will to live, written to a boy at my school. The first part is sung by the robot, which does not have your best intrests at heart, but the second is sung by me. Listen for the ocarina samples!<br><br><a href='lyrics/nhilism is a pussy.txt'>Lyrics</a>",
            "albumart":"general.png",
            "quality":"good",
            "filenamesBig":["finished/narcasism is a pussy.wav"],
            "filenamesSmall":["finished/narcasism is a pussy.mp3"]
        },{
            "title":"Passion",
            "description":"A piece i fully designed all the synth settings for. It's about what one of the voices in my brain begin thinking sometimes. It regards impulsivity and its energy-feeding nature. If social energy gets too high, it runs in and begins playing the controls instead like some drunkard.<br><br><a href='lyrics/ejectivo passion.txt'>Lyrics</a>",
            "albumart":"general.png",
            "quality":"ok",
            "filenamesBig":["finished/passion.wav"],
            "filenamesSmall":["finished/passion.mp3"]
        },{
            "title":"Simple",
            "description":"A slow return to the simplicity of composition of the pieces in the test-series, where elements are introduced slowly and thoughtfully. There are plenty of samples to listen to. However, as is clearly evident, the return hasn't been complete.",
            "albumart":"BONES FEAST II.png",
            "quality":"very good",
            "filenamesBig":["finished/simple.wav"],
            "filenamesSmall":["finished/simple.mp3"]
        },{
            "title":"Autopilot",
            "description":"A song about impulsivity and how it afffects me. I sung some of it, thinking the single vocal class i took would be enough, It wasn't, thus the class.<br><br><a href='lyrics/autopilot.txt'>Lyrics</a>",
            "albumart":"general.png",
            "quality":"good",
            "filenamesBig":["finished/autopilot.wav"],
            "filenamesSmall":["finished/autopilot.mp3"]
        },{
            "title":"To Saviest",
            "description":"A slow and dreadful \"song\" using microtones and loops to bring a sense of dread. By falling out of standard 12 TET, the lyrics are given more power and amplification and have their message sent clearer. <br><br><a href='lyrics/saviest.txt'>Lyrics</a>",
            "albumart":"microtonals.png",
            "quality":"excellent",
            "filenamesBig":["finished/saviest.wav"],
            "filenamesSmall":["finished/saviest.mp3"]
        },
        {
            "title":"Antieros",
            "description":"A song about what was once aromantiscm falling into shreds. The instrumention to the second part still does not exist yet.<br><br><a href='lyrics/antieros.txt'>Lyrics (part one)</a><br><a href='lyrics/antieros II.txt'>Lyrics (part two)</a>",
            "albumart":"general.png",
            "quality":"good",
            "filenamesBig":["finished/antieros.wav"],
            "filenamesSmall":["finished/antieros.wav.mp3"]
        },
        {
            "title":"Sea",
            "description":"A dissonant mess of music near the middle, with intresting bits covered, a bit like how the sea works. If you look deep enough forms will come out. Good luck running away.",
            "albumart":"general.png",
            "quality":"ok",
            "filenamesBig":["finished/chill.wav"],
            "filenamesSmall":["finished/chill.wav.mp3"]
        },
        {
            "title":"El Ejuctivo Loco",
            "description":"This song discusses my unfitness for social situations and tendency to cause discomfort in any community i join. It discusses what i do wrong and then derails itself into a conspiracy to kill/mute the part of my brain that does that, with the birepeated line \"Con solo un segundo de pansar, ha decidamos que vamos a mataremos el ejecutivo\".<br><br><a href='lyrics/ejectivo loco.txt'>Lyrics</a>",
            "albumart":"general.png",
            "quality":"very good",
            "filenamesBig":["finished/ejecutivo loco.wav"],
            "filenamesSmall":["finished/ejecutivo loco.wav.mp3"]
        },
        
        {
            "title":"El amor es un constructo falso",
            "description":"This song tries to discuss too many things at once whilst burdened with a self-inflicted requirement to rhyme. It, or at least the beggining of it, features a bilingual structure where Spanish, due to my lesser ability to create metaphors in it, is used to provide more direct speech then English does. Compare \"but the triple Xs are no sages\" and \"Pero, quiero yo solo las relaciones sexuales\" (but all i want is sexual relationships).<br><br>(note to lyric translators: Keep the bilingualism, but use similar languages; Spanish and Portugese (or Catalan), Russian and Ukranian, Welsh and Gaelic. Ensure that the second language is as mutually intelligable as possible without being too easy to read. Keep it bilingual. You should provide two translations such that the .5 verses have the target language and the .0 verses the foreign language, the exact opposite of the first translation. This ensures your target audience can understand the song properly. Should you only be able to translate to one language, collaborate with another translator to a foreign language and ensure the languages are still mixed.)<br><br><a href='lyrics/el amor es un constructo falso.txt'>Lyrics</a>",
            "albumart":"general.png",
            "quality":"very good",
            "filenamesBig":["finished/el amor es un constructo falso.wav"],
            "filenamesSmall":["finished/el amor es un constructo falso.wav.mp3"]
        },
        {
            "title":"Lusto",
            "description":"This slow-paced trilingual song discusses my own body-image, my lower, more unrefined desires, alongside other less desirable content. Please avoid if anybody around you is under 20 years of age.<br><br>Toki Pona has been used as a \"euphanism language\" to express the lowest desires, especially in the beggining with \"Mi wile palisa insa mi lupa\". This choice was seen fit for two reasons; to dumb down the lyrics to a point where even a lust-filled brain can comprehend them, and also to ensure that only a small portion of the world can read such vulgar and shameful text.<br><br><a href='lyrics/lusto.txt'>Lyrics</a>",
            "albumart":"general.png",
            "quality":"very good",
            "filenamesBig":["finished/lusto.wav"],
            "filenamesSmall":["finished/lusto.wav.mp3"]
        },
        {
            "title":"Verrücktertanz",
            "description":"The name is a compound word which literally translates to \"crazy dance\" (verrüct meaning crazy, and tanz meaning dance). The piece has three main parts disussing the crazy crap that probably happens in a nightclub, such as vomiting, rushed voices, crazy singing, screaming, and other rowdy things. We're kicked out before the party even gets started, in which a copywronged sample of Ke$ha's <i>TikTok</i> is sampled in full.<br><br>Tip: if you play samples with a length of like 20 ms and loop them, you can make tones from the repetiton alone, which is how the intro was formed.",
            "albumart":"general.png",
            "quality":"very good",
            "filenamesBig":["finished/Verrücktertanz.wav"],
            "filenamesSmall":["finished/Verrücktertanz.wav.mp3"]
        },
        {
            "title":"Transformer",
            "description":"(please note that the album art was implemented before the war and was selected for the multilingualism, vibe introduced by such a morbid sign, and lighting) I've captured the immense power of electricity running through transformers and used it to make heavy, industrial music. It's formed of three parts, the first being an optimistic introduction as we drive to the substation. The second shows the substation powering up as the sun rises. The third part brings us to a teenage loitering party in a sketchy place nearby with drunken break-dancing possibly caused by contact with a live wire.",
            "albumart":"danger.png",
            "quality":"excellent",
            "filenamesBig":["finished/transformer.wav"],
            "filenamesSmall":["finished/transformer.wav.mp3"]
        },
        {
            "title":"Kettle",
            "description":"The bitter tea burns. But you have to take life headfirst and put your head first. Or you'll fall behind. The juxtaposition between the harmonious synths and grotesque screams introduces the deep pull between lawless, pent-up attraction and rationality. The sample saying headfirst is the same one as was used in a song about love without realising where it was being wasted.",
            "albumart":"general.png",
            "quality":"excellent",
            "filenamesBig":["finished/kettle.wav"],
            "filenamesSmall":["finished/kettle.wav.mp3"]
        },
        {
            "title":"Slow",
            "description":"Sometimes, don't run and smash deadlines in half. The slow growth of this piece over the course of several minutes building up to nothing representes how much asted productivity there is in the world. The laughing-like-sound at the end is the maniac laughter of those who realised just how much waste there is and how it's messed up everything.",
            "albumart":"general.png",
            "quality":"very good",
            "filenamesBig":["finished/slow.wav"],
            "filenamesSmall":["finished/slow.wav.mp3"]
        },
        {
            "title":"Age of Mould",
            "description":"This is an (incomplete) album investigating the intersection between humans and their comrades in nature. Each song name is based off Russian words mutulated into the English language. Attempt to decode them.<br><br>Oh, Pass Not (opasnot) is where humans and the forest collide. Off-posed is where the grain train drops off their cargo. Day of Maya (dymovaya) is about the smoking chimneys, grown from an attempt to recreate industrial dubstep. Les of Murder is a piece about murder. The longest track, Yarost Forest Gnaws the Lost, is about anger, including what led to that murder. The final two tracks are to be taken together; Prokyrote Routine Robot (chore) and Smarite (death). These are the saddest and slowest ones.<br><br>Planned is the extravagant dance track.<br><br>Murder has a piano version, formed by exporting via midi. Unrefined sheet music of the piano interpration is available <a href=\"murder.pdf\">here</a>. It was made by LMMS and some manouvers may be impossible in real life. It is left as an excersice to the reader to resolve these issues and interpret the sheet music.",
            "albumart":"aom.png",
            "quality":"excellent",
            "filenamesBig":[
                "aom/Oh, pass not.wav",
                "aom/Off-Posed.wav",
                "aom/Day of Maya.wav",
                "aom/Les of Murder.wav",
                "aom/Les of Murder (piano).wav",
                "aom/Yarost Forest Gnaws the Lost.wav",
                "aom/Prokyrote Routine Robot.wav"
            ],
            "filenamesSmall":[
                "aom/Oh, pass not.wav.mp3",
                "aom/Off-Posed.wav.mp3",
                "aom/Day of Maya.wav.mp3",
                "aom/Les of Murder.wav.mp3",
                "aom/Les of Murder (piano).wav.mp3",
                "aom/Yarost Forest Gnaws the Lost.wav.mp3",
                "aom/Prokyrote Routine Robot.wav.mp3"
            ]
        },
        {
            "title":"Ghost Tunnel Syndrome",
            "description":"This is the tunnel through the backrooms so good luck. You can practically hear the acoustics of the tunnel as the ghostly, unconventional dance jig plays from somewhere inside, almost like a more malovelent version of Châtlet metro station. In fact, the screaming represents the lost musicians who went to investigate and instead had unintened vocal iprovisation over the music.",
            "albumart":"public-broser.png",
            "quality":"good",
            "filenamesBig":["finished/ghost tunnel syndrome.wav"],
            "filenamesSmall":["finished/ghost tunnel syndrome.wav.mp3"]
        },
        {
            "title":"The Surface of a Star",
            "description":"Dare to land there if you dare. The Soviets already have. In this piece, we can hear pitch bending on long notes wrapping up quick measures of quavers complimenting a bassline, showing the precious needed for a rocket to launch whilst also showing the inherent randomness of the such a complicated device. The beeps are a more annoying version of the cockpit alarms coming and going as the rocket experiences unearthly forces. And just as we picked up speed, we slow down to reach the sun.",
            "albumart":"public-broser.png",
            "quality":"good",
            "filenamesBig":["finished/The surface of a star.wav"],
            "filenamesSmall":["finished/The surface of a star.wav.mp3"]
        },
        {
            "title":"Dome-shaped ice ghost in a summer house",
            "description":"This piece initally started by following a <a href=\"https://www.youtube.com/watch?v=h3elFIKC3KE&t=215s&pp=ygUeaG93IHRvIHByb2R1Y2UgbGlrZSBpZ2xvb2dob3N0\">tutorial</a> on composing like Iglooghost (thus the name referencing a ghost and ice dome), but i ended going off track like i always do and it became something else (thus the name indirectness)",
            "albumart":"public-broser.png",
            "quality":"good",
            "filenamesBig":["finished/Dome-shaped ice ghost in a summer house.wav"],
            "filenamesSmall":["finished/Dome-shaped ice ghost in a summer house.wav.mp3"]
        },
        {
            "title":"The Cobra",
            "description":"Initially started as an imitation of COBRAH's high-dancibility tracks, but ended up falling slightly short",
            "albumart":"public-broser.png",
            "filenamesBig":["finished/the cobra.wav"],
            "filenamesSmall":["finished/the cobra.wav.mp3"]
        },
        {
            "title":"Lock Rié In",
            "description":"This piece is an attempt at composing in Locrian (an infamously dissonant scale). The result? A badass piece.",
            "albumart":"public-broser.png",
            "filenamesBig":["finished/lock rié in.wav"],
            "filenamesSmall":["finished/lock rié in.wav.mp3"]
        },
        {
            "title":"Dream",
            "description":"This piece initally started by trying to make a dreamy echoey piece of music like HANAH's <i>SO & SO</i>, but it ended up going somewhere else",
            "albumart":"public-broser.png",
            "filenamesBig":["finished/dream.wav"],
            "filenamesSmall":["finished/dream.wav.mp3"]
        },
        {
            "title":"Vladimir's story",
            "description":"Initially, this song came from an attempt to repliate Shamaran by Sevdaliza. It ended up taking mainly the chords and then started making its own stuff. Now, it stands as some musical accompiment to <a href='https://blog.reedsy.com/short-story/96x8hl/'>Vladimir is not ok</a>, where the beggining stands for the diary and then we transition into the 999 call and end.",
            "quality":"ok",
            "albumart":"isol.png",
            "filenamesBig":["finished/vladimir.wav"],
            "filenamesSmall":["finished/vladimir.wav.mp3"]
        },
        {
            "title":"you will die.",
            "description":"By continuing to read the description of the song \"you will die\", the Reader explicitly agrees to the following terms and conditions: The title of the song shall immediately apply to the Reader, resulting in death within 10 hours.",
            "albumart":"isol.png",
            "filenamesBig":["finished/you will die.wav"],
            "filenamesSmall":["finished/you will die.wav.mp3"]
        },
        {
            "title":"Isol",
            "description":"I tried to write a piece that wasn't as robotic and part-based as my previous ones. It still has two distinct parts, but they tie closer together and have naturalistic glissandos and grace notes to make the melody more intresting on top of a novel chord base.<br /><br />It tries to be isolating and sad, but without the masterful chords of a producer better then me, it fails.",
            "albumart":"isol.png",
            "quality":"good",
            "filenamesBig":["finished/isol.wav"],
            "filenamesSmall":["finished/isol.wav.mp3"]
        },
        {
            "title":"Lone",
            "description":"80 BPM, a phrygian key, and a chorus. How much sadder can we get?",
            "albumart":"isol.png",
            "quality":"ok",
            "filenamesBig":["finished/lone.wav"],
            "filenamesSmall":["finished/lone.wav.mp3"]
        },
        {
            "title":"Santander-Barcelona",
            "description":"This piece was made to capture the feeling of driving 8 hours across one of the biggest countries in Europe to get from the ferry terminal (at the atlantic) to our house (At the Medditeranian).<br /><br />We can hear how the mood changes as we first try to get away from the port and onto the <i>Autopista</i> before going through the desert and finding peace before being met with civilisation again near Barcelona (also Zaragoza, but that was barely relevant seeing as we were bypassing it).",
            "albumart":"Santander-barcelona.png",
            "quality":"excellent",
            "filenamesBig":["finished/Santander-Barcelona post.mp3.wav"],
            "filenamesSmall":["finished/Santander-Barcelona post.mp3.wav.mp3"]
        },
        {
            "title":"Sounds of the Night",
            "description":"Sounds of the night takes actual samples from the night when people are asleep and represents my normal sleeping cycle.<br /><br />It starts off with mysterious intro as the brain slowly turns off services and prepares itself to rest before turning off consciousness whilst it isn't looking, before the consciousness is put in watch-only mode during REM sleep. It will probably not remember.<br /><br />Then we hear some sounds of the night as i wake up after that dream before watching a YT short and going back to bed.<br /><br />And then we reach the end as other family members wake up early and slowly begin  being so loud they wake me up whilst i attempt to go back to sleep. We also hear a reference to test 16 with the alarm.",
            "albumart":"sounds of the night2.png",
            "quality":"excellent",
            "filenamesBig":["finished/sounds of the night video.mp4"],
            "filenamesSmall":["finished/sounds of the night video.mp4"]

        },
        {
            "title":"Coding Album",
            "description":"An album about programming languages. Each piece represents each language. Cobol starts with a cobweb-filled organ, Java and Kotlin are linked together musically (but Kotlin is nicer), Ruby is industrious and has a nice buildup, and so on.",
            "albumart":"coding.png",
            "quality":"very good",
            "filenamesSmall":[
                "coding/cobol.wav",
                "coding/haskell.wav",
                "coding/java.wav",
                "coding/kotlin.wav",
                "coding/ruby.wav",
                "coding/python.wav"
            ],
            "filenamesBig":[
                "coding/cobol.wav.mp3",
                "coding/haskell.wav.mp3",
                "coding/java.wav.mp3",
                "coding/kotlin.wav.mp3",
                "coding/ruby.wav.mp3",
                "coding/python.wav.mp3"
            ]
        },
        {
            "title":"Hero of Real",
            "description":"I mixed <i>Hero</i> by <i>Sevdaliza</i> and <i>Do You Feel Real</i>, also by <i>Sevdaliza</i>. Hopefully you like it.",
            "albumart":"public-broser.png",
            "quality":"good",
            "filenamesBig":["remixes/Hero of Real.ogg"],
            "filenamesSmall":["remixes/Hero of Real.ogg"]
        },
        {
            "title":"Pain",
            "description":"I mixed <i>Muñecas</i> by <i>Arca</i> and <i>Rafagá</i>, also by <i>Arca</i> to create a pained and dramatic piece, lacking the relative calmness of Muñecas or numbing consistency of Rafagá.",
            "albumart":"public-broser.png",
            "quality":"good",
            "filenamesBig":["remixes/pain.wav"],
            "filenamesSmall":["remixes/pain.wav.mp3"]
        },
        {
            "title":"simingei-singmaiup",
            "description":"A piece i forgot why i made",
            "albumart":"new-series2.png",
            "quality":"excellent",
            "filenamesBig":["finished/simingei-singmaiup.wav"],
            "filenamesSmall":["finished/simingei-singmaiup.wav.mp3"]
        },
        {
            "title":"Russian Radio Sounds",
            "description":`This piece mixes sounds from various <a href="https://priyom.org/military-stations/russia">russian military radio stations</a> with musical melody samples to create a tapestry of culture and fear. There are hidden messages, inspired by the one inside Windowlicker by Aphex Twin.`,
            "albumart":"public-broser.png",
            "quality":"very good",
            "filenamesBig":["finished/russian radio sounds.wav"],
            "filenamesSmall":["finished/russian radio sounds.wav.mp3"]
        },
        {
            "title":"WIR",
            "description":"This song represents the standardisation of student's knowledges by schools with a mechanical sound reminicent of a factory robot.<br /><br />One can hear a famous educator mumbling at the background, slowly realising doubt about the system (\"Maybe- Maybe- Maybe-\").",
            "albumart":"new-series2.png",
            "quality":"very good",
            "filenamesBig":["finished/WIR post.wav"],
            "filenamesSmall":["finished/WIR post.wav.mp3"]
        },
        {
            "title":"Funfair",
            "description":"This piece makes you as uncomfortable as i am sometimes using high pitches and a foreign language you probably can't speak.<br /><br />This piece paraphrases from Cyriak and Cbat in places.",
            "albumart":"tmo.png",
            "quality":"ok",
            "filenamesBig":["finished/funfair.wav"],
            "filenamesSmall":["finished/funfair.wav.mp3"]
        },
        {
            "title":"Whales",
            "description":"This is a piece about the sounds of the sea. Eventually, boat engines begin and a seismic survey starts. It nevers stops, not until the funerals of the stressed whales ends.<br /><br />I've taken samples from a lot of different places to represent how all humans are involved in this by using oil and by extension continuing the demand for seismic surveys.<br /><br />Visit <a href=\"https://www.youtube.com/watch?v=t0DHEldqfIc\">this video</a> for more info. Also, try guessing where each sample came from!",
            "albumart":"new-series2.png",
            "quality":"ok",
            "filenamesBig":["finished/whales.wav"],
            "filenamesSmall":["finished/whales.wav.mp3"]
        },
        {
            "title":"hhh",
            "description":"hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh  i wonder what happens if i just make experimental music instead of spending effort on cool stuff hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh hhhhhhh",
            "albumart":"public-broser.png",
            "filenamesBig":["finished/hhh.wav"],
            "filenamesSmall":["finished/hhh.wav.mp3"]
        },
        {
            "title":"Bass",
            "description":"I had a midi keyboard and decided to create a piece with it",
            "albumart":"tmo.png",
            "quality":"good",
            "filenamesBig":["finished/bass.wav"],
            "filenamesSmall":["finished/bass.wav.mp3"]
        },
        {
            "title":"Divergence",
            "description":"An album about neurodivergence. However, ironically, due to the same hobby cycle mentioned in the second song i couldn't finish the album. As such, i am left with this SINGLE piece about the chaos that happened as i grew up without understanding myself.",
            "albumart":"divergent.png",
            "quality":"good",
            "filenamesBig":["divergence/1.wav"],
            "filenamesSmall":["divergence/1.wav.mp3"]
        },
        {
            "title":"AAAAA (5 As)",
            "description":"AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA what happens when you use 190 bpm and a speedy scale and overlap AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA",
            "albumart":"tmo.png",
            "filenamesBig":["finished/5as.wav"],
            "filenamesSmall":["finished/5as.wav.mp3"]
        },
        {
            "title":"Reckless Cyclist",
            "description":"This song captures the motions of a reckless cyclist, who picks up speed going down a hill before hitting pedestrians and making them scream in anguish, before turning back and climbing the hill to the sound of ambulance sirens, and finally feeling guilt after what drugs the cyclist took had finally passed.",
            "albumart":"reckless2cover.png",
            "filenamesBig":["finished/The reckless cyclist.wav"],
            "filenamesSmall":["finished/The reckless cyclist.wav.mp3"]
        },
        {
            "title":"Bonk",
            "description":"A noise piece designed to explore sounds.",
            "albumart":"tmo.png",
            "filenamesBig":["finished/bonk.wav"],
            "filenamesSmall":["finished/bonk.wav.mp3"]
        },
        {
            "title":"New",
            "description":"The new and the all! Capture all together! The new button! The new and the all!!!!!!!!!!!!!!! PERFECT!!!!!!",
            "albumart":"new-series2.png",
            "quality":"ok",
            "filenamesBig":["finished/new.wav"],
            "filenamesSmall":["finished/new.wav.mp3"]
        },
        {
            "title":"3.mp3",
            "description":"This is a piece of music formed of two repeats of 1.mp3 and one play of 2.mp3.",
            "albumart":"public-broser.png",
            "filenamesBig":["finished/3.mp3.wav"],
            "filenamesSmall":["finished/3.mp3.wav.mp3"]
        },
        {
            "title":"BeepBoop",
            "description":"Beep the boops and boop the beeps and the song is there.",
            "albumart":"new-series2.png",
            "filenamesBig":["finished/beepboop.wav"],
            "filenamesSmall":["finished/beepboop.wav.mp3"]
        },
         {
            "title":"Small Talk",
            "description":"This song is now CC0 due to the fact i no longer fit the personality that wrote the song.<br><br>The first song is janky and not reccomended for people who enjoy pleasure. The second song is slighly more refined, featuring clearer lyrics, bilingualism, and more typical instrumental structure. Lyrics are available <a href=\"lyrics/small talk.txt\">here</a>.",
            "albumart":"general.png",
            "quality":"ok",
            "filenamesBig":[
                "finished/small talk.wav",
                "finished/small talk better.wav"
                ],
            "filenamesSmall":[
                "finished/small talk.wav.mp3",
                "finished/small talk better.wav.mp3"
                ]
        },
        {
            "title":"He",
            "description":"This song is now CC0 due to the fact i no longer fit the personality that wrote the song.<br><br>A first song. Good luck lyric transcribers because espeak is primative and i've abused the samples.",
            "albumart":"he.png",
            "quality":"ok",
            "filenamesBig":["finished/he.wav"],
            "filenamesSmall":["finished/he.wav.mp3"]
        },

        {
            "title":"Loop Album",
            "description":"Where 3.mp3 came from!",
            "albumart":"tmo.png",
            "filenamesBig":[
                "loop album/1.wav",
                "loop album/2.wav",
                "loop album/3.mp3.wav"
            ],
            "filenamesSmall":[
                "loop album/1.wav.mp3",
                "loop album/2.wav.mp3",
                "loop album/3.mp3"
            ]
        },
        {
            "title":"Test",
            "description":"My very first baby steps into electronic music.<br><br>These pieces of music were created by a mind, body, personality, brain, and computer that no longer exist or function like they did when they created this music series. This music series is no longer representative of that brain. As such, all of the works in the <code>test-series</code> directory are licensed under CC0.",
            "quality":"ok",
            "albumart":"tmo.png",
            "filenamesBig":[
                "test-series/test 01.wav",
                "test-series/test 02.wav",
                "test-series/test 03.wav",
                "test-series/test 04.wav",
                "test-series/test 05.wav",
                "test-series/test 06.wav",
                "test-series/test 07.wav",
                "test-series/test 08.wav",
                "test-series/test 09.wav",
                "test-series/test 10.wav",
                "test-series/test 11.wav",
                "test-series/test 12.wav",
                "test-series/test 13.wav",
                "test-series/test 14.wav",
                "test-series/test 15.wav",
                "test-series/test 16.wav",
                "test-series/test 17.wav",
                "test-series/test 18.wav",
                "test-series/test 19.wav",
                "test-series/test 20.wav",
                "test-series/test 21.wav",
                "test-series/test 22.wav",
                "test-series/test 25.wav",
                "test-series/test 26.wav",
                "test-series/test 27.wav",
                "test-series/test 28.wav"
            ],
            "filenamesSmall":[
                "test-series/test 01.wav.mp3",
                "test-series/test 02.wav.mp3",
                "test-series/test 03.wav.mp3",
                "test-series/test 04.wav.mp3",
                "test-series/test 05.wav.mp3",
                "test-series/test 06.wav.mp3",
                "test-series/test 07.wav.mp3",
                "test-series/test 08.wav.mp3",
                "test-series/test 09.wav.mp3",
                "test-series/test 10.wav.mp3",
                "test-series/test 11.wav.mp3",
                "test-series/test 12.wav.mp3",
                "test-series/test 13.wav.mp3",
                "test-series/test 14.wav.mp3",
                "test-series/test 15.wav.mp3",
                "test-series/test 16.wav.mp3",
                "test-series/test 17.wav.mp3",
                "test-series/test 18.wav.mp3",
                "test-series/test 19.wav.mp3",
                "test-series/test 20.wav.mp3",
                "test-series/test 21.wav.mp3",
                "test-series/test 22.wav.mp3",
                "test-series/test 25.wav.mp3",
                "test-series/test 26.wav.mp3",
                "test-series/test 27.wav.mp3",
                "test-series/test 28.wav.mp3"
            ]
        },
        {
            "title":"Really old pieces",
            "description":"This is perhaps the oldest music i've ever made. It dates from between 2017 to 2019, which is when i transitioned to LMMS, and was probably made in Aria Maestosa, Hydrogen drum machine, Musescore 3, or even Chrome Music Lab. In many of the pieces you can hear the player \"discovering\" as they go through the piece, literally playing in the music. This is a very childish and naïve approach to writing music, typical of a 13 year old dippin their toes into it. Since this music is so old and irrelevant to my current stage in production, this music is now under CC0. Make what use of it you will.",
            "albumart":"tmo.png",
            "quality":"ok",
            "filenamesBig":[
                "old/failures/another sad peice.mp3",
                "old/failures/apeiceaboutstuff.mp3",
                "old/failures/its hard.wav",
                "old/failures/multipleinstruments.mp3",
                "old/failures/my naïve sister's music.mp3",
                "old/failures/nice.mp3 dockers/nginx/public/music/old/failures/sad.mp3",
                "old/failures/sadacousticclarinetguitara.mp3",
                "old/failures/some nice melodies.mp3",
                "old/failures/test 0019.wav",
                "old/failures/test beat 00001.wav",
                "old/failures/test beat 00002.wav",
                "old/failures/verysad.mp3",
                "old/other peices/abstract discovery.mp3",
                "old/other peices/building collapse.mp3",
                "old/other peices/corruption of the life ride.mp3",
                "old/other peices/cyanide pill 00345.mp3",
                "old/other peices/death of a talent.mp3",
                "old/other peices/funky depression.mp3",
                "old/other peices/in the furnace of hate.mp3",
                "old/other peices/melody of the tortured souls.mp3",
                "old/other peices/the sadness prison and how to get there.mp3",
                "old/other peices/the sound of pauses in a peice of music made by me.mp3",
                "old/other peices/the story of the energy.mp3",
                "old/other peices/tortured accordion.mp3",
                "old/other peices/wake me from my eternal sleep.mp3",
                "old/sessions/session 08 01 2019.mp3",
                "old/sessions/session 09 01 2019.mp3",
                "old/sessions/session 11 01 2019 melodies.mp3",
                "old/sessions/session 12 01 2019.mp3"
            ],
            "filenamesSmall":[
                "old/failures/another sad peice.mp3",
                "old/failures/apeiceaboutstuff.mp3",
                "old/failures/its hard.wav",
                "old/failures/multipleinstruments.mp3",
                "old/failures/my naïve sister's music.mp3",
                "old/failures/nice.mp3 dockers/nginx/public/music/old/failures/sad.mp3",
                "old/failures/sadacousticclarinetguitara.mp3",
                "old/failures/some nice melodies.mp3",
                "old/failures/test 0019.wav",
                "old/failures/test beat 00001.wav",
                "old/failures/test beat 00002.wav",
                "old/failures/verysad.mp",
                "old/other peices/abstract discovery.mp3",
                "old/other peices/building collapse.mp3",
                "old/other peices/corruption of the life ride.mp3",
                "old/other peices/cyanide pill 00345.mp3",
                "old/other peices/death of a talent.mp3",
                "old/other peices/funky depression.mp3",
                "old/other peices/in the furnace of hate.mp3",
                "old/other peices/melody of the tortured souls.mp3",
                "old/other peices/the sadness prison and how to get there.mp3",
                "old/other peices/the sound of pauses in a peice of music made by me.mp3",
                "old/other peices/the story of the energy.mp3",
                "old/other peices/tortured accordion.mp3",
                "old/other peices/wake me from my eternal sleep.mp3",
                "old/sessions/session 08 01 2019.mp3",
                "old/sessions/session 09 01 2019.mp3",
                "old/sessions/session 11 01 2019 melodies.mp3",
                "old/sessions/session 12 01 2019.mp3"
                ]
            
        }
    ]

    function fadeIn(selector, delay, animationLength) {

    };

    let placeForCards = $("#portfolio")

    let actualI = 0
    let selected = getQalNum($("#classes").val())

    function getQalNum(quality){
        // This is not the best way i think; a list might be better

        return quality == "ok" ? 1 : quality == "good" ? 2 : quality == "very good" ? 3 : quality == "excellent" ? 4 : 0
    }

    console.log("retuning music of quality " + selected)

    for (let i = 0; i < projects.length; i++) {
        if (actualI == 0 || actualI%3 == 0) {
            row = $("<div>").addClass("row musics")
        }

        console.log(getQalNum(projects[i].quality) >= selected)
        
        if (getQalNum(projects[i].quality) >= selected){
            console.log(projects.length)
            actualI = actualI + 1
            col = $("<div>").addClass("col-s-12 col-md-4")

            row.append(col.append(makeCard(projects[i], col)))
            placeForCards.append(row)

        }

        if (i > projects.length){ break}
    }

    $("body").show()

    if(!optimised()){
        fadeIn("body", 200, 1000)
        fadeIn(".card", 250, 500)
    }

    var count = $(".btn-primary").length;
    $("#count").text(count);

    $(".removable").remove()
}

$(document).ready(function () {
    $("#confirm").click(function () {
        console.log("Changed!")
        
        putInURL("class", $("#classes").val())
        $("#classSpan").text($("#classes").val())
        
        $(".musics").remove()
        run()
    })

    className = getFromURL("class")
    $("#classes").val(className || "very good")

    $("#confirm").click()

    $(".musics").remove()
    run()
})

