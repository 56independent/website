$("body").append(`
<nav class="navbar navbar-expand-lg bg-body-tertiary">
    <div class="container-fluid">
        <a class="navbar-brand" href="/public/index.html" style="min-height: 0px;">56independent</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/public/index.html">Main Page</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/public/portfolio/index.html">Portfolio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/public/music.html">Music i made</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/public/markdowns/index.html">Publications</a>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Programming Projects
                  </a>
                  <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="https://fossmm-56independent-fdacfbc89096f905f4aab75709b5b928a571ed1a90.gitlab.io/public/">FOSS Music Maker</a></li>
                    <li><a class="dropdown-item" href="https://plot-ide-56independent-ddf58a757c55d2717a37c165f9abddbceef41f09.gitlab.io/public/?project=%7B%7D">Plot IDE</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="/public/canvas/index.html">Simulations with HTML</a></li>
                    <li><a class="dropdown-item" href="/public/gomake/index.html">Gomake: Software maker IDE in need of TLC</a></li>
                    <li><a class="dropdown-item" href="/public/gomake/index.html">Gomake: Software maker IDE in need of TLC</a></li>
                    <li><a class="dropdown-item" href="/public/messanger/index.html">Messanger: Peer-based messaging service</a></li>
                    <li><a class="dropdown-item" href="/public/peerdown/index.html">Peerdown: Peer-based collaborative editor</a></li>
                  </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
`)
